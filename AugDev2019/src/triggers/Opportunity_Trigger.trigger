trigger Opportunity_Trigger on Opportunity (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    in_gage__Ingage_Trigger_Settings__c tSetting = in_gage__Ingage_Trigger_Settings__c.getOrgDefaults();
	if(!setting.in_gage__Deactivate_Opportunity_Trigger__c && !tSetting.in_gage__Deactivate_Opportunity_Trigger__c){
        boolean userHasIngageLicense = false;
        PackageLicense inGagePackage;
        try{
        	inGagePackage = [SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'in_gage' limit 1];        
        	if(inGagePackage != null){
	        	boolean isSandbox = [SELECT IsSandbox FROM Organization].isSandbox;
	        	if(isSandbox) userHasIngageLicense = true;
	        	else{
                    if(UserInfo.isCurrentUserLicensed('in_gage')) userHasIngageLicense = true;
	        	}
	        }
        }catch(exception e){
        	// Try-Catch due to "List has no rows for assignment to SObject" failure in In-Gage Development Environment.        
        	if(test.isRunningTest() || userInfo.getOrganizationId() == '00D24000000K4NmEAK'){  // due to Try-Catch above
				userHasIngageLicense = true;
			}
        }
        if(userHasIngageLicense){
    		in_gage.Opportunity_Trigger triggerHandler = new in_gage.Opportunity_Trigger();
    		triggerHandler.execute();
        }
    }
}