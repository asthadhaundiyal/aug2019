trigger QualityAssessmentFormTrigger on Quality_Assessment_Form__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) {

    in_gage.QualityAssessmentForm_Trigger triggerHandler = new in_gage.QualityAssessmentForm_Trigger();
    triggerHandler.execute();
}