trigger QAAIFeedbackTrigger on QA_AI_Feedback__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) {
	
    in_gage.QAAIFeedback_Trigger triggerHandler = new in_gage.QAAIFeedback_Trigger();
    triggerHandler.execute();
}