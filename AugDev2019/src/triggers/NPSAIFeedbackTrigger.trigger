trigger NPSAIFeedbackTrigger on NPS_AI_Feedback__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) {
	
    in_gage.NPSAIFeedback_Trigger triggerHandler = new in_gage.NPSAIFeedback_Trigger();
    triggerHandler.execute();
}