trigger Survey on Survey__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) 
{
	in_gage.Survey_Trigger triggerHandler = new in_gage.Survey_Trigger();
    triggerHandler.execute();  
}