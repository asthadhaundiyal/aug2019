trigger AnalyticsTrigger on Analytics__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    in_gage.Analytics_Trigger triggerHandler = new in_gage.Analytics_Trigger();
    triggerHandler.execute();
}