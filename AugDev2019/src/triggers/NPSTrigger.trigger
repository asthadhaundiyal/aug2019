trigger NPSTrigger on NPS__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) {

    in_gage.NPS_Trigger triggerHandler = new in_gage.NPS_Trigger();
    triggerHandler.execute();
}