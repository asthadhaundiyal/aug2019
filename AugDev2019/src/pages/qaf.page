<apex:page standardController="Quality_Assessment_Form__c" extensions="QualityAssessmentFormController" showHeader="true" >
    <apex:pageBlock rendered="{!displayAIData}">
        <apex:include pageName="AIQualityFormLightning"/>
    </apex:pageBlock>
    <style>
        .formBackground{background-color: white;}
    </style>
    
    <apex:form >
        <apex:inlineEditSupport event="ondblClick" showOnEdit="inlineEditSave,inlineEditCancel" hideOnEdit="editButton,deleteButton,cloneButton,newButton" disabled="{!isAgentUser}"/>
        
        <apex:sectionHeader title="{!IF(Quality_Assessment_Form__c.Id!=null,Quality_Assessment_Form__c.Name,'New Quality Assessment Form')}" />
        <apex:pageMessages />
        <apex:pageBlock rendered="{!canRead==false}">You do not have access to this record.</apex:pageBlock>
        
        <apex:pageBlock mode="{!detailMode}" id="detailSection" rendered="{!canRead}">
            <apex:pageBlockButtons location="top">
                <apex:commandButton id="editButton" value="Edit" action="{!editQAF}" rendered="{!AND(detailMode!='edit',OR(canEdit,(isAgentUser && !isLocked && !hasSuccessfullyAppealed)))}" reRender="detailSection,relLists"/>
                <apex:commandButton id="deleteButton" value="Delete" action="{!Delete}" rendered="{!AND(detailMode!='edit',canDel)}" />
                <apex:commandButton id="cloneButton" value="Clone" action="{!cloneQAF}" rendered="{!AND(detailMode!='edit',canEdit, canDel)}" />
                <apex:commandButton id="newButton" value="New" action="{!newQAF}" rendered="{!AND(detailMode!='edit',canEdit, canDel)}" />
                <apex:commandButton id="submit" value="Submit" action="{!submitForm}" rendered="{!AND(detailMode!='edit',canEdit, canDel, Quality_Assessment_Form__c.Can_Submit_Form__c)}"/>
                
                <apex:commandButton id="saveButton" value="Save" action="{!saveQAF}" rendered="{!detailMode=='edit'}" />
                <apex:commandButton id="cancelButton" value="Cancel" action="{!cancelQAF}" rendered="{!detailMode=='edit'}" immediate="true" html-formnovalidate="formnovalidate" />
                <apex:commandButton id="inlineEditSave" value="Save" action="{!saveQAF}"   style="display:none"/>
                <apex:commandButton id="inlineEditCancel" value="Cancel" action="{!cancelQAF}" style="display:none"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection >
                <apex:outputField value="{!qa.Agent__c}" rendered="{!OR(detailMode!='edit',Quality_Assessment_Form__c.id!=null)}" /><apex:inputField value="{!qa.Agent__c}" taborderhint="1" rendered="{!AND(detailMode=='edit', Quality_Assessment_Form__c.id==null)}" required="{!requiredFields}" />
                <apex:outputText value="{!agentName}" label="Agent Full Name" rendered="{!detailMode!='edit'}"/>
                <apex:outputText value="{!managerName}" label="Manager Full Name" rendered="{!detailMode!='edit'}"/>
                <apex:outputField value="{!qa.Call_Date__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Call_Date__c}" taborderhint="6" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                
                

                
                <apex:outputField value="{!qa.Question_Set__c}" rendered="{!OR(detailMode!='edit',answerCount>0)}" />
                <apex:inputField value="{!qa.Question_Set__c}" taborderhint="2" rendered="{!AND(detailMode=='edit',answerCount==0)}">
                    <apex:actionSupport event="onchange" action="{!refreshCriticalErrors}" />
                    <apex:actionSupport event="onchange" rerender="criticalErrorSection1,criticalErrorSection2,questionSection" />
                </apex:inputField>
                
          
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Type</apex:outputLabel>
                    <apex:actionRegion >
                        <apex:outputField value="{!qa.Type__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" />
                        <apex:inputField value="{!qa.Type__c}" taborderhint="3" required="{!requiredFields}" rendered="{!AND(detailMode=='edit',canEdit==true)}" >
                            <apex:actionSupport event="onchange" rerender="recordLookup"/>                   
                        </apex:inputField>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                
                
                <apex:outputField value="{!Quality_Assessment_Form__c.Quality_Team_Agent__c}" />
                <apex:pageblockSectionItem />
                <apex:outputField value="{!qa.Submitted_Form_Date__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Submitted_Form_Date__c}" taborderhint="9" rendered="{!AND(detailMode=='edit',canEdit==true)}" /> 
            </apex:pageBlockSection>
            
            <apex:pageBlockSection id="recordLookup" rendered="{!OR(Quality_Assessment_Form__c.Question_Set__c != null,Quality_Assessment_Form__c.Case__c!=null, Quality_Assessment_Form__c.Opportunity__c!=null)}">
                <apex:inputfield label="Case" id="Case" value="{!qa.Case__c}" taborderhint="4"  rendered="{!AND(detailMode=='edit',canEdit==true,qa.Type__c != 'Opportunity')}" />
                <apex:outputfield label="Case" id="CaseRead" value="{!qa.Case__c}" rendered="{!OR(detailMode!='edit',canEdit==false) && qa.Type__c != 'Opportunity'}" >
                    <apex:inlineEditSupport disabled="true"/>
                </apex:outputfield>   
                <apex:inputfield label="Opportunity" id="Oppty" value="{!qa.Opportunity__c}" taborderhint="4" rendered="{!AND(detailMode=='edit',canEdit==true,qa.Type__c == 'Opportunity')}" />
                <apex:outputfield label="Opportunity" id="OpptyRead" value="{!qa.Opportunity__c}" rendered="{!OR(detailMode!='edit',canEdit==false) && qa.Type__c == 'Opportunity'}" >
                    <apex:inlineEditSupport disabled="true"/>
                </apex:outputfield>
                
            </apex:pageBlockSection>
            <!-- Case Details -->
            <apex:pageBlockSection >                
                <apex:outputField value="{!qa.Case_Type__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Case_Type__c}" taborderhint="5" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
            	                <apex:outputField value="{!qa.CLID__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.CLID__c}" taborderhint="7" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                <apex:outputField value="{!qa.Call_Duration__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Call_Duration__c}" taborderhint="5" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                <apex:outputField value="{!qa.Call_Recording_Link__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Call_Recording_Link__c}" taborderhint="8" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
            </apex:pageBlockSection>
             <!-- Case Details -->
            <apex:pageBlockSection >
                <apex:outputField value="{!qa.Comments__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" />
                <apex:inputField value="{!qa.Comments__c}" taborderhint="5" rendered="{!AND(detailMode=='edit',canEdit==true)}"
                                 						onchange="ShowCharCount(this, 32768, '{!$Component.myComm}');"
                    									onmousedown="ShowCharCount(this, 32768, '{!$Component.myComm}');"
                    									onkeyup="ShowCharCount(this, 32768, '{!$Component.myComm}');"
                    									onkeydown="ShowCharCount(this, 32768, '{!$Component.myComm}');"
                                 onclick="ShowCharCount(this, 32768, '{!$Component.myComm}');">&nbsp;&nbsp;
                <apex:outputPanel id="myComm" rendered="{!AND(detailMode=='edit',canEdit==true)}">32768 left</apex:outputPanel>
                </apex:inputField>
                <apex:outputField value="{!Quality_Assessment_Form__c.Total_Score__c}" />
                <apex:pageblockSectionItem /><apex:outputField value="{!Quality_Assessment_Form__c.Max_Score__c}" />
                <apex:pageblockSectionItem /><apex:outputField value="{!Quality_Assessment_Form__c.Percentage_Completed__c}" />
                <apex:pageblockSectionItem />
                <apex:outputField value="{!qa.AI_Behavioural_Score__c}" rendered="{!Quality_Assessment_Form__c.AI_Created__c}">
                	<apex:inlineEditSupport disabled="true"/>
                </apex:outputField>
                <apex:outputField value="{!qa.AI_Created__c}" rendered="{!Quality_Assessment_Form__c.AI_Created__c}">
                	<apex:inlineEditSupport disabled="true"/>
                </apex:outputField>
                <apex:outputField value="{!qa.Submit_Form__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}">
                    <apex:inlineEditSupport disabled="true"/>
                </apex:outputField>
                <!--<apex:inputField value="{!qa.Submit_Form__c}" taborderhint="10" rendered="{!AND(detailMode=='edit',canEdit==true)}"/>-->
                
                <!--<apex:pageBlockSectionItem >
<apex:outputLabel >Submit Form</apex:outputLabel>
<apex:actionRegion >
<apex:outputField value="{!qa.Submit_Form__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}"/>
<apex:inputField value="{!qa.Submit_Form__c}" taborderhint="10" rendered="{!AND(detailMode=='edit',canEdit==true)}">
<apex:actionSupport event="onchange" rerender="questionSection"/> 
</apex:inputField>
</apex:actionRegion>
</apex:pageBlockSectionItem> -->
                
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Critical Error" rendered="{!AND(seeCriticalErrorFields,Quality_Assessment_Form__c.Question_Set__c != null)}" columns="1" id="criticalErrorSection1">
                <apex:outputField value="{!qa.Profanity__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Profanity__c}" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                <apex:outputField value="{!qa.Security_Check__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Security_Check__c}" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                <apex:outputField value="{!qa.Rudeness__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Rudeness__c}" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                <apex:outputField value="{!qa.Unauthorized_Termination_of_Call__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Unauthorized_Termination_of_Call__c}" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                <apex:outputField value="{!qa.Misrepresentation_of_the_Company__c}" rendered="{!OR(detailMode!='edit',canEdit==false)}" /><apex:inputField value="{!qa.Misrepresentation_of_the_Company__c}" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Critical Error" rendered="{!AND(NOT(seeCriticalErrorFields),Quality_Assessment_Form__c.Question_Set__c != null)}" columns="1" id="criticalErrorSection2">                 
                <apex:repeat value="{!criticalErrorList}" var="crEr">
                    <apex:outputField value="{!crEr.flag.Flag__c}" label="{!crEr.err.Name}" rendered="{!OR(detailMode!='edit',canEdit==false)}"/><apex:inputField value="{!crEr.flag.Flag__c}" label="{!crEr.err.Name}" rendered="{!AND(detailMode=='edit',canEdit==true)}" />
                </apex:repeat>
            </apex:pageBlockSection>
            
            
            <br />
            <apex:pageBlockSection collapsible="true" id="sysInfo" title="System Information" rendered="{!detailMode!='edit'}">
                <apex:outputField value="{!Quality_Assessment_Form__c.CreatedById}" />
                <apex:outputField value="{!Quality_Assessment_Form__c.LastModifiedById}" />
                <apex:outputField value="{!Quality_Assessment_Form__c.CreatedDate}" />
                <apex:outputField value="{!Quality_Assessment_Form__c.LastModifiedDate}" />
                <script>twistSection(document.getElementById('{!$Component.detailSection.sysInfo}').getElementsByTagName('img')[0]);</script>
            </apex:pageBlockSection>
            
            <!--<apex:PageBlockSection title="Questions" columns="1" rendered="{!detailMode!='edit'}">-->
            <apex:PageBlockSection title="Questions" columns="1" rendered="{!qa.Question_Set__c != ''}">
                
                
                <apex:pageBlock mode="{!detailMode}" rendered="{!displayAnswers}" id="questionSection">
                    
                    <!--
<apex:pageBlockButtons location="both">

<apex:commandButton value="Answer Questions" action="{!answerQuestions}" rendered="{!mode!='edit' && (!qa.Submit_Form__c || isQAuser) && canEdit == true}" reRender="questionSection"/>
<apex:commandLink value="Save Answers" action="{!saveAnswers}" rendered="{!mode='edit'}" target="_top" styleClass="btn" style="text-decoration:none;padding:4px;" />
<apex:commandButton value="Cancel" action="{!cancelEdit}" rendered="{!mode='edit'}" reRender="questionSection"/>

</apex:pageBlockButtons>
-->
                    
                    <apex:repeat value="{!sectionList}" var="section">
                        <apex:pageBlockSection title="{!section.Section_Name__c}" columns="1" collapsible="false">
                            
                            
                            <apex:dataTable value="{!qAndAlist[section.id]}" var="question" style="width:95%;">
                                
                                <apex:column style="width:50%;">
                                    <apex:facet name="header">&nbsp;&nbsp;&nbsp;&nbsp;Question</apex:facet>
                                    &nbsp;&nbsp;
                                    
                                    <apex:pageBlockSection >
                                        <apex:pageBlockSectionItem dataTitle="{!question.answer.Question__r.Question_Tooltip__c}">
                                            <apex:outputPanel >
                                              	<apex:outputPanel rendered="{!question.answer.Question__r.Question_Tooltip__c != ''}">
                                                    <c:helpicon helpText="{!question.answer.Question__r.Question_Tooltip__c}"/>
                                                </apex:outputPanel>
                                                <apex:outputPanel rendered="{!question.answer.Question__r.Question_Tooltip__c == ''}">
                                                    <apex:outputLabel value="" rendered="false"/>
                                                </apex:outputPanel>
                                            </apex:outputPanel>
                                            <apex:outputField value="{!question.answer.Question_Text__c}"/>
                                        </apex:pageBlockSectionItem>
                                    </apex:pageBlockSection>
                                    
                                    
                                </apex:column>
                                
                                <apex:column style="width:5%;">
                                    <apex:facet name="header">Answer</apex:facet><br/>
                                    
                                    <apex:outputPanel rendered="{!question.answer.Question_Type__c != 'Picklist'}">
                                        <apex:outputField value="{!question.answer.Answer__c}" rendered="{!detailMode!='edit'}"/><apex:inputField value="{!question.answer.Answer__c}" rendered="{!AND(detailMode=='edit',canEdit==true)}"/>
                                    </apex:outputPanel>
                                    
                                    <apex:outputPanel rendered="{!question.answer.Question_Type__c == 'Picklist'}">
                                        <apex:outputField value="{!question.answer.Picklist_Answer__c}" rendered="{!detailMode!='edit'}"/>
                                        <apex:selectList value="{!question.answer.Picklist_Answer__c}" size="1" multiselect="false" required="false" rendered="{!AND(detailMode=='edit',canEdit==true)}">
                                            <apex:selectOptions value="{!question.pkListOptions}"/>
                                        </apex:selectList>
                                    </apex:outputPanel>
                                    
                                </apex:column>
                                
                                <apex:column style="width:10%;">
                                    <apex:outputLabel rendered="{!detailMode='edit'}">Comments&nbsp;&nbsp;</apex:outputLabel><br />
                                    <apex:outputField value="{!question.answer.Comments__c}" rendered="{!detailMode!='edit'}"/>
                                    <apex:inputTextArea value="{!question.answer.Comments__c}" rendered="{!AND(detailMode=='edit',canEdit==true)}" rows="1" 
                                                        onchange="ShowCharCount(this, 255, '{!$Component.myTASize}');"
                    									onmousedown="ShowCharCount(this, 255, '{!$Component.myTASize}');"
                    									onkeyup="ShowCharCount(this, 255, '{!$Component.myTASize}');"
                    									onkeydown="ShowCharCount(this, 255, '{!$Component.myTASize}');"
                                                        onclick="ShowCharCount(this, 255, '{!$Component.myTASize}');"/><br/>
                                    <apex:outputPanel id="myTASize" rendered="{!AND(detailMode=='edit',canEdit==true)}">255 left</apex:outputPanel><br/><br/>
                                </apex:column>
                                <apex:column style="width:5%;">
                                    <apex:facet name="header">n/a</apex:facet><br />
                                    <apex:outputField value="{!question.answer.N_A__c}" rendered="{!detailMode!='edit'}"/><apex:inputField value="{!question.answer.N_A__c}" rendered="{!detailMode='edit'}"/>
                                </apex:column>
                                
                            </apex:dataTable>
                            
                        </apex:pageBlockSection>
                    </apex:repeat>
                    
                </apex:pageBlock>
                
            </apex:PageBlockSection>
            
            <apex:pageBlockSection title="{!customLabel['Appeals_Section']}" collapsible="false" rendered="{!customLabel['Display_Appeals']}">
                <apex:outputField value="{!qa.Appeal_Reason_Long__c}" rendered="{!detailMode!='edit'}" label="{!customLabel['Appeal_Reason']}"/><apex:inputField value="{!qa.Appeal_Reason_Long__c}" taborderhint="11" rendered="{!AND(detailMode=='edit',qa.Submit_Form__c == true)}" label="{!customLabel['Appeal_Reason']}" style="width: 200px; height: 100px"/>
                <apex:commandLink value="{!customLabel['Submit_for_Appeal']}" action="{!submitForAppeal}" rendered="{!AND(detailMode!='edit', qa.Appeal_Reason_Long__c != null,qa.Submit_Form__c,!isLocked, !hasSuccessfullyAppealed)}"/>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
    </apex:form>
    
    <apex:PageBlock id="relLists" mode="maindetail" rendered="{!canRead}">
        <apex:relatedList list="ProcessSteps" rendered="{!AND(detailMode!='edit', customLabel['Display_Appeals'] =='true')}">
            <apex:facet name="header">Approval History</apex:facet>
        </apex:relatedList>
        <apex:relatedList list="Coaching_Items__r" rendered="{!detailMode!='edit'}" />
        <apex:relatedList list="QA_AI__r" rendered="{!AND(Quality_Assessment_Form__c.AI_Created__c == true && detailMode!='edit')}"/>
    </apex:PageBlock>
    
    
<script type="text/javascript">
 
    
{
    
    function openLookup(baseURL, width, modified, searchParam){
        var originalbaseURL = baseURL;
        var originalwidth = width;
        var originalmodified = modified;
        var originalsearchParam = searchParam;
    
        var lookupType = baseURL.substr(baseURL.length-3, 3);
        if (modified == '1') baseURL = baseURL + searchParam;

    	var isCustomLookup = false;

    	// Following "001" is the lookup type for Account object so change this as per your standard or custom object

          if(lookupType == "500" || lookupType == "006" ){
    
              var urlArr = baseURL.split("&");
              console.log('urlArr = '+ urlArr);
              var txtId = '';
              if(urlArr.length > 2) {
                urlArr = urlArr[1].split('=');
                txtId = urlArr[1];
              }
      		  //Following is the url of Custom Lookup page. You need to change that accordingly
              var id="{!Quality_Assessment_Form__c.Agent__r.User_Record__c}";
              baseURL = "/apex/CustomLookup?txt=" + txtId + "&agentId=" + id  /*+ "&id=" + "{!Quality_Assessment_Form__c.id}"*/;
              console.debug(baseURL);

              //Following is the id of apex:form control "myForm". You need to change that accordingly
              baseURL = baseURL + "&frm=" + escapeUTF("{!$Component.myForm}");
              if (modified == '1') {
                baseURL = baseURL + "&lksearch=" + searchParam;
              }
      		  // Following is the ID of inputField that is the lookup to be customized as custom lookup
              if(txtId.indexOf('Case') > -1 || txtId.indexOf('Oppty') > -1 ){
                isCustomLookup = true;
              }
    	}
        console.log('txtId = '+ txtId);
        console.log('isCustomLookup = '+ isCustomLookup);

        if(isCustomLookup == true){
          openPopup(baseURL, "lookup", 350, 480, "width="+width+",height=480,toolbar=no,status=no,directories=no,menubar=no,resizable=yes,scrollable=no", true);
        }
        else {
          if (modified == '1') originalbaseURL = originalbaseURL + originalsearchParam;
          openPopup(originalbaseURL, "lookup", 350, 480, "width="+originalwidth+",height=480,toolbar=no,status=no,directories=no,menubar=no,resizable=yes,scrollable=no", true);
        }
  	}
    
}

function ShowCharCount(myTA, maxSize, SizeLabel) {
	document.getElementById(SizeLabel).innerHTML = (maxSize - myTA.value.length) + ' chars left';
} 
    
 
</script>
    
    
</apex:page>