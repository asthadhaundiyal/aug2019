global class EmployeeFcrUpdate implements Schedulable, Database.Batchable<sObject>{

	global final String QUERY;
	global final Integer BATCH_SIZE = 25;
	global static String CRON_EXP = '0 0 0 3 9 ? 2022';
	global static Boolean IsRunning = false;
	
	global EmployeeFcrUpdate() {
	
		QUERY = 'select id from Employee__c where User_Record__r.IsActive = true';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		return Database.getQueryLocator(QUERY);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		IsRunning = true;
		
		Database.update(scope, false);
		
		IsRunning = false;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
    global void execute(SchedulableContext sc) {
		
		EmployeeFcrUpdate efu = new EmployeeFcrUpdate();
		Database.executeBatch(efu, BATCH_SIZE);
	}
}