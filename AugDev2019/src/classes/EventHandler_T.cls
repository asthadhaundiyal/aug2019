/**
 * Test Class for EventHandler
 */
@isTest
private class EventHandler_T {
    
    private static Employee__c emp,emp2;
    private static Coaching_Form__c cf, cf2;
    private static List<Goals__c> gs;
    private static List<ID> gsIds;
    private static Case c;
    private static NPS__c n;
    private static Quality_Assessment_Form__c q;
    
    /**
     * Setup test data
     */
    private static void setupData(){
        
        
        emp = new Employee__c();
        emp.First_Name__c = 'Test';
        emp.User_Record__c = userInfo.getUserId();
        emp.Has_In_Gage_Licence__c = true;
        insert emp;
        
        user u = [select id from user where id != :userInfo.getUserId() and isActive = true limit 1];
        emp2 = new Employee__c();
        emp2.First_Name__c = 'Test';
        emp2.User_Record__c = u.Id;
        emp2.Has_In_Gage_Licence__c = true;
        insert emp2;
        
        cf = new Coaching_Form__c();
        // set here required fields: cf.Required_Field__c = 'x'
        cf.Agent__c = emp.id;
        cf.Performance_Start_Date__c = Date.newInstance(system.today().year(), system.today().month(), 1);
        cf.Performance_End_Date__c = cf.Performance_Start_Date__c.addMonths(1);
        insert cf;
        
        cf2 = new Coaching_Form__c(
        	Agent__c = emp2.id, 
        	Performance_Start_Date__c = Date.newInstance(system.today().year(), system.today().month(), 1), 
    		Performance_End_Date__c = null
    	);
    	insert cf2;
        
        gs = new List<Goals__c>();
        
        gs.add(
            new Goals__c(
                Coaching_Form__c = cf.id,
                Goal_Description__c = 'Quality %'
            )
        );
        gs.add(new Goals__c(
                Coaching_Form__c = cf2.id,
                Goal_Description__c = 'Quality %'
            )
        );
        gs.add(
            new Goals__c(
                Coaching_Form__c = cf.id,
                Goal_Description__c = 'NPS %'
            )
        );
        gs.add(new Goals__c(
                Coaching_Form__c = cf2.id,
                Goal_Description__c = 'NPS %'
            )
        );
        gs.add(
            new Goals__c(
                Coaching_Form__c = cf.id,
                Goal_Description__c = 'First Contact Resolution %'
            )
        );
        gs.add(new Goals__c(
                Coaching_Form__c = cf2.id,
                Goal_Description__c = 'First Contact Resolution %'
            )
        );
        gs.add(
            new Goals__c(
                Coaching_Form__c = cf.id,
                Goal_Description__c = 'Employee Engagement Survey'
            )
        );
        gs.add(new Goals__c(
                Coaching_Form__c = cf2.id,
                Goal_Description__c = 'Employee Engagement Survey'
            )
        );
        insert gs;
        
        gsIds = new List<ID>();
        for(Goals__c goal : gs){
        
            gsIds.add(goal.id);
        }

        c = new Case();
        c.Status = [select MasterLabel from CaseStatus where isClosed = true limit 1].MasterLabel;
        //c.ClosedDate = system.today();
        c.status = 'Closed';
        c.in_gage__First_Employee__c = emp.id;
        insert c;
        
        n = new NPS__c();
        n.Employee__c = emp.id;
        n.Answer_1__c = 1;
        n.Answer_2__c = 2;
        insert n;
        
        q = new in_gage__Quality_Assessment_Form__c();
        q.Agent__c = emp.id;
        q.Submitted_Form_Date__c = system.today();
        insert q;
    }

    /**
     * Test handleEvents
     */
    static testMethod void handleEvents_Test(){
        
        setupData();
        
        List<Event> events = new List<Event>();
        
        // CF
        events.add(new Event(Event.EventType.CF_CHANGE, cf.Agent__c, cf.id, cf.Performance_Start_Date__c));
        
        // commented out because Coaching Forms no longer updated by Events created from CASE, QAS, or NPS trigger.  As per Rod 16/5/2016.
        /*
        // Case
        events.add(new .Event(.Event.EventType.CASE_CHANGE, c.First_Employee__c, c.id, Date.valueOf(c.ClosedDate)));
        
        // QAF
        events.add(new .Event(.Event.EventType.QAF_CHANGE, q.Agent__c, q.id, q.Submitted_Form_Date__c));
        
        // NPS
        events.add(new .Event(.Event.EventType.NPS_CHANGE, n.Employee__c, n.id, Date.valueOf(n.CreatedDate)));
        */
        // FCR
        events.add(new Event(Event.EventType.FCR_CHANGE, c.in_gage__First_Employee__c, c.id, Date.valueOf(c.ClosedDate)));
        
        Test.startTest();
        
            EventHandler.PREVENT_RECURSIVE_EXECUTION = false;
            EventHandler.handleEvents(events);
        
        Test.stopTest();
        
        for(Goals__c goal : [select id, Goal_Description__c, Performance__c  from Goals__c where id IN: gsIds]){
            
            if(goal.Goal_Description__c == 'Quality %'){
            
                system.assert(goal.Performance__c != null);
                continue;
            }else if(goal.Goal_Description__c == 'NPS %'){

                system.assert(goal.Performance__c != null);        
                continue;
            }else if(goal.Goal_Description__c == 'First Contact Resolution %'){
    
                system.assert(goal.Performance__c != null);        
                continue;
            }
        }
        
    }
    
    
    static testMethod void handleEvents_CFchangeTest(){
        
        changeCoachingForm(false);
        
    }
    
    
    static testMethod void handleEvents_CFchangeBatchTest(){
        
        changeCoachingForm(true);
        
    }
    
    
    static testMethod void handleEvents_CFchangeBatchBULKTest(){
        
        setupData();
        
        List<Event> events = new List<Event>();
        events.add(new Event(Event.EventType.CF_CHANGE, cf.Agent__c, cf.id, cf.Performance_Start_Date__c));
        events.add(new Event(Event.EventType.CF_CHANGE, cf2.Agent__c, cf2.id, cf2.Performance_Start_Date__c));
        
        Test.startTest();
		   EventHandler.PREVENT_RECURSIVE_EXECUTION = false;
	       EventHandler.testBatchCalculation = true;
	       EventHandler.handleEvents(events);
        Test.stopTest();
    }
    
    static void changeCoachingForm(boolean testBatchCalculation){
    
    	setupData();
        
        List<Event> events = new List<Event>();
        
        // CF
        events.add(new Event(Event.EventType.CF_CHANGE, cf.Agent__c, cf.id, cf.Performance_Start_Date__c));
        
        Test.startTest();
        
            EventHandler.PREVENT_RECURSIVE_EXECUTION = false;
            EventHandler.testBatchCalculation = testBatchCalculation;
            EventHandler.handleEvents(events);
        
        Test.stopTest();
        
        for(Goals__c goal : [select id, Goal_Description__c, Performance__c  from Goals__c where id IN: gsIds and Coaching_Form__c = :cf.id]){
            
            if(goal.Goal_Description__c == 'Quality %'){
            
                system.assert(goal.Performance__c != null);
                continue;
            }else if(goal.Goal_Description__c == 'NPS %'){

                system.assert(goal.Performance__c != null);        
                continue;
            }else if(goal.Goal_Description__c == 'First Contact Resolution %'){
    
                system.assert(goal.Performance__c != null);        
                continue;
            }
        }
    
    }
    
    
    
    
 /*   static testMethod void bulk100Cases(){
        
        setupData();
        
        integer testvolume = 10;
        
        list<case> newCases = new list<case>();
        for(integer i=0; i<(testvolume-1)*.8 ; i++){
        	newCases.add(new case(Status = 'Closed', First_Employee__c = emp.id, First_Case_Resolution__c = false));
        }
        for(integer i=0; i<(testvolume*.2); i++){
        	newCases.add(new case(Status = 'Closed', First_Employee__c = emp.id, First_Case_Resolution__c = true));
        }
        insert newCases;
        
        
        List<in_gage.Event> events = new List<in_gage.Event>();
        
        // CF
        events.add(new .Event(.Event.EventType.CF_CHANGE, cf.Agent__c, cf.id, cf.Performance_Start_Date__c));
    
    	Test.startTest();
            //in_gage.EventHandler.PREVENT_RECURSIVE_EXECUTION = false;
            in_gage.EventHandler.handleEvents(events);
        
        Test.stopTest();
    
    	Employee__c empCheck= [select id, FCR_30_days__c from Employee__c where id = :emp.id];
    	system.assertEquals(80, empCheck.FCR_30_days__c);
    
    	Goals__c goalCheck = [select id, Performance_Text__c , Goal_Description__c, Performance__c  from Goals__c where Goal_Description__c = 'First Contact Resolution %' and Coaching_Form__r.Agent__c = :emp.id and Coaching_Form__c = :cf.id];
    	system.assertEquals(0.8,goalCheck.Performance__c);
    
    }
*/
    
}