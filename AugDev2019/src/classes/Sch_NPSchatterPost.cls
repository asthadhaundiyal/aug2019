global class Sch_NPSchatterPost implements Schedulable {
	global void execute(SchedulableContext sc) {
		
		list<NPS__c> npsRecords = findNPSrecords();
	
		map<id,string> ownerNames = new map<id,string>();
        map<id,string> ownerUserNames = new map<id,string>();
		if(npsRecords != null)	ownerNames = getOwners(npsRecords);
		
	    List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
		id chatterGroupId = [SELECT Id FROM CollaborationGroup WHERE Name = 'Legendary Customer Service'].id;
		
		//list<FeedItem> feedItems = new list<FeedItem>();
		
	    for (NPS__c nps : npsRecords) {
            if(ownerNames!=null && ownerNames.get(nps.id)!=null){               
                //string message = ' Well done to '+ ownerNames.get(nps.id) + ' for a positive Net Promoter Score';
                string message = ' Well done for a positive Net Promoter Score';
                if(nps.Feedback_Comment__c!=null)
                   message += '\nThis is what the customer said: '+ nps.Feedback_Comment__c;
                
                ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        
                input.subjectId = nps.id;
                
                ConnectApi.MessageBodyInput body = new ConnectApi.MessageBodyInput();
                body.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
                ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
                textSegment.text = message;
                
                ConnectApi.MentionSegmentInput groupMentionSegment = new ConnectApi.MentionSegmentInput();
                groupMentionSegment.id = chatterGroupId;
                
                ConnectApi.MentionSegmentInput ownerMentionSegment = new ConnectApi.MentionSegmentInput();
                // ownerMentionSegment.username = ownerUserNames.get(nps.id);
                ownerMentionSegment.id = ownerNames.get(nps.id);
                
                body.messageSegments.add(groupMentionSegment);
                body.messageSegments.add(ownerMentionSegment);
                body.messageSegments.add(textSegment);
                
                input.body = body;
        
                ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(input);
                batchInputs.add(batchInput);
                //feedItems.add(new FeedItem(Body = message, ParentId = chatterGroupId, RelatedRecordId = nps.id));  
            }	        
	    }
	
	    ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
		//if(!feedItems.isEmpty()) insert feedItems;
		
		rescheduleJob();
		
	}
	
	private list<NPS__c> findNPSrecords(){
		
		map<id,feedItem> feedItemMap = new map<id,feedItem>([SELECT Body,Id,ParentId FROM FeedItem WHERE CreatedDate >= yesterday AND Type = 'TextPost']);
		
		//exclude the NPS records that already have a Well Done...
		set<id> npsIds = new set<id>();
		for(feedItem fi : feedItemMap.values()){
			if(fi.Body.contains('Well done to')) npsIds.add(fi.ParentId);
		}
		
		return [select id, case__c, opportunity__c, Feedback_Comment__c from NPS__c where id not in: npsIds and CreatedDate >= today and Answer_1__c=10];
	}
	
	/*private map<id,string> getOwners(list<NPS__c> npsRecords){
	
		set<id> recordIds = new set<id>();
		for(NPS__c nps : npsRecords){
			if(nps.case__c != null) recordIds.add(nps.case__c);
			if(nps.opportunity__c != null) recordIds.add(nps.opportunity__c);
		}
		map<id,string> ownerNames = new map<id,string>();
        map<id,string> ownerUserNames = new map<id, string>();
		for(Case c: [SELECT ID, Owner.FirstName, OwnerId, Owner.UserName from Case where Id IN:recordIds]){
                if(string.valueOf(c.OwnerId).left(3)=='005'){  // to ignore queue owners.
                	Case_Ignore_Status__c ignoreSetting = Case_Ignore_Status__c.getValues(c.OwnerId);
                	if(ignoreSetting == null){ // if user is not listed, then post positive NPS feed
                    	ownerNames.put(c.id,c.Owner.FirstName);
                        ownerUserNames.put(c.id, c.Owner.UserName);
                	}
            	}
		}
		for(Opportunity O: [SELECT ID, Owner.FirstName, OwnerId, Owner.UserName from Opportunity where Id IN:recordIds]){
                if(string.valueOf(o.OwnerId).left(3)=='005'){  // to ignore queue owners.
                	Case_Ignore_Status__c ignoreSetting = Case_Ignore_Status__c.getValues(o.OwnerId);
                	if(ignoreSetting == null){ // if user is not listed, then post positive NPS feed
                    	ownerNames.put(o.id,o.Owner.FirstName);
                        ownerUserNames.put(o.id, o.Owner.UserName);
                	}
            	}
		}
		map<id,string> returnMap = new map<id,string>();
		if(!ownerNames.isEmpty()){
			for(NPS__c nps : npsRecords){
				if(ownerNames.containsKey(nps.Case__c)) returnMap.put(nps.id, ownerNames.get(nps.Case__c));
				if(ownerNames.containsKey(nps.Opportunity__c)) returnMap.put(nps.id, ownerNames.get(nps.Opportunity__c));
			}
		}
		if(!returnMap.isEmpty()) return returnMap;
		
		return null;
	}*/
    private map<id,string> getOwners(list<NPS__c> npsRecords){
	
		set<id> recordIds = new set<id>();
		for(NPS__c nps : npsRecords){
			if(nps.case__c != null) recordIds.add(nps.case__c);
			if(nps.opportunity__c != null) recordIds.add(nps.opportunity__c);
		}
		map<id,string> ownerNames = new map<id,string>();
		for(Case c: [SELECT ID, Owner.FirstName, OwnerId, Owner.UserName from Case where Id IN:recordIds]){
                if(string.valueOf(c.OwnerId).left(3)=='005'){  // to ignore queue owners.
                	Case_Ignore_Status__c ignoreSetting = Case_Ignore_Status__c.getValues(c.OwnerId);
                	if(ignoreSetting == null){ // if user is not listed, then post positive NPS feed
                    	ownerNames.put(c.id,c.OwnerId);
                	}
            	}
		}
		for(Opportunity O: [SELECT ID, Owner.FirstName, OwnerId, Owner.UserName from Opportunity where Id IN:recordIds]){
                if(string.valueOf(o.OwnerId).left(3)=='005'){  // to ignore queue owners.
                	Case_Ignore_Status__c ignoreSetting = Case_Ignore_Status__c.getValues(o.OwnerId);
                	if(ignoreSetting == null){ // if user is not listed, then post positive NPS feed
                    	ownerNames.put(o.id,o.OwnerId);
                	}
            	}
		}
		map<id,string> returnMap = new map<id,string>();
		if(!ownerNames.isEmpty()){
			for(NPS__c nps : npsRecords){
				if(ownerNames.containsKey(nps.Case__c)) returnMap.put(nps.id, ownerNames.get(nps.Case__c));
				if(ownerNames.containsKey(nps.Opportunity__c)) returnMap.put(nps.id, ownerNames.get(nps.Opportunity__c));
			}
		}
		if(!returnMap.isEmpty()) return returnMap;
		
		return null;
	}
	
	@testVisible
	@future
	private static void rescheduleJob(){

		CronJobDetail cronJob = [SELECT Id,JobType,Name FROM CronJobDetail WHERE Name = 'In-gage Positive NPS Schedule'];
		if(cronJob != null){
			CronTrigger cron = [SELECT Id FROM CronTrigger WHERE CronJobDetailId = :cronJob.id];
			if(cron != null) System.abortJob(cron.id);
		}
		
		Sch_NPSchatterPost positiveNPS = new Sch_NPSchatterPost();
		/*datetime dt = datetime.now();
		dt = dt.addHours(1);
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';*/
		String schedule = '0 0 0,6,12,18 ? * *';
		system.schedule('In-gage Positive NPS Schedule', schedule , positiveNPS);
	
	}


}