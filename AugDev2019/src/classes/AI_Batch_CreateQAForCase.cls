global class AI_Batch_CreateQAForCase implements Database.Batchable<sObject>, Database.Stateful{
    // called in batch size of 1
    
    global final String query;
    global final set<id> caseIDs;
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    decimal aiQAFrequency = setting.QA_AI_Batch_Frequency__c;
    boolean batchQAAI = setting.Reduce_QA_AI_Procesing__c;
    boolean qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
    boolean qaAISEEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();


    global AI_Batch_CreateQAForCase(set<id> caseIDs){
        this.caseIDs = caseIDs;
        query = getCaseQuery() + ' where id in :caseIDs';
    }

    global AI_Batch_CreateQAForCase(string query){
        this.query = query;
    }
    
	private string getCaseQuery(){
        return 'select id, Origin, Type, Subject, Description, in_gage__AI_Language__c, Employee__c from Case';
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<QA_AI__c> qaAIList = new List<QA_AI__c>();
        List<Case> caseList = (List<Case>)scope;
        Set<Id> caseIds = new Set<Id>();
        for(case c: caseList) caseIds.add(c.id);
        map<Id,sObject> recordTypeEligibleMap = AIUtilityClass.getRecordTypeEligibleMap(caseIds);
        for(Case c :caseList){
            boolean forCC = checkIfAICCEligible(c, recordTypeEligibleMap);
            boolean forSE = checkIfAISEEligible(c, recordTypeEligibleMap); 
            system.debug('forCC: '+ forCC);
            system.debug('forSE: '+ forSE);
            if(qaAIEnabled && forCC){
                if(c.Description != null && c.in_gage__AI_Language__c!= null){
                    //clean description on case
                    system.debug('c.Description: '+ c.Description);
                    String processedDesc = getProcessedData(c.Description);
                    String processedSub = getProcessedData(c.Subject);
                    system.debug('processedDesc: '+ processedDesc);
                    //create new QA_AI record
                    qaAIList.add(new QA_AI__c(Parent_Case__c = c.Id
                                                       ,Text_to_Analyse__c = processedDesc
                                                       ,Text_to_Analyse_Subject__c = processedSub
                                                       ,Language__c = c.in_gage__AI_Language__c
                                                       ,Categorisation_Only__c = true
                                                       ,For_CC__c = forCC
                                                       ,For_SE__c = forCC
                                                       ,Source__c = 'Customer'
                                                       ,Employee__c = c.in_gage__Employee__c));
                    
                    system.debug('qaAIList: '+ qaAIList);
                } 
        	}
            if(!(qaAIEnabled && forCC) && qaAISEEnabled && forSE){
                if(c.Description != null && c.in_gage__AI_Language__c!= null){
                    //clean description on case
                    system.debug('c.Description: '+ c.Description);
                    String processedDesc = getProcessedData(c.Description);
                    String processedSub = getProcessedData(c.Subject);
                    system.debug('processedDesc: '+ processedDesc);
                    //create new QA_AI record
                    qaAIList.add(new QA_AI__c(Parent_Case__c = c.Id
                                                       ,Text_to_Analyse__c = processedDesc
                                                       ,Text_to_Analyse_Subject__c = processedSub
                                                       ,Language__c = c.in_gage__AI_Language__c
                                                       ,Categorisation_Only__c = true
                                                       ,For_CC__c = false
                                                       ,For_SE__c = forSE
                                                       ,Source__c = 'Customer'
                                                       ,Employee__c = c.in_gage__Employee__c));
                    
                    system.debug('qaAIList: '+ qaAIList);
                } 
        	}
        } 
        if(!qaAIList.isEmpty()) insert qaAIList;
    }
    private String getProcessedData(String description){
        String systemCleansed = '';
        String replacedData = '';
    	if(String.isNotBlank(description)){
            systemCleansed = removeSystemGeneratedData(description); 
            system.debug('systemCleansed: '+ systemCleansed);
            String url_pattern = 'https?://.*?\\s+';
        	String EMAIL_PATTERN = '([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)';                
        	replacedData = systemCleansed.replaceAll(url_pattern, '').replaceAll(EMAIL_PATTERN, ''); 
            system.debug('replacedData: '+ replacedData);
        }
        if(String.isNotBlank(replacedData))return replacedData.replaceAll('[0-9]', '').substringBefore('\n> ');   
        else return systemCleansed.replaceAll('[0-9]', '');
	}
    
    global void finish(Database.BatchableContext BC){
        
        //run QA Prediction batch
        if(qaAIEnabled || qaAISEEnabled){
            if(batchQAAI && aiQAFrequency!=null){
                /*DateTime startDT = DateTime.now();
                DateTime closeDT = startDT.addHours(-(aiQAFrequency.intValue()+1));*/
                
                Integer batchFrequency = aiQAFrequency.intValue() + 1;
                Datetime startDT = Datetime.now();    
            	DateTime closeDT = startDT.addHours(-batchFrequency);
            	system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);
                
                List<QA_AI__c> qaAIList = [select id from QA_AI__c where Categorisation_Only__c = true and Prediction_Completed__c = false and CreatedDate >= :closeDT AND CreatedDate <=:startDT];
                set<Id> qaAIIds = new set<Id>();
                for(QA_AI__c qa: qaAIList)qaAIIds.add(qa.id);
                if(!qaAIIds.isEmpty()) database.executebatch(new AI_Batch_QAPrediction(qaAIIds),100);
            }else{
                List<QA_AI__c> qaAIList = [select id from QA_AI__c where Categorisation_Only__c = true AND Parent_Case__c in :caseIds];         
                set<Id> qaAIIds = new set<Id>();
                for(QA_AI__c qa: qaAIList)qaAIIds.add(qa.id);
                if(!qaAIIds.isEmpty()) database.executebatch(new AI_Batch_QAPrediction(qaAIIds),1);
            }
            system.debug('batch finished'); 
        }
    } 
    private String removeSystemGeneratedData(String content){
        map<string,Email_For_Processing_Data__c> domainMap = Email_For_Processing_Data__c.getall();
        for(string dName : domainMap.keyset()) content = content.substringBefore('@'+ dName).substringBefore('--------------- Original Message');
        
        map<string,AI_Ignore_Texts__c > ignoreTextMap = AI_Ignore_Texts__c.getall();
        for(string t : ignoreTextMap.keyset()) content = content.replaceAll(t, '');
        system.debug('content: '+ content);
        return content;
    }
    private boolean checkIfAICCEligible(Case c, map<Id,sObject> recordTypeMap){
        boolean eligible = true;
        map<string,Case_Languages_for_AI__c> langSettingMap = Case_Languages_for_AI__c.getall();
        map<string,AI_Ignore_Case_Origin__c> orignSettingMap = AI_Ignore_Case_Origin__c.getall();
        map<string,Case_Types_to_Ignore_AI__c> typeSettingMap = Case_Types_to_Ignore_AI__c.getall();
        map<string,Case_RTypes_to_Ignore_AI__c> rTypeSettingMap = Case_RTypes_to_Ignore_AI__c.getall();
        if(c.in_gage__AI_Language__c!=null && !langSettingMap.isEmpty() && !langSettingMap.containsKey(c.in_gage__AI_Language__c)){
        	eligible = false;
        }
        if(c.Origin!=null && !orignSettingMap.isEmpty() && orignSettingMap.containsKey(c.Origin)){
        	eligible = false;
        }
        if(c.Type!=null && !typeSettingMap.isEmpty() && typeSettingMap.containsKey(c.Type)){
         	eligible = false;
        }
        if(recordTypeMap != null && recordTypeMap.containsKey(c.id)){
        	string recordTypeId = (String)recordTypeMap.get(c.Id).get('RecordTypeId');
            if(recordTypeId!=null && !rTypeSettingMap.isEmpty() && rTypeSettingMap.containsKey(recordTypeId)) eligible = false; 
        }
        return eligible;
    }
    private boolean checkIfAISEEligible(Case c, map<Id,sObject> recordTypeMap){
        boolean eligible = true;
        map<string,Languages_to_Support_for_AI_SE__c> langSettingMap = Languages_to_Support_for_AI_SE__c.getall();
        map<string,AI_SE_Ignore_Case_Origin__c> orignSettingMap = AI_SE_Ignore_Case_Origin__c.getall();
        map<string,AI_SE_Ignore_Case_RType__c> rTypeSettingMap = AI_SE_Ignore_Case_RType__c.getall();
        map<string,AI_SE_Ignore_Case_Type__c> typeSettingMap = AI_SE_Ignore_Case_Type__c.getall();
        if(c.in_gage__AI_Language__c!=null && !langSettingMap.isEmpty() && !langSettingMap.containsKey(c.in_gage__AI_Language__c)){
        	eligible = false;
        }
        if(c.Origin!=null && !orignSettingMap.isEmpty() && orignSettingMap.containsKey(c.Origin)){
        	eligible = false;
        }
        if(c.Type!=null && !typeSettingMap.isEmpty() && typeSettingMap.containsKey(c.Type)){
         	eligible = false;
        }
        if(recordTypeMap != null && recordTypeMap.containsKey(c.id)){
        	string recordTypeId = (String)recordTypeMap.get(c.Id).get('RecordTypeId');
            if(recordTypeId!=null && !rTypeSettingMap.isEmpty() && rTypeSettingMap.containsKey(recordTypeId)) eligible = false; 
        }
        return eligible;
    }
}