public with sharing class Employee_Trigger extends TriggerHandler {

    // *************************************
    // ******* Public Declarations *********
    // *************************************

    // ******************************
    // ******* Constructor **********
    // ******************************
    public Employee_Trigger() {}

    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {

		updateManagerEmployee((List<Employee__c>) newObjects, null);
        updateDefaultSurveyDay((List<Employee__c>) newObjects);
        updateNextSurveyDate((List<Employee__c>) newObjects);
        updateOwner((List<Employee__c>) newObjects, null);
        updateUserPhoto((List<Employee__c>) newObjects);
        updateStartDate((List<Employee__c>) newObjects);
    }

    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {

        updateDefaultSurveyDay((List<Employee__c>) newObjects);
        updateNextSurveyDate((List<Employee__c>) newObjects);
        updateOwner((List<Employee__c>) newObjects, (Map<Id, Employee__c>) oldMap);
        updateStartDate((List<Employee__c>) newObjects);
    	boolean surveyUpdate = checkForSurveyUpdate((List<Employee__c>) newObjects, (Map<Id, Employee__c>) oldMap);
        if(!surveyUpdate){
    		updateManagerEmployee((List<Employee__c>) newObjects, (Map<Id, Employee__c>) oldMap);
    		updateUserPhoto((List<Employee__c>) newObjects);
        }
    }

    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
      //if new Employee record type is Agent, create Agent's coaching form & goals
      List<Coaching_Form__c> formsToInsert=new List<Coaching_Form__c>();
      Map<ID, Coaching_Form__c> mapAgentCoachingForm=new Map<ID, Coaching_Form__c>();
      Map<ID, Coaching_Form__c> mapManagerCoachingForm=new Map<ID, Coaching_Form__c>();
      Map<ID, RecordType> recordTypes=new Map<ID, RecordType>([Select ID, Name from RecordType]);
      List<Employee__c> ingageEmployees=[SELECT ID, User_Record__c from Employee__c limit 50000];
      Map<ID, Employee__c> mapUserEmployees=new Map<ID, Employee__c>();
      for(Employee__c e: ingageEmployees)
      {
        mapUserEmployees.put(e.User_Record__c, e);
      }

      for(sObject obj: newObjects)
      {

        Employee__c emp=(Employee__c)obj;
        if(recordTypes.get(emp.RecordTypeId).Name=='Create New Employee')
        {
          Coaching_Form__c cForm=new Coaching_Form__c();
          if(emp.Manager_User__c!=null && mapUserEmployees.containsKey(emp.Manager_User__c))
            cForm.Manager__c=mapUserEmployees.get(emp.Manager_User__c).Id;
          cForm.Agent__c=emp.id;
          formsToInsert.add(cForm);
          mapAgentCoachingForm.put(obj.Id, cForm);          
        }
        else if(recordTypes.get(emp.RecordTypeId).Name=='Create New Manager')
        {
          Coaching_Form__c cForm=new Coaching_Form__c();
          if(emp.Manager_User__c!=null && mapUserEmployees.containsKey(emp.Manager_User__c))
            cForm.Manager__c=mapUserEmployees.get(emp.Manager_User__c).Id;
          cForm.Agent__c=emp.id;
          formsToInsert.add(cForm);
          mapManagerCoachingForm.put(obj.Id, cForm);
        }

      }
      // insert all the coaching form
      insert formsToInsert;
	  
      //In-gage features
      boolean qualityEnabled = FeatureConsoleAPI.qaEnabled();
      boolean npsEnabled = FeatureConsoleAPI.npsEnabled();
      boolean eeEnabled = FeatureConsoleAPI.eeEnabled();
      boolean fcrEnabled = FeatureConsoleAPI.fcrEnabled();
        
      // insert goals for Agent
      List<Goals__c> agentGoalsToInsert=new List<Goals__c>();
      for(ID employeeId: mapAgentCoachingForm.keySet())
      {
          Coaching_Form__c form=mapAgentCoachingForm.get(employeeId);
          if(fcrEnabled){
          	Goals__c agentGoals1 =new Goals__c();
          	agentGoals1.Goal_Description__c  ='First Contact Resolution %';
          	agentGoals1.Weighting__c=25;
          	agentGoals1.Coaching_Form__c=form.Id;
            agentGoalsToInsert.add(agentGoals1);
          }
		  
          if(npsEnabled){
          	Goals__c agentGoals2 =new Goals__c();
          	agentGoals2.Goal_Description__c  ='NPS %';
          	agentGoals2.Weighting__c=40;
          	agentGoals2.Coaching_Form__c=form.Id;
            agentGoalsToInsert.add(agentGoals2);
          }
          
          if(qualityEnabled){
          	Goals__c agentGoals3 =new Goals__c();
          	agentGoals3.Goal_Description__c  ='Quality %';
          	agentGoals3.Weighting__c=35;
          	agentGoals3.Coaching_Form__c=form.Id;
            agentGoalsToInsert.add(agentGoals3);
          }
      }
	  if(!agentGoalsToInsert.isEmpty()) insert agentGoalsToInsert;

      // insert goals for manager
      List<Goals__c> managerGoalsToInsert=new List<Goals__c>();
      for(ID employeeId: mapManagerCoachingForm.keySet())
      {
          Coaching_Form__c form=mapManagerCoachingForm.get(employeeId);
          
          if(eeEnabled){
              Goals__c managerGoals1 =new Goals__c();
              managerGoals1.Goal_Description__c    ='Employee Engagement Survey';
              managerGoals1.Weighting__c=30;
              managerGoals1.Coaching_Form__c=form.Id;
              managerGoalsToInsert.add(managerGoals1);
          }
          
          if(fcrEnabled){
              Goals__c managerGoals2 =new Goals__c();
              managerGoals2.Goal_Description__c    ='First Contact Resolution %';
              managerGoals2.Weighting__c=20;
              managerGoals2.Coaching_Form__c=form.Id;
              managerGoalsToInsert.add(managerGoals2);
          }
          if(qualityEnabled){
              Goals__c managerGoals3 =new Goals__c();
              managerGoals3.Goal_Description__c    ='Quality %';
              managerGoals3.Weighting__c=20;
              managerGoals3.Coaching_Form__c=form.Id;
              managerGoalsToInsert.add(managerGoals3);
          }
          if(npsEnabled){
              Goals__c managerGoals4 =new Goals__c();
              managerGoals4.Goal_Description__c    ='NPS %';
              managerGoals4.Weighting__c=30;
              managerGoals4.Coaching_Form__c=form.Id; 
              managerGoalsToInsert.add(managerGoals4);
          }
      }

      if(!managerGoalsToInsert.isEmpty()) insert managerGoalsToInsert;
      updateUserWithEmployeeId((List<Employee__c>) newObjects, null);
      

    }

    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
    	
    	boolean surveyUpdate = checkForSurveyUpdate((List<Employee__c>) newObjects, (Map<Id, Employee__c>) oldObjectsMap);
        if(!surveyUpdate){
    		updateHierarchy((List<Employee__c>) newObjects, (Map<Id, Employee__c>) oldObjectsMap);
        }
        boolean licenseChanged = checkIfLicenseChanged((List<Employee__c>) newObjects, (Map<Id, Employee__c>) oldObjectsMap);
        if(licenseChanged){
    		updateUserWithEmployeeId((List<Employee__c>) newObjects, (Map<Id, Employee__c>) oldObjectsMap);
        }
    }

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}


	// bypasses trigger actions if only the Suvery Reminder Date is blanked.
    private boolean checkForSurveyUpdate(List<Employee__c> newObjects, Map<Id, Employee__c> oldMap){
    	for(Employee__c e:newObjects){
    		if(e.Survey_Reminder_Date__c == null && oldMap.get(e.id).Survey_Reminder_Date__c != null) return true;
    	}
    	return false;
    }
    
    //update user record if license is removed only
    private boolean checkIfLicenseChanged(List<Employee__c> newObjects, Map<Id, Employee__c> oldMap){
    	for(Employee__c e:newObjects){
    		if(e.Has_In_Gage_Licence__c != oldMap.get(e.id).Has_In_Gage_Licence__c) return true;
    	}
    	return false;
    }
    

    // update user photo from Chatter
    private void updateUserPhoto(List<Employee__c> employeeList){
        set<id> userIDs=new set<id>();
        
        for(Employee__c e: employeeList){
          userIDs.add(e.User_Record__c);
        }
        Map<id,User> userMap= new Map<ID, User>([SELECT ID, SmallPhotoUrl from user where ID in: userIDs]);

        for(Employee__c e: employeeList){
        	if(userMap.containsKey(e.User_Record__c)){
		        string photoUrl = userMap.get(e.User_Record__c).SmallPhotoUrl;
		        //if(photoURL.contains('profilephoto/005/T')) continue;
		        e.Employee_Photo_Custom__c = photoURL;
		    }
        }
    }     
      

    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    private void updateDefaultSurveyDay(List<Employee__c> objects){

        for(Employee__c e : objects){

            if(e.Default_Survey_Day__c == null){

                e.Default_Survey_Day__c = randomWithLimit(27) + 1;
            }
        }
    }

    private void updateNextSurveyDate(List<Employee__c> objects){

        Date today = System.today();

        for(Employee__c e : objects){

            if(e.Next_Survey_Date__c == null || e.Next_Survey_Date__c < today || (e.Next_Survey_Date__c == today && e.Send_Engagement_Survey__c)){

                e.Next_Survey_Date__c = System.Date.newInstance(today.year(), today.month(), Integer.valueOf(e.Default_Survey_Day__c)).addMonths(1);
            }
        }
    }

    private void updateOwner(List<Employee__c> objects, Map<Id, Employee__c> oldObjectsMap){

        for(Employee__c e : objects){
            Employee__c oldE = (oldObjectsMap != null ? oldObjectsMap.get(e.id) : new Employee__c());

            if(e.User_Record__c!=null && (e.User_Record__c != oldE.User_Record__c || e.User_Record__c != e.OwnerId)){

                e.OwnerId = e.User_Record__c;
            }
        }
    }
    
    // update employee start date
    private void updateStartDate(List<Employee__c> employeeList){
        set<id> userIDs=new set<id>();
        
        for(Employee__c e: employeeList){
          userIDs.add(e.User_Record__c);
        }
        Map<id,User> userMap= new Map<ID, User>([SELECT ID, CreatedDate from user where ID in: userIDs]);

        for(Employee__c e: employeeList){
        	if(userMap.containsKey(e.User_Record__c)){
                system.debug('user created date '+ userMap.get(e.User_Record__c).CreatedDate);
		        e.Employee_Start_Date__c  = userMap.get(e.User_Record__c).CreatedDate;
                system.debug('employee sd '+e.Employee_Start_Date__c);
		    }
        }
    }	

    private Integer randomWithLimit(Integer upperLimit){

        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
    }
    
    private void updateManagerEmployee(List<Employee__c> objects, Map<Id, Employee__c> oldObjectsMap){

		List<Id> userIds = new List<Id>();
        for(Employee__c e : objects){
			
			Employee__c oldE = (oldObjectsMap!= null && oldObjectsMap.containsKey(e.Id) ? oldObjectsMap.get(e.Id) : new Employee__c());
            system.debug('e: '+ e);
            system.debug('oldE: '+ oldE);
			
			if(e.Manager_Employee__c == null && e.Manager_User__c != null || e.Manager_User__c != oldE.Manager_User__c){
				
				userIds.add(e.User_Record__c);
			}
        }
        
        system.debug('userIds: '+ userIds);
        
        if(userIds.size() > 0){
        
	        Map<Id, User> userMap = new Map<Id, User>([select Id, Manager.Employee_ID__c from User where Id IN: userIds]);
	        system.debug('userMap: '+ userMap);
        	for(Employee__c e : objects){
	        	
	        	if(userMap.containsKey(e.User_Record__c)){
	        		
		        	User u = userMap.get(e.User_Record__c);
                    system.debug('u: '+ u);
		        	
		        	if(u.Manager != null){
		        		
		        		e.Manager_Employee__c = u.Manager.Employee_ID__c;	
		        	}
	        	}
	        }
        }
    }
    
    private void updateHierarchy(List<Employee__c> objects, Map<Id, Employee__c> oldObjectsMap){
        
        // Get Managers to Calculate
        Set<Id> empToQuery = new Set<Id>();
        for(Employee__c e : objects){
        	
        	Employee__c oldE = oldObjectsMap.get(e.Id);
        	
        	if(e.Manager_Employee__c != oldE.Manager_Employee__c){
        		
        		if(e.Manager_Employee__c != null){
        			
        			empToQuery.add(e.Manager_Employee__c);
        		}
        		
        		if(oldE.Manager_Employee__c != null){
        			
        			empToQuery.add(oldE.Manager_Employee__c);
        		}
        	}
        	
        	if(e.Manager_Employee__c != null && e.Employees_in_Hierarchy__c != oldE.Employees_in_Hierarchy__c){
        		
        		empToQuery.add(e.Manager_Employee__c);
        	}
        }
        
        if(empToQuery.size() > 0){
	        
	        // Get Childs and Generate Hierarchy
	        Map<Id,Set<Id>> hierarchy = new Map<Id,Set<Id>>();
	        for(Employee__c childEmp : [select Id, Employees_in_Hierarchy__c, Manager_Employee__c from Employee__c where Manager_Employee__c IN: empToQuery]){
	        	
	        	if(hierarchy.containsKey(childEmp.Manager_Employee__c)){
	        		
	        		hierarchy.get(childEmp.Manager_Employee__c).add(childEmp.Id);
	        	}else{
	        		
	        		hierarchy.put(childEmp.Manager_Employee__c, new Set<Id> { childEmp.Id });
	        	}
	        	
	        	if(childEmp.Manager_Employee__c != null && childEmp.Employees_in_Hierarchy__c != null){
	        		
	        		hierarchy.get(childEmp.Manager_Employee__c).addAll((Set<Id>) JSON.deserialize(childEmp.Employees_in_Hierarchy__c, Set<Id>.class));
	        	}
	        }
	        
	        List<Employee__c> empToUpdate = new List<Employee__c>();
	        for(Id idE : empToQuery){
				
				Employee__c e = new Employee__c(Id = idE);
	            e.Employees_in_Hierarchy__c = JSON.serialize(hierarchy.get(idE));
	
	            empToUpdate.add(e);
	        }
	        
	        if(empToUpdate.size () > 0){
	        	
	        	Database.update(empToUpdate, false);
	        }
        }
    }
    //Update User Record with Employee ID - Process builder to Apex
    private void updateUserWithEmployeeId(List<Employee__c> objects, Map<Id, Employee__c> oldObjectsMap){

      set<id> userIds = new set<id>();
      List<User> userToQuery = new List<User>();
      list<User> userToUpdate = new list<User>();
      for(Employee__c emp : objects){
          if(emp.User_Record__c != null) userIds.add(emp.User_Record__c); 
      }
      if(!userIds.isEmpty()) userToQuery=[SELECT ID, Employee_ID__c  from User where id in :userIds];
        for(Employee__c emp : objects){
        	if(!userToQuery.isEmpty()){
          		for(User u : userToQuery){
                	string empUserId = emp.User_Record__c;
                    if(empUserId!= null && empUserId == u.Id){
                        if(emp.Has_In_Gage_Licence__c) u.Employee_ID__c = emp.Id;
                        else u.Employee_ID__c = null;
                        userToUpdate.add(u);
                    }                    
        		}     
        	}  
        }
        if(!userToUpdate.isEmpty()) update userToUpdate;
  
    }//Update User Record with Employee ID - Process builder to Apex

    private void createEvents(List<Employee__c> newObjects, Map<Id, Employee__c> oldObjectsMap) {
    
        for(Employee__c e : newObjects){
            
            Employee__c oldE = (oldObjectsMap != null ? oldObjectsMap.get(e.id) : new Employee__c());
            
            if(e.Manager_Employee__c != oldE.Manager_Employee__c){
                
				if(e.Manager_Employee__c != null){
					
					addEvent(e.Id, e.Id, System.today());
				}
				
				if(oldE.Manager_Employee__c != null){
					
					addEvent(oldE.Id, oldE.Id, System.today());
				}                		
            }
        }
    }

    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    private void addEvent(String agentID, String recID, Date dt){
        
        events.add(new Event(Event.EventType.CF_CHANGE, agentID, recID, dt));
    }
}