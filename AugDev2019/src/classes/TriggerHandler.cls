// Generic Trigger Handler John Jordan (John@airspeed.co.uk) 08/12/2015
//
public abstract class TriggerHandler 
{

    public static List<Event> events = new List<Event>();

    /**
     * @description Counters To Track How Often A Method On A Given Trigger Has Fired
     */
    private static map<string,integer> ActualRecursionsByMethod = new map<string,integer>();
    public boolean CheckRecursionsDuringTestCoverage = false;
    
    // prevent recursion within trigger instance.
    private static boolean executeBeforeUpdate = true;
    private static boolean executeAfterUpdate = true;
    
    private static boolean executeNPSAfterUpdate = true;
    
    public static boolean runBeforeUpdateOnce(){
        if(executeBeforeUpdate){
            executeBeforeUpdate=false;
            return true;
        }else{
            return executeBeforeUpdate;
        }
    }
    
    public static boolean runOnce(){
        if(executeAfterUpdate){
            executeAfterUpdate=false;
            return true;
        }else{
            return executeAfterUpdate;
        }
    }
    
    public static boolean runOnceNPS(){
        if(executeNPSAfterUpdate){
            executeNPSAfterUpdate=false;
            return true;
        }else{
            return executeNPSAfterUpdate;
        }
    }
    
    /**
     * @description Used to prevent the trigger handler from being executed more than once
     */
   // private static integer hasExecuted_Before = 0;
   // private static integer hasExecuted_After = 0; 

    /**
     * @description This will be called in the trigger before insert event
     * @param newObjects List of new sObjects to be inserted
     */
    public virtual void beforeInsert(List<SObject> newObjects) {}
        
    /**
     * @description This will be called in the trigger before update event
     * @param oldObjects List of the sObjects being updated with their original values
     * @param newObjects List of the sObjects being updated with their new values
     * @param oldMap Map of the sObjects being updated with their original values
     * @param newMap Map of the sObjects being updated with their new values
     */
    public virtual void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {}

    /**
     * @description This will be called in the trigger before delete event
     * @param objects List of the sObjects being deleted
     * @param objectsMap Map of the sObjects being deleted
     */
    public virtual void beforeDelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}

    /**
     * @description This will be called in the trigger after insert event
     * @param newObjects List of the sObjects being inserted
     * @param newObjectsMap Map of the sObjects being inserted
     */
    public virtual void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}
        
    /**
     * @description This will be called in the trigger after update event
     * @param oldObjects List of the sObjects being updated with their original values
     * @param newObjects List of the sObjects being updated with their new values
     * @param oldObjectsMap Map of the sObjects being updated with their original values
     * @param newObjectsMap Map of the sObjects being updated with their new values
     */
    public virtual void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {}

    /**
     * @description This will be called in the trigger after delete event
     * @param objects List of objects to be deleted
     * @param objectsMap Map of objects to be deleted
     */
    public virtual void afterDelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}

    /**
     * @description This will be called in the trigger after undelete event
     * @param objects List of objects which have been undeleted
     * @param objectsMap Map of objects which have been undeleted
     */
    public virtual void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}

    /**
     * @description This will call the relevant method in the trigger handler for the current trigger event
     */
    public void execute()
    {
        // Call the relevant trigger event method
        if (Trigger.isBefore) {             
            if (Trigger.isDelete) {
                beforeDelete(Trigger.old, Trigger.oldMap);
            } 
            else if (Trigger.isInsert) {
                beforeInsert(Trigger.new);
            }
            else if (Trigger.isUpdate) {
                beforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            }
        } 
        else {
            if (Trigger.isDelete) {
                afterDelete(Trigger.old, Trigger.oldMap);
            }
            else if (Trigger.isInsert) {
                afterInsert(Trigger.new, Trigger.newMap);
            }
            else if (Trigger.isUpdate) {
                afterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            }
            else if (Trigger.isUndelete) {
                afterUndelete(Trigger.new, Trigger.newMap);
            }
        }
        
        eventManager();
    }

    /**
     * @description This is called by the child class to prevent the trigger handler being
     *              executed recursively.
     * @param prevent Controls whether the trigger handler can be called recursively
    */
    public boolean IsMethodAllowedToRecurse(string key,integer MaxRecursions)
    { 
        system.debug('Checking if ' + key + ' is allowed to run again.');
        if (MaxRecursions==null) return true;
        //if (!MaximumRecursionsByMethod.containskey(key)) return true; // its not limited
        
        if (!ActualRecursionsByMethod.containskey(key))
        {
            system.debug('First Time Through');
            ActualRecursionsByMethod.put(key,1); // it has run once 
            return true;
        }
        else
        {
            if (CheckRecursionsDuringTestCoverage==true || !Test.isRunningTest())
            {
                ActualRecursionsByMethod.put(key,ActualRecursionsByMethod.get(key)+1);
            }
            if (ActualRecursionsByMethod.get(key)>MaxRecursions) return false;
            return true;
        }
    }

    public void eventManager() {
    
        try{
            
            integer eventsSize = events.size();
            
            if(eventsSize > 0){
            	EventHandler.handleEvents(events);
            }
        }catch(Exception e){}
    }
}