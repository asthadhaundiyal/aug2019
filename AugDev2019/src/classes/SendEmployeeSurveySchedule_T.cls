/**
 * Test Class for Case_Trigger
 */
@isTest
private class SendEmployeeSurveySchedule_T {

    /**
     *
     */
    static testMethod void test() {
      Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
  		UserRole ur = [SELECT Id FROM UserRole limit 1];

      Date today = system.today();
      
      ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
      setting.Employee_Engagement_Survey__c = '12345';
      upsert setting;
	
      Employee__c emp = new Employee__c();
      emp.First_Name__c = 'Test';
      emp.Default_Survey_Day__c = today.day();
      emp.Next_Survey_Date__c = today;

      insert emp;

      Employee__c empAssert = [select id, Next_Survey_Date__c, Send_Engagement_Survey__c from Employee__c where id =: emp.id];

      system.assert(empAssert.Next_Survey_Date__c == emp.Next_Survey_Date__c);

      Test.startTest();

        // Create new user with a non-null user role ID
      User u =[SELECT ID from User where Profile.Name='System Administrator' and isActive = true limit 1];

      emp.User_Record__c=u.Id;
      update emp;

      String jobId = System.schedule('SendEmployeeSurveySchedule', SendEmployeeSurveySchedule.CRON_EXP, new SendEmployeeSurveySchedule());

      Test.stopTest();

      empAssert = [select id, Next_Survey_Date__c, Send_Engagement_Survey__c from Employee__c where id =: emp.id];

      System.debug(empAssert.Next_Survey_Date__c);
      system.assert(empAssert.Next_Survey_Date__c == today.addMonths(1));
      system.assert(empAssert.Send_Engagement_Survey__c == false);
  }
}