global class Batch_Send_Survey_Reminders implements Database.Batchable<sObject>{
    
    global final String query;

	global Batch_Send_Survey_Reminders(string q){
		query=q;
	}

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
		list<Employee__c> eList; 
		try{ eList = (list<Employee__c>)scope;
		}catch(exception e){
		}
		
		list<Opportunity> oList;
		try{ oList = (list<Opportunity>)scope;
		}catch(exception e){
		}
		 
		list<Case> cList;
		try{ cList = (list<Case>)scope;
		}catch(exception e){
		}
		if(eList!=null){
			for(Employee__c e:eList){
				e.Send_Engagement_Survey__c = true;
			}
			update eList;
		}
		if(oList!=null){
			for(Opportunity o:oList){
				o.Opportunity_Won_Survey_Sent__c = false;
			}
			update oList;
		}
		if(cList!=null){

			// check owner is active and has Employee Id			
			set<id> ownerIds = new set<id>();
			for(case c: cList){
				if(string.valueOf(c.OwnerId).left(3)=='005') ownerIds.add(c.OwnerId);
			}
			map<id,user> uMap = new map<id,user>([select id, in_gage__Employee_ID__c, isActive from user where id in: ownerIds]);
            if(!uMap.isEmpty()){
			
				for(case c: cList){
                    if(uMap.get(c.OwnerId)!=null){
                        if(uMap.get(c.OwnerId).in_gage__Employee_ID__c == null || !uMap.get(c.OwnerId).isActive)c.in_gage__Survey_Reminder_Date__c = null;
                    }					
				}
			}
			
			// fire the reminder survey || blank reminder date.
			update cList;
		}

    }

    global void finish(Database.BatchableContext BC){
    }
    
    
}