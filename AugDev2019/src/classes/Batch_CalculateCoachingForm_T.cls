@isTest
public class Batch_CalculateCoachingForm_T {

    private static Employee__c emp;
    private static Coaching_Form__c cf;
    private static List<Goals__c> gs;
    private static list<opportunity> opptys;
    private static map<string,recordType> rtMap;
    private static string batchQuery;
        
    private static void setupData(){
        setupCustomSetting();
        
        batchQuery = 'select id, Performance_Start_Date__c,Performance_End_Date__c,Agent__c,Agent__r.User_Record__c, (select id,Goal__c,Performance__c,CalculationProgressWeight__c from Goals__r) from Coaching_Form__c where (Performance_End_Date__c = null OR Performance_End_Date__c >= LAST_N_DAYS:30) AND Agent__c != null AND Performance_Start_Date__c != null';
        
        emp = new Employee__c();
        emp.First_Name__c = 'Test';
        emp.User_Record__c = userInfo.getUserId();
        emp.Has_In_Gage_Licence__c = true;
        insert emp;
        
        cf = new Coaching_Form__c();
        cf.Agent__c = emp.id;
        cf.Performance_Start_Date__c = Date.newInstance(system.today().year(), system.today().month(), 1);
        cf.Performance_End_Date__c = cf.Performance_Start_Date__c.addMonths(1);
        insert cf;
        
        gs = new List<Goals__c>();
        gs.add(
            new Goals__c(
                Coaching_Form__c = cf.id,
                Goal_Name__c = 'WON %'
            )
        );
        gs.add(
            new Goals__c(
                Coaching_Form__c = cf.id,
                Goal_Name__c = 'CLOSED %'
            )
        );
        insert gs;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
       
        opptys = new list<opportunity>();
        opptys.add(new opportunity(name = 'sale1', Amount = 10, StageName = 'Closed Won', accountId = a.id, CloseDate  = date.today()));
        opptys.add(new opportunity(name = 'sale2', Amount = 2, StageName = 'Closed Lost', accountId = a.id, CloseDate = date.today()));
        opptys.add(new opportunity(name = 'sale3', Amount = 3, StageName = 'Closed Won', accountId = a.id, CloseDate = date.today()));
        opptys.add(new opportunity(name = 'sale4', Amount = 4, StageName = 'Prospecting', accountId = a.id, CloseDate = date.today()));
		insert opptys;

    }
    
    public static void setupCustomSetting(){
        
        /* Calculation Types = 
            '0/1' = the first returned value / the second. 
            't/ct' = true / record count 
            'rc' = record count enabled by the 'field' alias in the SOQL
        */
        list<Custom_Goal_Calculation__c> cgcList = new list<Custom_Goal_Calculation__c>();
        cgcList.add(new Custom_Goal_Calculation__c(name ='WON %',
                                                   SOQL_Select__c ='IsWon field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'IsClosed = true AND OwnerId in :ownerIds AND CloseDate >= <startDate> AND CloseDate <= <endDate>',
                                                   SOQL_GroupBy__c ='IsWon'
                                                   ,Calculation_Type__c = 't/ct',
                                                   Display_Performance_as_Percent__c  = true));
        cgcList.add(new Custom_Goal_Calculation__c(name ='CLOSED %',
                                                   SOQL_Select__c ='IsClosed field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId in :ownerIds AND CloseDate >= <startDate> AND CloseDate <= <endDate>',
                                                   SOQL_GroupBy__c ='IsClosed'
                                                   ,Calculation_Type__c = 't/ct',
                                                  Display_Performance_as_Percent__c  = false));
        
        
        /*
        
        cgcList.add(new Custom_Goal_Calculation__c(name ='ANP %',
                                                   SOQL_Select__c ='SUM(Sold_Amount__c),SUM(Target_Amount__c)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'IsWon = true AND OwnerId = <ownerId> AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>'
                                                   ,Calculation_Type__c = '0/1'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='Conversion Rate',
                                                   SOQL_Select__c ='SUM(Opportunity_Counter__c),SUM(Lead_Counter__c)',
                                                   SOQL_From__c ='Conversion__c',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Event_Date__c >= <startDate> AND Event_Date__c <= <endDate>'
                                                   ,Calculation_Type__c = '0/1'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='H&W Module',
                                                   SOQL_Select__c ='Health_and_Wellbeing__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c ='Health_and_Wellbeing__c'
                                                   ,Calculation_Type__c = 't/ct'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='Med Evac Module',
                                                   SOQL_Select__c ='Medical_Evacuation__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c = 'Medical_Evacuation__c'
                                                   ,Calculation_Type__c = 't/ct'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='Vision and Dental Modules',
                                                   SOQL_Select__c ='Vision_and_Dental__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c = 'Vision_and_Dental__c'
                                                   ,Calculation_Type__c = 't/ct'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='Outpatient Module',
                                                   SOQL_Select__c ='Outpatient__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c ='Outpatient__c'
                                                   ,Calculation_Type__c = 't/ct'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='US Cover Module',
                                                   SOQL_Select__c ='US_Cover__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c = 'US_Cover__c'
                                                   ,Calculation_Type__c = 't/ct'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='Platinum Core Plan',
                                                   SOQL_Select__c ='Core_Plan__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c = 'Core_Plan__c'
                                                   ,Calculation_Type__c = 'Platinum'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='Gold Core Plan',
                                                   SOQL_Select__c ='Core_Plan__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c = 'Core_Plan__c'
                                                   ,Calculation_Type__c = 'Gold'));
        cgcList.add(new Custom_Goal_Calculation__c(name ='Silver Core Plan',
                                                   SOQL_Select__c ='Core_Plan__c field,COUNT(Id)',
                                                   SOQL_From__c ='Opportunity',
                                                   SOQL_Where__c = 'OwnerId = <ownerId> AND Quote_Status__c = \'Policy Paid\' AND Cool_Off__c = false AND Policy_Paid_Date__c >= <startDate> AND Policy_Paid_Date__c <= <endDate>',
                                                   SOQL_GroupBy__c = 'Core_Plan__c'
                                                   ,Calculation_Type__c = 'Silver'));
        
        
        */
        
        
        insert cgcList;
        
    }
    
    static testMethod void testWON(){
        setupdata();
        
        test.startTest();
        database.executeBatch(new Batch_CalculateCoachingForm(batchQuery),1);
        test.stopTest();
        system.assertEquals(67,[select Performance__c from Goals__c where Goal_Name__c = 'WON %' limit 1].Performance__c);
    }
    
    static testMethod void testCLOSED(){
        setupdata();
        
        test.startTest();
        database.executeBatch(new Batch_CalculateCoachingForm(batchQuery),1);
        test.stopTest();
        system.assertEquals(1,[select Performance__c from Goals__c where Goal_Name__c = 'CLOSED %' limit 1].Performance__c);
    }
        
    static testMethod void testCoachingFormServices(){
    	setupdata();
        
        test.startTest();
    	CoachingFormServices.recalculate(cf.id);
        test.stopTest();
        system.assertEquals(67,[select Performance__c from Goals__c where Goal_Name__c = 'WON %' limit 1].Performance__c);
        system.assertEquals(false,[select isCalculating__c from Coaching_Form__c where id = :cf.id].isCalculating__c);
        
    }
    
}