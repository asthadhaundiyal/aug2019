@isTest
public class Analytics_Trigger_T {

	private static list<opportunity> opptys = new list<opportunity> ();
	private static list<case> cases = new list<case>();
    private static account acc1;
    private static account acc2;
	private static Employee__c emp;
	private static NPS__c n;
    private static ingage_Application_Settings__c setting;
    private static Case_Types_to_Run_Case_Trigger__c  cTypeSetting;
    private static Opp_Types_to_Run_Opp_Trigger__c  oTypeSetting;

	// replicate won oppty
	private static testmethod void wonOppty(){
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            setting = new ingage_Application_Settings__c();
            setting.Loyal_Customer__c = 7;
            
            oTypeSetting = new Opp_Types_to_Run_Opp_Trigger__c();
            oTypeSetting.Name = 'Other';
            insert oTypeSetting;
            
            acc1 = new account(name = 'test Account1');
            insert acc1;
            contact con1 = new contact(lastName = 'test Contact1', accountId = acc1.id);
            insert con1;
            
            acc2 = new account(name = 'test Account2');
            insert acc2;
            contact con2 = new contact(lastName = 'test Contact2', accountId = acc2.id);
            insert con2;
    
            opptys.add(new opportunity(accountid = acc1.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con1.id, name = 'test Oppty', type='Other'));
            opptys.add(new opportunity(accountid = acc2.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con2.id, name = 'test Oppty2', type='Other'));
    
            emp = new Employee__c();
            emp.First_Name__c = 'Test';
            insert emp;
            
            n = new NPS__c(Employee__c = emp.id, Answer_1__c = 1);
    
            test.startTest();
            insert opptys;
            test.stopTest();
            
            list<Analytics__c> aList = [select id,stat_Case__c,stat_Case_NPS__c,stat_NPS__c,stat_Oppty__c,stat_Oppty_NPS__c,stat_Promoter__c,stat_Detractor__c from Analytics__c where Opportunity__c = :opptys[0].id];
            system.assertEquals(1, aList.size());
            
            Analytics__c a = aList[0];
            system.assertEquals(null, a.stat_Case__c);
            system.assertEquals(null, a.stat_Case_NPS__c);
            system.assertEquals(null, a.stat_NPS__c);
            system.assertEquals(1, a.stat_Oppty__c);
            system.assertEquals(null, a.stat_Oppty_NPS__c);
            system.assertEquals(null, a.stat_Promoter__c);
            system.assertEquals(null, a.stat_Detractor__c);
        }
		
	}

	// replicate won case
	private static testmethod void wonCase(){
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            setting = new ingage_Application_Settings__c();
            setting.Loyal_Customer__c = 7;
            setting.Case_Types_for_Survey__c = 'Mechanical;Electrical;Electronic;Structural;Other;';
            insert setting;
            
            cTypeSetting = new Case_Types_to_Run_Case_Trigger__c();
            cTypeSetting.Name = 'Other';
            insert cTypeSetting;
            
            acc1 = new account(name = 'test Account1');
            insert acc1;
            contact con1 = new contact(lastName = 'test Contact1', accountId = acc1.id);
            insert con1;
            
            acc2 = new account(name = 'test Account2');
            insert acc2;
            contact con2 = new contact(lastName = 'test Contact2', accountId = acc2.id);
            insert con2;
    
            cases.add(new case(accountid = acc1.id, subject ='test Case', status = 'Closed', type='Other'));
            cases.add(new case(accountid = acc2.id, subject ='test Case2', status = 'Closed', type='Other'));
            
            emp = new Employee__c();
            emp.First_Name__c = 'Test';
            insert emp;
            
            n = new NPS__c(Employee__c = emp.id, Answer_1__c = 1);
            test.startTest();
            insert cases;
            test.stopTest();
    
            list<Analytics__c> aList = [select id,stat_Case__c,stat_Case_NPS__c,stat_NPS__c,stat_Oppty__c,stat_Oppty_NPS__c,stat_Promoter__c,stat_Detractor__c from Analytics__c where Case__c = :cases[0].id];
            /*system.assertEquals(1, aList.size());
            
            Analytics__c a = aList[0];
            system.assertEquals(1, a.stat_Case__c);
            system.assertEquals(null, a.stat_Case_NPS__c);
            system.assertEquals(null, a.stat_NPS__c);
            system.assertEquals(null, a.stat_Oppty__c);
            system.assertEquals(null, a.stat_Oppty_NPS__c);
            system.assertEquals(null, a.stat_Promoter__c);
            system.assertEquals(null, a.stat_Detractor__c);*/
        }
	
	}
	/**/
	// replicate completed NPS for Opportunity
	private static testmethod void wonOpptyNewNPS(){
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            setting = new ingage_Application_Settings__c();
            setting.Loyal_Customer__c = 7;
            
            oTypeSetting = new Opp_Types_to_Run_Opp_Trigger__c();
            oTypeSetting.Name = 'Other';
            insert oTypeSetting;
            
            acc1 = new account(name = 'test Account1');
            insert acc1;
            contact con1 = new contact(lastName = 'test Contact1', accountId = acc1.id);
            insert con1;
            
            acc2 = new account(name = 'test Account2');
            insert acc2;
            contact con2 = new contact(lastName = 'test Contact2', accountId = acc2.id);
            insert con2;
    
            opptys.add(new opportunity(accountid = acc1.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con1.id, name = 'test Oppty', type='Other'));
            opptys.add(new opportunity(accountid = acc2.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con2.id, name = 'test Oppty2', type='Other'));
    
            emp = new Employee__c();
            emp.First_Name__c = 'Test';
            insert emp;
            
            n = new NPS__c(Employee__c = emp.id, Answer_1__c = 1);
            test.startTest();
            insert opptys;
            n.Opportunity__c = opptys[0].id;
            n.Account__c = acc1.id;
            insert n;
            system.assertEquals(1,[select count() from NPS__c where id=:n.id]);		
            test.stopTest();
            
            list<Analytics__c> aList = [select id,stat_Case__c,stat_Case_NPS__c,stat_NPS__c,stat_Oppty__c,stat_Oppty_NPS__c,stat_Promoter__c,stat_Detractor__c from Analytics__c where Opportunity__c = :opptys[0].id];
            system.assertEquals(1, aList.size());
            
            Analytics__c a = aList[0];
            system.assertEquals(null, a.stat_Case__c);
            system.assertEquals(null, a.stat_Case_NPS__c);
            system.assertEquals(1, a.stat_NPS__c);
            //system.assertEquals(1, a.stat_Oppty__c);
            system.assertEquals(1, a.stat_Oppty_NPS__c);
        }
	}

	// replicate completed NPS for Case
	private static testmethod void wonCaseNewNPS(){
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            setting = new ingage_Application_Settings__c();
            setting.Loyal_Customer__c = 7;
            setting.Case_Types_for_Survey__c = 'Mechanical;Electrical;Electronic;Structural;Other;';
            insert setting;
            
            cTypeSetting = new Case_Types_to_Run_Case_Trigger__c();
            cTypeSetting.Name = 'Other';
            insert cTypeSetting;
            
            acc1 = new account(name = 'test Account1');
            insert acc1;
            contact con1 = new contact(lastName = 'test Contact1', accountId = acc1.id);
            insert con1;
            
            acc2 = new account(name = 'test Account2');
            insert acc2;
            contact con2 = new contact(lastName = 'test Contact2', accountId = acc2.id);
            insert con2;
    
            cases.add(new case(accountid = acc1.id, subject ='test Case', status = 'Closed', type='Other'));
            cases.add(new case(accountid = acc2.id, subject ='test Case2', status = 'Closed', type='Other'));
            
            emp = new Employee__c();
            emp.First_Name__c = 'Test';
            insert emp;
            
            n = new NPS__c(Employee__c = emp.id, Answer_1__c = 1);
            test.startTest();
            insert cases;
            n.Case__c = cases[0].id;
            n.Account__c = acc1.id;
            n.Answer_2__c = 9;
            insert n;
            test.stopTest();
            
            list<Analytics__c> aList = [select id,stat_Case__c,stat_Case_NPS__c,stat_NPS__c,stat_Oppty__c,stat_Oppty_NPS__c,stat_Promoter__c,stat_Detractor__c,Loyal_Customer__c from Analytics__c where recordId__c like :acc1.id];
            /*system.assertEquals(1, aList.size());
            
            Analytics__c a = aList[0];
            system.assertEquals(1, a.stat_Case__c);
            system.assertEquals(1, a.stat_Case_NPS__c);
            system.assertEquals(1, a.stat_NPS__c);
            system.assertEquals(null, a.stat_Oppty__c);
            system.assertEquals(null, a.stat_Oppty_NPS__c);
            system.assertEquals(null, a.stat_Promoter__c);
            system.assertEquals(1, a.stat_Detractor__c);	
            system.assertEquals(1, a.Loyal_Customer__c);*/
        }
	}/**/

	// replicate completed NPS for Opportunity
	private static testmethod void wonOpptyExistingNPS(){
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            setting = new ingage_Application_Settings__c();
            setting.Loyal_Customer__c = 7;
            
            oTypeSetting = new Opp_Types_to_Run_Opp_Trigger__c();
            oTypeSetting.Name = 'Other';
            insert oTypeSetting;
            
            acc1 = new account(name = 'test Account1');
            insert acc1;
            contact con1 = new contact(lastName = 'test Contact1', accountId = acc1.id);
            insert con1;
            
            acc2 = new account(name = 'test Account2');
            insert acc2;
            contact con2 = new contact(lastName = 'test Contact2', accountId = acc2.id);
            insert con2;
    
            opptys.add(new opportunity(accountid = acc1.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con1.id, name = 'test Oppty', type='Other'));
            opptys.add(new opportunity(accountid = acc2.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con2.id, name = 'test Oppty2', type='Other'));
    
            emp = new Employee__c();
            emp.First_Name__c = 'Test';
            insert emp;
            
            n = new NPS__c(Employee__c = emp.id, Answer_1__c = 1);
            insert opptys;
            n.Opportunity__c = opptys[1].id;
            n.Answer_1__c = 5;
            insert n;
            Analytics__c a2 = new Analytics__c(recordId__c = opptys[1].id +' ' + n.id, Opportunity__c = opptys[1].id, NPS__c = n.id);
            insert a2;
            
            test.startTest();
            n.Opportunity__c = opptys[1].id;
            n.Answer_1__c = 9;
            update n;
            test.stopTest();
            
            list<Analytics__c> aList2 = [select id,stat_Case__c,stat_Case_NPS__c,stat_NPS__c,stat_Oppty__c,stat_Oppty_NPS__c,stat_Promoter__c,stat_Detractor__c from Analytics__c where Opportunity__c = :opptys[1].id];
            //system.assertEquals(1, aList2.size());
            
            /*Analytics__c a = aList2[1];
            system.assertEquals(null, a.stat_Case__c);
            system.assertEquals(null, a.stat_Case_NPS__c);
            system.assertEquals(1, a.stat_NPS__c);
            system.assertEquals(1, a.stat_Oppty__c);
            system.assertEquals(1, a.stat_Oppty_NPS__c);
            system.assertEquals(1, a.stat_Promoter__c);
            system.assertEquals(null, a.stat_Detractor__c);*/
        }
	}

	// replicate completed NPS for Case
	private static testmethod void wonCaseExistingNPS(){
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            setting = new ingage_Application_Settings__c();
            setting.Loyal_Customer__c = 7;
            setting.Case_Types_for_Survey__c = 'Mechanical;Electrical;Electronic;Structural;Other;';
            insert setting;
            
            cTypeSetting = new Case_Types_to_Run_Case_Trigger__c();
            cTypeSetting.Name = 'Other';
            insert cTypeSetting;
            
            acc1 = new account(name = 'test Account1');
            insert acc1;
            contact con1 = new contact(lastName = 'test Contact1', accountId = acc1.id);
            insert con1;
            
            acc2 = new account(name = 'test Account2');
            insert acc2;
            contact con2 = new contact(lastName = 'test Contact2', accountId = acc2.id);
            insert con2;
    
            cases.add(new case(accountid = acc1.id, subject ='test Case', status = 'Closed', type='Other'));
            cases.add(new case(accountid = acc2.id, subject ='test Case2', status = 'Closed', type='Other'));
            
            emp = new Employee__c();
            emp.First_Name__c = 'Test';
            insert emp;
            
            n = new NPS__c(Employee__c = emp.id, Answer_1__c = 1);
            insert cases;
            n.Case__c = cases[1].id;
            n.Account__c = acc2.id;
            insert n;
            Analytics__c a2 = new Analytics__c(recordId__c = cases[1].id +' ' + n.id, Case__c = cases[1].id, NPS__c = n.id);
            insert a2;		
            test.startTest();
            
            n.Case__c = cases[1].id;
            n.Answer_1__c = 7;
            update n;
            test.stopTest();
            
            list<Analytics__c> aList2 = [select id,stat_Case__c,stat_Case_NPS__c,stat_NPS__c,stat_Oppty__c,stat_Oppty_NPS__c,stat_Promoter__c,stat_Detractor__c from Analytics__c where Case__c = :cases[1].id];
            //system.assertEquals(1, aList2.size());
            
            /*Analytics__c a = aList2[1];
            system.assertEquals(1, a.stat_Case__c);
            system.assertEquals(1, a.stat_Case_NPS__c);
            system.assertEquals(1, a.stat_NPS__c);
            system.assertEquals(null, a.stat_Oppty__c);
            system.assertEquals(null, a.stat_Oppty_NPS__c);
            system.assertEquals(null, a.stat_Promoter__c);
            system.assertEquals(null, a.stat_Detractor__c);*/
         }
	}
	
}