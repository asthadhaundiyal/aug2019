/* Create NPS AI Feedback(Training) Data - Batch */

global class AI_Batch_CreateNPSTrainingData implements Database.Batchable<sObject>, Database.Stateful{
    
    global final String query;
    global final Map<String, Set<String>> fbMap;
    global final Map<String, String> npsLangMap = createNPSLanguageMap();
    global final set<Id> npsAIFBIds = new set<Id>();
    
    global AI_Batch_CreateNPSTrainingData(String q){
        query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<NPS__c> npsList = (List<NPS__c>)scope;
        List<NPS_AI_Feedback__c> npsAIFList = new List<NPS_AI_Feedback__c>();
        
        for(NPS__c nps :npsList){
            //clean feedback comment on nps
            String fComment = nps.Feedback_Comment__c;
            system.debug('fComment: '+ fComment);
            if(fComment!= null){
                String aiLang = nps.AI_Language__c;
                String surveyLang = nps.Language__c;
                String trainLang = '';
                if(aiLang != null) trainLang = aiLang;
                else if(npsLangMap.containsKey(nps.Language__c)) trainLang = npsLangMap.get(nps.Language__c);
                
                system.debug('aiLang: '+ aiLang);
                system.debug('surveyLang: '+ surveyLang);
                system.debug('trainLang: '+ trainLang);
                
                String processedData = getProcessedFeedback(fComment);
                String processedReason = '';
                system.debug('Cleaned Data: '+ processedData);
                //update nps reasons
                if(processedData!= null && trainLang != ''){
                    if(nps.Reason__c != null) processedReason = nps.Reason__c;  
                    else{
                        //get the nps feedback type values for each type for the training language      
                        Map<String, NPS_Feedback_Types__c> npsReasonsMap = NPS_Feedback_Types__c.getAll();
                        List<String> npsReasonList = new List<String>();
                        npsReasonList.addAll(npsReasonsMap.keySet());
                        
                        Map<String, Set<String>> npsMap = new Map<String, Set<String>>();
                        for(String feedbackName : npsReasonsMap.keySet()){
                            if(feedbackName.containsIgnoreCase(trainLang)){                  
                                Set<String> valuesSet = new Set<String>();
                                String feedbackType = npsReasonsMap.get(feedbackName).Type__c;
                                String searchText = npsReasonsMap.get(feedbackName).Values__c;
                                List<String> splitText = searchText.split(',');
                                for(String s : splitText)valuesSet.add(s.trim());
                                npsMap.put(feedbackType, valuesSet); 
                            }
                            
                        }
                        system.debug('npsMap: '+ npsMap);
                        if(!npsMap.isEmpty()){
                            String npsReason = '';
                            Set<String> npsReasonsSet = null;
                            List<String> partsList = processedData.toLowerCase().split('[\'\"\\n\\s,;@&.?$+-]+');
                            for(String fbType : npsMap.keySet()){
                                if(!partsList.isEmpty()){
                                    for(String s: partsList){
                                        if(npsMap.get(fbType).contains(s)){
                                            npsReason += fbType+', ';
                                        }
                                    }
                                }
                            } 
                            if(npsReason!=null && npsReason!=''){
                                npsReasonsSet = new Set<String>(npsReason.split(', '));
                            }
                            if(npsReasonsSet!=null){
                                for(String s: npsReasonsSet)
                                    processedReason  += s +', ';
                            }else processedReason = 'Other';
                        }else processedReason = 'Other';              
                    }
                }
                
                //create new NPS AI Feedback record
                npsAIFList.add(new NPS_AI_Feedback__c(NPS__c = nps.Id
                                                      ,Text_for_Training__c = processedData             
                                                      ,Language__c = trainLang
                                                      ,Category__c = processedReason
                                                      ,Added_for_Training__c = true)); 
            }
        }
        Database.SaveResult[] srList = Database.insert(npsAIFList);
        for (Database.SaveResult sr : srList) npsAIFBIds.add(sr.getId());
    }
    
    global void finish(Database.BatchableContext BC){
        
        //run lanugage detection batch
        Set<Id> npsAIFIds = new Set<Id>();
        for(NPS_AI_Feedback__c nps : [select id, Text_for_Training__c, Language__c, Category__c  from NPS_AI_Feedback__c where id IN :npsAIFBIds]){
            if(nps.Language__c == null) npsAIFIds.add(nps.Id);
        }        
        if(npsAIFIds.size() > 0){
            system.debug('executing AI_Batch_NPSTrainingLanguageDetection');
            database.executebatch(new AI_Batch_NPSTrainingLanguageDetection(npsAIFIds),1);
        }
    }
    
    //Helper methods
    private String getProcessedFeedback(String npsFeedback){
        String systemCleansed = '';
        String processedFeedback = '';
        if(String.isNotBlank(npsFeedback)){
            systemCleansed = removeSystemGeneratedData(npsFeedback);
            system.debug('systemCleansed: '+ systemCleansed);
            String url_pattern = 'https?://.*?\\s+';
            String EMAIL_PATTERN = '([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)';                
            processedFeedback = systemCleansed.replaceAll(url_pattern, '').replaceAll(EMAIL_PATTERN, ''); 
        }
        if(String.isNotBlank(processedFeedback))return processedFeedback.replaceAll('[0-9]', '');   
        else return systemCleansed.replaceAll('[0-9]', '');
    }
    private String removeSystemGeneratedData(String content){
        map<string,AI_Ignore_Texts__c > ignoreTextMap = AI_Ignore_Texts__c.getall();
        for(string t : ignoreTextMap.keyset()) content = content.replaceAll(t, '');
        system.debug('content: '+ content);
        return content;
    }
    
    private Map<String, String> createNPSLanguageMap(){
        Map<String, NPS_Feedback_Types__c> typeSettingMap = NPS_Feedback_Types__c.getAll();
        Map<String, String> langMap = new Map<String, String>();
        for(String feedbackName : typeSettingMap.keySet()){
            Set<String> valuesSet = new Set<String>();
            String surveyLang = typeSettingMap.get(feedbackName).Language__c;
            String aiLang = typeSettingMap.get(feedbackName).AI_Training_Language__c;
            langMap.put(surveyLang, aiLang);
        }
        return langMap;
    }
    
}