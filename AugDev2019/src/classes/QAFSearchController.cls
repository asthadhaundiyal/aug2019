public with sharing class QAFSearchController{
    
    private list<in_gage__Quality_Assessment_Form__c> qafList {get; set;}
    public in_gage__Quality_Assessment_Form__c qa {get; set;}
    private set<Id> qafSelectedSet;
    public Integer qafSelectedCount {get; set;}
    public String selectedOneQAF {get; set;}
    public Boolean qaSEWithAIEnabled {get;set;}
    
    public list<String> alphaList {get; set;}
    public String alphaFilter {get; set;}
    public String empType {get;set;}
    public list<SelectOption> empTypes {get;set;}
    public list<SelectOption> qafTypes {get;set;}
    public String qafType {get;set;}
    public String empName {get; set;}
    public String mgrName {get; set;} 
    public String caseType {get;set;}
    public list<SelectOption> caseTypes {get;set;}
    public String sentimentType {get;set;}
    public list<SelectOption> sentimentTypes {get;set;}
    public String emotionType {get;set;}
    public list<SelectOption> emotionTypes {get;set;}
    public Date startDate{get;set;}
    public Date endDate{get;set;}
    public List<Employee__c> empList {get;set;}
    
    private String saveEmpName;
    private String saveQAFType;
    private String saveMgrName;
    private String saveCaseType;
    private String saveSentType;
    private String saveEmoType;
    private String saveStartDate;
    private String saveEndDate;
    private String queryQAF;
    //private integer offset;    
    //private integer pageSize;         
    //public integer totalRecords;
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sortFieldSave;
    
    public QAFSearchController(){
        //pageSize = 10;
        qa = new in_gage__Quality_Assessment_Form__c();
        qafList = new list<in_gage__Quality_Assessment_Form__c>();
        empList = new List<Employee__c>();
        this.qaSEWithAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        qafSelectedSet = new set<Id>();
        empTypes = empTypes();
        caseTypes = caseTypes();
        qafTypes = qafTypes();
        sentimentTypes = sentimentTypes();
        emotionTypes = emotionTypes(); 
        buildQuery();  
    }
    
    public ApexPages.StandardSetController stdSetControllerQAF {
        get {
            if(stdSetControllerQAF == null) {
                size = 10;
                stdSetControllerQAF = new ApexPages.StandardSetController(Database.getQueryLocator(queryQAF));
                stdSetControllerQAF.setPageSize(Integer.valueOf(10));
                stdSetControllerQAF.setPageSize(size);
                noOfRecords = stdSetControllerQAF.getResultSize();
            }
            system.debug('stdSetControllerQAF: '+ stdSetControllerQAF);
            system.debug('noOfRecords: '+ noOfRecords);
            return stdSetControllerQAF;
        }
        set;
    }
    
    public list<in_gage__Quality_Assessment_Form__c> getCurrentQAFList() {
        qafList = new list<in_gage__Quality_Assessment_Form__c>();
        for (in_gage__Quality_Assessment_Form__c qaf : (list<in_gage__Quality_Assessment_Form__c>)stdSetControllerQAF.getRecords()) {
            qafList.add(qaf);
        }
        return qafList;
    }
    
    public PageReference empCallLookup(){
        Map<Id, Employee__c> empMap = new Map<ID, Employee__c>([Select id, Name, First_Name__c, Last_Name__c, Manager_User__c from Employee__c where Has_In_Gage_Licence__c = true limit 10000]);
        for(Id id : empMap.keySet()) empList.add(empMap.get(id));
        system.debug('empList: '+ empList);
        PageReference pr = new PageReference('/apex/CustomEmpLookupPage');
        pr.setRedirect(false);
        return pr;
    }
    public PageReference mgrCallLookup(){
        List<Employee__c> eList1 = [Select id, Manager_Employee__c from Employee__c where Manager_Employee__c!=null and Has_In_Gage_Licence__c = true limit 10000];

        Set<Id> mgrIds = new Set<Id>();
        for(Employee__c e : eList1) mgrIds.add(e.Manager_Employee__c);
        
        empList = [Select id, Name, First_Name__c, Last_Name__c, Manager_User__c from Employee__c where id IN :mgrIds limit 10000];
        PageReference pr = new PageReference('/apex/CustomEmpLookupPage');
        pr.setRedirect(false);
        return pr;
    }
    
    public PageReference returnSelectedEmp(){
        empName = ApexPages.currentPage().getParameters().get('EmpName');
        PageReference pr = new PageReference('/apex/QAFSearchPage');
        pr.setRedirect(false);
        return pr;
    }
    public PageReference returnSelectedMgr(){
        mgrName = ApexPages.currentPage().getParameters().get('MgrName');
        PageReference pr = new PageReference('/apex/QAFSearchPage');
        pr.setRedirect(false);
        return pr;
    }
    
    public PageReference setCaseType(){
        if(caseType =='All'){
           caseType = '';
           saveCaseType = null; 
        } 
       if(caseType!='') saveCaseType = caseType;
       return null;
    }
    public PageReference setSentimentType(){
        if(sentimentType =='All'){
           sentimentType = '';
           saveSentType = null; 
        } 
       if(sentimentType!='')saveSentType = sentimentType;
       return null;
    }
    public PageReference setEmotionType(){
        if(emotionType =='All'){
           emotionType = '';
           saveEmoType = null; 
        } 
       if(emotionType!='')saveEmoType = emotionType;
       return null;
    }
    public PageReference setStartDate(){
        if(startDate!=null){
			DateTime DT = DateTime.newInstance(startDate.year(), startDate.month(), startDate.day());
        	saveStartDate =  DT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        }
       return null;
    }
    public PageReference setEndDate(){
        if(endDate!=null){
			DateTime DT = DateTime.newInstance(endDate.year(), endDate.month(), endDate.day());
        	saveEndDate =  DT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        }
        return null;
    }
    public PageReference setQAFType(){
        if(qafType =='All'){
            qafType = '';
            saveQAFType = null;
        } 
       if(qafType!='') saveQAFType = qafType;
       return null;
    }
    
    public PageReference clearAll(){
        qafList.clear();
        empType = null;
        qafType = null;
        empName = null;
        mgrName = null;
        caseType = null;
        sentimentType = null;
        emotionType = null;
        startDate = null;
        endDate = null;
        saveEmpName = null;
        saveQAFType = null;
        saveMgrName = null;
        saveCaseType = null;
        saveSentType = null;
        saveEmoType = null;
        saveStartDate = null;
        saveEndDate = null;
        buildQuery();
        return null;
    }
    
    public PageReference searchQAF() {
        system.debug('empName: '+ empName);
        system.debug('qafType: '+ qafType);
        system.debug('mgrName: '+ mgrName);
        system.debug('caseType: '+ caseType);
        system.debug('sentimentType: '+ sentimentType);
        system.debug('emotionType: '+ emotionType);
        system.debug('startDate: '+ startDate);
        system.debug('endDate: '+ endDate);
        if(empName!='') saveEmpName = empName;
        if(qafType!='' && qafType!='All') saveQAFType = qafType;
        if(mgrName!='') saveMgrName = mgrName;
        if(caseType!='' && caseType!='All')saveCaseType = caseType;
        if(sentimentType!='' && sentimentType!='All')saveSentType = sentimentType;
        if(emotionType!='' && emotionType!='All')saveEmoType = emotionType;
        if(startDate!=null){
			DateTime DT = DateTime.newInstance(startDate.year(), startDate.month(), startDate.day());
        	saveStartDate =  DT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        }
        if(endDate!=null){
            endDate = endDate.addDays(1);
			DateTime DT = DateTime.newInstance(endDate.year(), endDate.month(), endDate.day());
        	saveEndDate =  DT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        }
        system.debug('saveEmpName: '+ saveEmpName);
        system.debug('saveQAFType: '+ saveQAFType);
        system.debug('saveMgrName: '+ saveMgrName);
        system.debug('saveCaseType: '+ saveCaseType);
        system.debug('saveSentType: '+ saveSentType);
        system.debug('saveEmoType: '+ saveEmoType);
        system.debug('saveStartDate: '+ saveStartDate);
        system.debug('saveEndDate: '+ saveEndDate);
        buildQuery(); 
        return null;
    }
    
    public void buildQuery() {
        stdSetControllerQAF = null;
        queryQAF = 'select id, name, Agent__c,Agent__r.Name, Agent_Full_Name__c, Agent_Manager__c, Agent_Manager__r.Name, Total_Score__c, AI_Behavioural_Score__c,' 
            + 'Case__c, Case__r.CaseNumber, Case__r.Type, Overall_Agent_Sentiment_Label__c, Quality_Team__c, Submitted_Form_Date__c, CreatedDate, AI_Created__c'  
            +' from in_gage__Quality_Assessment_Form__c where Agent__c!= null'; 
        
        if (saveEmpName != null) queryQAF += ' AND Agent__r.Name = :saveEmpName';
        if (saveQAFType != null){
            if(saveQAFType == 'AI') queryQAF += ' AND AI_Created__c = true';
            else if(saveQAFType == 'Non AI') queryQAF += ' AND AI_Created__c = false';
        } 
        if (saveMgrName != null) queryQAF += ' AND Manager_Employee_Name__c = :saveMgrName';
        if (saveCaseType != null) queryQAF += ' AND Case__r.Type = :saveCaseType';
        if (saveSentType != null) queryQAF += ' AND Overall_Agent_Sentiment_Label__c = :saveSentType';
        if (saveStartDate != null) queryQAF += ' AND CreatedDate >=' + saveStartDate;
        if (saveEndDate != null) queryQAF += ' AND CreatedDate <=' + saveEndDate;
        
        queryQAF += ' ORDER BY ' + String.escapeSingleQuotes(sortField) + ' ' + String.escapeSingleQuotes(sortDirection) + ' LIMIT 10000';
        
        system.debug('queryQAF:' + queryQAF);
    }
    public String sortDirection {
        get { if (sortDirection == null) {  sortDirection = 'asc'; } return sortDirection;  }
        set;
    }
    public String sortField {
        get { if (sortField == null) {sortField = 'Name'; } return sortField;  }
        set; 
    }
    public void sortToggle() {
        sortDirection = sortDirection.equals('asc') ? 'desc NULLS LAST' : 'asc';
        if (sortFieldSave != sortField) {
            sortDirection = 'asc';
            alphaFilter = 'All';
            sortFieldSave = sortField;
        }
        buildQuery();
    }
    public PageReference editQAF(){
        String qafId= ApexPages.currentPage().getParameters().get('qafId'); 
        PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('id', qafId);
        pageRef.getParameters().put('mode', 'edit');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference delQAF(){
        String qafId= ApexPages.currentPage().getParameters().get('qafId'); 
        in_gage__Quality_Assessment_Form__c qaf = [Select id from in_gage__Quality_Assessment_Form__c where id =:qafId limit 1];
        if(qaf !=null && qaf.id !=null) delete qaf;
        return null;
    }
    
    public PageReference newQAF(){
        PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('mode', 'edit');
        pageRef.setRedirect(true);
        return pageRef;
    }

    public list<SelectOption> caseTypes() {
        List<SelectOption> caseTypes = new List<SelectOption>();
        caseTypes.add(new SelectOption('All', 'All'));
        Schema.DescribeFieldResult fieldResult = Case.Type.getDescribe();
        for( Schema.PicklistEntry f : fieldResult.getPicklistValues()){
            caseTypes.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return caseTypes;
    }
    public list<SelectOption> sentimentTypes(){  
        List<SelectOption> sentimentTypes = new List<SelectOption>();
        sentimentTypes.add(new SelectOption('All', 'All'));
        Schema.DescribeFieldResult fieldResult = QA_AI_Feedback__c.Sentiment__c.getDescribe();
        for( Schema.PicklistEntry f : fieldResult.getPicklistValues()){
            sentimentTypes.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return sentimentTypes; 
    }
    public list<SelectOption> emotionTypes(){  
        List<SelectOption> emotionTypes = new List<SelectOption>();
        emotionTypes.add(new SelectOption('All', 'All'));
        Schema.DescribeFieldResult fieldResult = QA_AI_Feedback__c.Emotion__c.getDescribe();
        for( Schema.PicklistEntry f : fieldResult.getPicklistValues()){
            emotionTypes.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return emotionTypes; 
    }
    public list<SelectOption> empTypes(){  
        List<SelectOption> empTypes = new List<SelectOption>();
        empTypes.add(new SelectOption('Agent', 'Agent'));
        empTypes.add(new SelectOption('Manager', 'Manager'));
        return empTypes; 
    }
     public list<SelectOption> qafTypes(){  
        List<SelectOption> qafTypes = new List<SelectOption>();
        qafTypes.add(new SelectOption('AI', 'AI'));
        qafTypes.add(new SelectOption('Non AI', 'Non AI'));
        qafTypes.add(new SelectOption('All', 'All'));
        return qafTypes; 
    }
    
     
    
    public Boolean hasNext {
        get {
            return stdSetControllerQAF.getHasNext();
            }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return stdSetControllerQAF.getHasPrevious();
            }
        set;
    }
    
    public Integer pageNumber {
        get {
            return stdSetControllerQAF.getPageNumber();
        }
        set;
    }
    /*public Integer totalPages {
        get {
            totalPages = stdSetControllerQAF.getResultSize() / stdSetControllerQAF.getPageSize();
            return totalPages;
        }
        set;
    }*/

    
    public void first() {
         stdSetControllerQAF.first();
    }
     
    public void last() {
         stdSetControllerQAF.last();
    }

    // returns the previous page of records
    public void previous() {
        stdSetControllerQAF.previous();
    }

    // returns the next page of records
    public void next() {
        stdSetControllerQAF.next();
    }
}