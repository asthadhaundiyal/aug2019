@istest
public class AI_Batch_DeleteTrainingRecords_T {
		
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static QA_AI_Feedback__c qaFB;
    private static NPS_AI_Feedback__c naFB;
	
	private static void setupData(){

        naFB = new NPS_AI_Feedback__c(Text_for_Training__c = 'text for training', Category__c = 'Testing1', Language__c = 'English', Added_for_Training__c= true);
		insert naFB;
        
        qaFB = new QA_AI_Feedback__c(Text_for_Training__c = 'text for training', Category__c = 'Testing1', Language__c = 'English', Sub_Category__c ='test1', Emotion__c ='Happiness', Sentiment__c ='Positive');
		insert qaFB;
	}
	
	private static testmethod void testDeleteNPSTraining() {
		
		setupData();
		Test.startTest();
		string npsQuery = 'select id FROM NPS_AI_Feedback__c where Added_for_Training__c = true';
        database.executeBatch(new AI_Batch_DeleteTrainingRecords(npsQuery),1);
		
		Test.stopTest();
	}  
    private static testmethod void testDeleteQATraining() {
		
		setupData();
		Test.startTest();
		string qaQuery = 'select id FROM QA_AI_Feedback__c where in_gage__Category_Sent_for_Training__c = true';
        database.executeBatch(new AI_Batch_DeleteTrainingRecords(qaQuery),200);
		
		Test.stopTest();
	} 
}