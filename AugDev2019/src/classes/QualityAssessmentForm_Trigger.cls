public with sharing class QualityAssessmentForm_Trigger extends TriggerHandler {
    
    // *************************************
    // ******* Public Declarations *********
    // *************************************
    // ******************************
    // ******* Constructor **********
    // ******************************
    public QualityAssessmentForm_Trigger() {}
    
    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {
    	populateAgentManagerField(newObjects);
        populateCaseClosedDate(newObjects);
        //populateAIBehaviouralScore(newObjects);
    }

    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
    	populateAgentManagerField(newObjects);
        populateCaseClosedDate(newObjects);
    }

    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
        
        // createEvents((List<in_gage__Quality_Assessment_Form__c>) newObjects, null); // Live Coaching Form update not required.  as per Rod 16/5/2016
        //checkQuestionSet((List<in_gage__Quality_Assessment_Form__c>) newObjects,null);  // Now managed by the QAF VF Page, March 2017.
        system.debug('after insert');
        createCoachingItems((List<in_gage__Quality_Assessment_Form__c>) newObjects,null);
    }

    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
		
		if(runOnce()){
			// createEvents((List<in_gage__Quality_Assessment_Form__c>) newObjects, (Map<Id, in_gage__Quality_Assessment_Form__c>) oldObjectsMap);  // Live Coaching Form update not required.  as per Rod 16/5/2016
	        checkQuestionSet((List<in_gage__Quality_Assessment_Form__c>) newObjects, (Map<Id, in_gage__Quality_Assessment_Form__c>) oldObjectsMap);
	    	system.debug('after update');
            
            createCoachingItems((List<in_gage__Quality_Assessment_Form__c>) newObjects,(Map<Id, in_gage__Quality_Assessment_Form__c>) oldObjectsMap);
            //updateSubmitForm((List<in_gage__Quality_Assessment_Form__c>) newObjects,(Map<Id, in_gage__Quality_Assessment_Form__c>) oldObjectsMap);
		}
        
    }

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {
    
        // createEvents((List<in_gage__Quality_Assessment_Form__c>) oldObjects, null);  // Live Coaching Form update not required.  as per Rod 16/5/2016
    }

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {
    
        // createEvents((List<in_gage__Quality_Assessment_Form__c>) objects, null);  // Live Coaching Form update not required.  as per Rod 16/5/2016
    }

    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    
    private void populateAIData(){
        
    }
    private void createEvents(List<in_gage__Quality_Assessment_Form__c> newObjects, Map<Id, in_gage__Quality_Assessment_Form__c> oldObjectsMap) {
    
        for(in_gage__Quality_Assessment_Form__c q : newObjects){
            
            in_gage__Quality_Assessment_Form__c oldQ = (oldObjectsMap != null ? oldObjectsMap.get(q.id) : new in_gage__Quality_Assessment_Form__c());
            
            // event for the new cf
            if(q.Submitted_Form_Date__c != null && q.Agent__c != null &&
                (q.Submitted_Form_Date__c != oldQ.Submitted_Form_Date__c || 
                        q.Agent__c != oldQ.Agent__c ||
                            q.Total_Score__c != oldQ.Total_Score__c)){
                
                //addEvent(q.Agent__c, q.id, Date.newInstance(q.Submitted_Form_Date__c.year(), q.Submitted_Form_Date__c.month(), q.Submitted_Form_Date__c.day()));
                addEvent(q.Agent__c, q.id, Date.valueOf(q.Submitted_Form_Date__c));
            }
            
            // event for the old cf
            if(oldQ.Submitted_Form_Date__c != null && oldQ.Agent__c != null &&
                (q.Submitted_Form_Date__c != oldQ.Submitted_Form_Date__c || 
                        q.Agent__c != oldQ.Agent__c ||
                            q.Total_Score__c != oldQ.Total_Score__c)){
                
                addEvent(oldQ.Agent__c, oldQ.id, Date.valueOf(q.Submitted_Form_Date__c));
            }
        }
    }
    
    private void addEvent(String agentID, String recID, Date dt){
        
        events.add(new Event(Event.EventType.QAF_CHANGE, agentID, recID, dt));
    }
    
    
	private void checkQuestionSet(list<in_gage__Quality_Assessment_Form__c> triggerNew, map<id,in_gage__Quality_Assessment_Form__c> oldMap){
	
		//map<id,in_gage__Quality_Assessment_Form__c> QAFmap = new map<id,in_gage__Quality_Assessment_Form__c>();
		
		if(oldMap == null){  // trigger is insert << insert actions managed by QAF VF page March 2017.
			//for(in_gage__Quality_Assessment_Form__c qaf : triggerNew){
				//if(qaf.Question_Set__c != null) QAFmap.put(qaf.id,qaf);
			//}
		}else{  // trigger is update
			for(in_gage__Quality_Assessment_Form__c qaf : triggerNew){
				if(qaf.Number_of_Answers__c >0){
					if(qaf.Question_Set__c != oldMap.get(qaf.id).Question_Set__c && oldMap.get(qaf.id).Question_Set__c != null) qaf.addError('The Question Set cannot be changed when the Quality Assessment Form has answers.');
					if(qaf.Agent__c != oldMap.get(qaf.id).Agent__c) qaf.addError('The Agent cannot be changed when the Quality Assessment Form has answers.');
				}else if(qaf.Question_Set__c != null){
						//if(qaf.Question_Set__c != oldMap.get(qaf.id).Question_Set__c)  QAFmap.put(qaf.id,qaf);
					//QAFmap.put(qaf.id,qaf);   << now managed by QAF VF page, March 2017.
				}
			}		
		}
		//if(!QAFmap.isEmpty()) createBlankAnswerRecords(QAFmap);  << now managed by QAF VF page, March 2017.
	
	}
	
	/*  Process now managed by QAF VF page.  March 2017.
	private void createBlankAnswerRecords(map<id,in_gage__Quality_Assessment_Form__c> QAFmap){
		
		list<Quality_Assessment_Form_Answer__c> newAnswers = new list<Quality_Assessment_Form_Answer__c>();
		
		// obtain section and question records
		set<id> questionSetIds = new set<id>();
		for(in_gage__Quality_Assessment_Form__c qaf:QAFmap.values()){
			questionSetIds.add(qaf.Question_Set__c);
		}
		
		if(!questionSetIds.isEmpty()){
		
			map<id,Question_Section__c> sectionMap = new map<id,Question_Section__c>([select id,Section_Name__c from Question_Section__c where Question_Set__c in: questionSetIds order by Section_Number__c]);
		    list<Question__c> questions = [select id, Question__c, Question_No__c, Question_Section__c, Scoring__c, Picklist_Options__c from Question__c where Question_Section__c in: sectionMap.keySet() order by Question_No__c];
			
			// increment through each questionSet
			for(id qsId: questionSetIds){
				// increment through each QAF record
				for(id QAFid : QAFmap.keySet()){
					// if the QAF matches the Question Set	
					if(QAFmap.get(QAFid).Question_Set__c == qsId){
						// increment through each question to create an answer record
						for(Question__c q: questions){
							newAnswers.add(new Quality_Assessment_Form_Answer__c(
								in_gage__Quality_Assessment_Form__c = QAFid,
								Question_Set__c = qsId,
								Question__c = q.id,
								Question_Type__c = q.Picklist_Options__c != null ? 'Picklist' : null,
								Answer__c = false
							));
						}
					}				
				}
			}
		}
		if(!newAnswers.isEmpty()) database.insert(newAnswers,false);
	}*/
   /* private void updateSubmitForm(list<in_gage__Quality_Assessment_Form__c> triggerNew, map<id,in_gage__Quality_Assessment_Form__c> oldMap){
        for(in_gage__Quality_Assessment_Form__c qaf : triggerNew){
            system.debug(' old %age completed = ' + oldMap.get(qaf.id).Percentage_Completed__c);
            system.debug(' new %age completed = ' + qaf.Percentage_Completed__c);
            if(qaf.Percentage_Completed__c != oldMap.get(qaf.id).Percentage_Completed__c){
             	qaf.Submit_To_Agent__c = true;   
            }
		}
    }*/
	private void createCoachingItems(list<in_gage__Quality_Assessment_Form__c> triggerNew, map<id,in_gage__Quality_Assessment_Form__c> oldMap){
		
		// check forms are submitted
		list<in_gage__Quality_Assessment_Form__c> qafList = new list<in_gage__Quality_Assessment_Form__c>();
		map<id,id> agentToManagerMap = new map<id,id>();
		set<string> managerIds = new set<string>();
		map<id,user> managerMap = new map<id,user>();
		
		for(in_gage__Quality_Assessment_Form__c qaf : triggerNew){
			if(oldMap == null) oldMap = new map<id,in_gage__Quality_Assessment_Form__c>();
			if(!oldMap.containsKey(qaf.id)) oldMap.put(qaf.id,new in_gage__Quality_Assessment_Form__c(Agent__c = qaf.Agent__c));
            system.debug('new Submit = '+qaf.Submit_Form__c);
			system.debug('old Submit = '+oldMap.get(qaf.id).Submit_Form__c);
            system.debug('ManagerId = ' + qaf.ManagerId__c);
            if(qaf.Submit_Form__c && !oldMap.get(qaf.id).Submit_Form__c) qafList.add(qaf);
			managerIds.add(qaf.ManagerId__c);
			agentToManagerMap.put(qaf.Agent__c,qaf.ManagerId__c);
		}
		if(!managerIds.isEmpty()) managerMap = new map<id,user>([select id,firstname,lastname,email from user where id in: managerIds]);
        system.debug('qafList '+ qafList);
		// submitted QAFs
		if(!qafList.isEmpty()){
			list<Coaching_Items__c> coachingItems = new list<Coaching_Items__c>();
			list<FeedItem> feedItems = new list<FeedItem>();
			id templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'QA_Critical_Error' limit 1].id;
			string chatterGroup = 'Legendary Customer Service';
			id chatterGroupId;
            if(test.isRunningTest()) {
              chatterGroup = 'testInGageChatterGroup';
            }
            chatterGroupId = [SELECT Id FROM CollaborationGroup WHERE Name = :chatterGroup limit 1].Id;
			for(in_gage__Quality_Assessment_Form__c qaf: qafList){
				string comment;
				string mgrEmail = qaf.Managers_Email__c;
				if(test.isRunningTest()) mgrEmail = 'testEmail@email.com';
				if(qaf.Percentage_Completed__c == 0 && (mgrEmail != null)){
					comment = '0 Score';
					emailManager(qaf,templateId, managerMap.get(agentToManagerMap.get(qaf.Agent__c)),mgrEmail);
				}
                system.debug('qaf.Percentage_Completed__c '+ qaf.Percentage_Completed__c);
				if(qaf.Percentage_Completed__c >= 100) {
					//comment = '100 Score';
					comment = string.valueof(qaf.Percentage_Completed__c) +' Score';
					//String message='Congrats to ' + qaf.Agent_Full_Name__c + ' for getting 100% on your quality form :)';
					String message='Congrats to ' + qaf.Agent_Full_Name__c + ' for getting '+string.valueof(qaf.Percentage_Completed__c)+'% on your quality form :)';
                    if(chatterGroupId != null) feedItems.add(new FeedItem(Body = message, ParentId = chatterGroupId));
				}
				if(qaf.Percentage_Completed__c >= 1 && qaf.Percentage_Completed__c <=60 ) comment = '60 or below score';
		
				if(comment != null){
					Coaching_Items__c ci = newCoachingItem(qaf,comment);
					if(ci !=null) coachingItems.add(ci);
				}
			}
			if(feedItems != null) database.insert(feedItems,false);
			if(!coachingItems.isEmpty()) database.insert(coachingItems,false);
		}
		
	}
	
	private Coaching_Items__c newCoachingItem(in_gage__Quality_Assessment_Form__c qaf,string comment){
		return new Coaching_Items__c(
			Coaching_Item_Catergory__c = 'Quality',
			Coaching_Item_Comment__c = comment,
			Employee__c = qaf.Agent__c,
			For_Review__c = true,
			in_gage__Quality_Assessment_Form__c = qaf.id
		);	
	}
	
	private void emailManager(in_gage__Quality_Assessment_Form__c qaf,id templateId,user mgr,string mgrEmail){
		
		string mgrFname, mgrLname;
		if(test.isRunningTest()){
			mgrFname ='testFirstname';
			mgrLname ='testLastname';
		}else{
			mgrFname = mgr.firstname;
			mgrLname = mgr.lastname;
		}
		
		contact c = new contact(firstname = mgrFname, lastname= mgrLname, email= mgrEmail);
		insert c;
		
		String[] toAddresses = new String[] {mgrEmail};
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(toAddresses);
		mail.setTemplateId(templateId);
		mail.setTargetObjectId(c.id);
		mail.setSaveAsActivity(false);
		mail.setWhatId(qaf.id);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		
		delete c;
	}
	
		
	private void populateAgentManagerField(list<in_gage__Quality_Assessment_Form__c> triggerNew){
		
		set<id> empIds = new set<id>();       
		for(in_gage__Quality_Assessment_Form__c qaf:triggerNew){
			if(qaf.ManagerId__c != null) {
				qaf.Agent_Manager__c = qaf.ManagerId__c;
			}else if(qaf.Agent__c != null){
				empIds.add(qaf.Agent__c);
			}
		}
		if(empIds!=null){
			map<id,Employee__c> eMap = new map<id,Employee__c>([select id, Manager_User__c from Employee__c where id in:empIds]);
			for(in_gage__Quality_Assessment_Form__c qaf:triggerNew){
				if(eMap.containsKey(qaf.Agent__c)) qaf.Agent_Manager__c = eMap.get(qaf.Agent__c).Manager_User__c;
			}
				
		}
				
	}
    
    
    private void populateCaseClosedDate(list<in_gage__Quality_Assessment_Form__c> triggerNew){
		
        set<id> caseIds = new set<id>();
		for(in_gage__Quality_Assessment_Form__c qaf:triggerNew){
			if(qaf.Case__c != null)
				caseIds.add(qaf.Case__c);
		}
		if(caseIds!=null){
			map<id,Case> eMap = new map<id,Case>([select id, ClosedDate from Case where id in:caseIds]);
			for(in_gage__Quality_Assessment_Form__c qaf:triggerNew){
				if(eMap.containsKey(qaf.Case__c)) qaf.Call_Date__c = eMap.get(qaf.Case__c).ClosedDate;
			}
				
		}
				
	}

    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
}