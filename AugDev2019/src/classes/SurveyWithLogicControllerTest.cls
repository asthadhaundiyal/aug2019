@isTest
private class SurveyWithLogicControllerTest {
	@testSetup
    static void setup() {
        Survey__c survey = new Survey__c(Submit_Response__c='Thanks');
        insert survey;
        Survey_Question__c surveyQuestion = new Survey_Question__c(Question__c='Question 1');
        surveyQuestion.Survey__c = survey.Id;
        surveyQuestion.OrderNumber__c=1;
        surveyQuestion.Is_Parent__c = true;
        surveyQuestion.Type__c = 'Slider';
        insert surveyQuestion;
        Survey_Question__c surveyQuestion1 = new Survey_Question__c(Question__c='Question 1.1');
        surveyQuestion1.Survey__c = survey.Id;
        surveyQuestion1.OrderNumber__c=1;
        surveyQuestion1.Parent_Question__c = surveyQuestion.Id;
        surveyQuestion1.Type__c = 'Single Select--Vertical';
        insert surveyQuestion1;
        Survey_Option__c surveyOption = new Survey_Option__c(Survey_Question__c = surveyQuestion1.Id, Option__c='1');
        insert surveyOption;
        Survey_Option__c surveyOption1 = new Survey_Option__c(Survey_Question__c = surveyQuestion1.Id, Option__c='2');
        insert surveyOption1;
        survey.First_Question__c = surveyQuestion.Id;
        update survey;
    }
    
    @isTest
    static void testInitialize() {
        Survey__c survey = [Select id from Survey__c limit 1];

        Test.startTest();
        	String result = SurveyWithLogicController.initialize(survey.Id);
        Test.stopTest();

        System.assertEquals(false, result.equalsIgnoreCase(''), 'Should not be empty.');
    }
    
    @isTest
    static void testGetResponseFromOptionMultiple() {
        List<SurveyWithLogicController.CustomSelectOption> optionList =
            new List<SurveyWithLogicController.CustomSelectOption>{new SurveyWithLogicController.CustomSelectOption('1', 'a')};

        Test.startTest();
        String result = SurveyWithLogicController.getResponseFromOption(new List<String>{'1'}, optionList);
        Test.stopTest();
        
        System.assertEquals('a', result, 'should be equal');
    }
    
    @isTest
    static void testGetResponseFromOptionSingle() {
        List<SurveyWithLogicController.CustomSelectOption> optionList =
            new List<SurveyWithLogicController.CustomSelectOption>{new SurveyWithLogicController.CustomSelectOption('1', 'a')};

        Test.startTest();
        String result = SurveyWithLogicController.getResponseFromOption('1', optionList);
        Test.stopTest();
        
        System.assertEquals('a', result, 'should be equal');
    }
    
    @isTest
    static void testSubmitSurveyResponse() {
        Survey__c survey = [Select id from Survey__c limit 1];
        Survey_Option__c surveyOption = [select id from Survey_Option__c limit 1];
        Survey_Question__c surveyQuestion1 = [select id from Survey_Question__c where Is_Parent__c=true];
        
        Survey_Question__c surveyQuestion = new Survey_Question__c(Question__c='Question 2');
        surveyQuestion.Survey__c = survey.Id;
        surveyQuestion.OrderNumber__c=2;
        surveyQuestion.Is_Parent__c = false;
        surveyQuestion.Type__c = 'Slider';
        surveyQuestion.Default_Next_Question__c = surveyQuestion1.Id;
        insert surveyQuestion;
        survey.First_Question__c = surveyQuestion.Id;
        update survey;

        String wrapperStr = SurveyWithLogicController.initialize(survey.Id);
        SurveyWithLogicController.MainWrapper wrapper =
            (SurveyWithLogicController.MainWrapper)JSON.deserialize(wrapperStr, SurveyWithLogicController.MainWrapper.class);
        for(SurveyWithLogicController.ResponseWrapper wrap : wrapper.listOfWrapper) {
            if(wrap.childQuestionList != null) {
                wrap.childQuestionList[0].optionId = surveyOption.Id;
            } else {
                wrap.response = '5';
            }
        }
        
        Test.startTest();
        SurveyWithLogicController.submitSurveyResponse(JSON.serialize(wrapper));
        Test.stopTest();
        
        List<SurveyQuestionResponse__c> response = [select id from SurveyQuestionResponse__c];
        System.assertNotEquals(0, response.size(), 'Response size should not be 0');
    }
}