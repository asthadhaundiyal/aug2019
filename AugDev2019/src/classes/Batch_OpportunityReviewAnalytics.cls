global class Batch_OpportunityReviewAnalytics implements 
Database.Batchable<sObject>, Database.Stateful{
    
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    global final String Query;
    
    global Batch_OpportunityReviewAnalytics(){
        Query = getOpportunityQuery();
    }
    
    private string getOpportunityQuery(){
        //string d = string.valueOf(date.today());
        date startDate = Date.today().addDays(-1);
        date closeDate = Date.today();
        string startDateTime = startDate.year()+'-'+dt(startDate.month())+'-'+dt(startDate.day())+'T20:00:00Z';
        string closeDateTime = closeDate.year()+'-'+dt(closeDate.month())+'-'+dt(closeDate.day())+'T21:00:00Z';
		system.debug('startDateTime '+ startDateTime + ' closeDateTime ' + closeDateTime);
        return 'select id, accountId FROM Opportunity where isWon = true and CreatedDate >=' + startDateTime + 'AND CreatedDate <' + closeDateTime;
    
        //return 'select id, accountId FROM Opportunity where isWon = true and Closed_Date__c ='+d;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        list<Opportunity> oppList = (list<Opportunity>)scope;              
        list<Analytics__c> analyticsQuery = database.query(getAnalyticRecordsQuery(oppList));
        list<Analytics__c> analyticsUpdate = new list<Analytics__c>();
        
        // prepare the update list
        for(Opportunity o :oppList){
            boolean inAnalyticsQuery = false;
            for(Analytics__c a : analyticsQuery){
                string accountId = a.recordId__c.substringBefore(' ')+' ';
                //check if analytic record already exist for the account
                if(string.valueOf(o.accountId)+' ' == accountId){
                    inAnalyticsQuery = true; 
                    a.Opportunity__c = o.Id;
                    analyticsUpdate.add(a);
                }
            }
            // create new Analytics record if one doesnt exist
            if(!inAnalyticsQuery) 
                analyticsUpdate.add(new Analytics__c(recordId__c = string.valueOf(o.accountid)+' ' + string.valueOf(o.id), 
                                                              Account__c = string.valueOf(o.accountid),
                                                              Opportunity__c = string.valueOf(o.id)));
        }
        if(!analyticsUpdate.isEmpty()) {
            try{
                upsert analyticsUpdate recordId__c;
            }catch(exception e){
                system.debug('Error upserting Analytics record. Index[0] = : '+analyticsUpdate[0] + '. '+ e.getMessage());
            }
        }
    }
    
    private string dt(integer i){
        string s = string.valueOf(i);
        if(i<10) s='0'+s;
        return s;
    }
    
    private string getAnalyticRecordsQuery(list<Opportunity> oppList){
        integer Analytic_Search_Days = 0;
        if(setting.No_of_Days_Lookup_for_Analytics__c!=null)
            Analytic_Search_Days = (setting.No_of_Days_Lookup_for_Analytics__c).intValue();
        else Analytic_Search_Days = 30;
        date sDate = Date.today().addDays(-Analytic_Search_Days);
        string searchDate = sDate.year()+'-'+dt(sDate.month())+'-'+dt(sDate.day())+'T00:00:00Z';
        // get all the Analytics records starting for the account created in last 30 days
        string query = 'SELECT id, recordId__c,Opportunity__c,NPS__c FROM Analytics__c WHERE CreatedDate >=' +searchDate+ 'AND  (recordId__c LIKE \'';
        integer i=0;
        for(Opportunity o: oppList){
            query += string.valueOf(o.accountId);
            if(i < oppList.size()-1) query += '%\' OR recordId__c LIKE \'';
            i++;
        }
        query+= '%\')';
        return query;
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
}