public with sharing class CoachingForm_Trigger extends TriggerHandler {
    
    // *************************************
    // ******* Public Declarations *********
    // *************************************
    
    // ******************************
    // ******* Constructor **********
    // ******************************
    public CoachingForm_Trigger() {}
    
    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {
    	checkCFforRecalculation((List<Coaching_Form__c>) newObjects,null);
    }

    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
    	checkCFforRecalculation((List<Coaching_Form__c>) newObjects,(Map<Id, Coaching_Form__c>) oldMap);
    }

    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
        createEvents((List<Coaching_Form__c>) newObjects, null);
    }

    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
		createEvents((List<Coaching_Form__c>) newObjects, (Map<Id, Coaching_Form__c>) oldObjectsMap);
    }

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {
        createEvents((List<Coaching_Form__c>) oldObjects, null);
    }

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {
    	//checkCFforRecalculation((List<Coaching_Form__c>) objects,null);
        createEvents((List<Coaching_Form__c>) objects, null);
    }

    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    private void createEvents(List<Coaching_Form__c> newObjects, Map<Id, Coaching_Form__c> oldObjectsMap) {
    	
    	
		system.debug('newObjects = '+ newObjects);
        system.debug('oldObjectsMap = '+ oldObjectsMap);
    	boolean runMethod = IsMethodAllowedToRecurse('CFcreateEvents',1);
    	system.debug('runMethod = '+runMethod);
    	
    	if(runMethod){  // only wish to call the calculation batch once.
    	
    		set<id> cIds = new set<id>();
    		system.debug('newObjects.Size = ' + newObjects.size());
	        for(Coaching_Form__c c : newObjects){
	            
	            //if(c.IsValidForCalculating__c){
	            
	            	system.debug('c = '+ c);
	            
		            Coaching_Form__c oldC = (oldObjectsMap != null ? oldObjectsMap.get(c.id) : new Coaching_Form__c());
		            system.debug('oldC = '+ oldC);
		            
		            
		            // event for the new cf
		            if(c.Recalculate__c || (c.Performance_Start_Date__c != null /*&& c.Performance_End_Date__c != null*/ && c.Agent__c != null)){
						system.debug('Past Gate1');
		                if(((oldC.Recalculate__c != c.Recalculate__c) && c.Recalculate__c) 
		                	|| c.Performance_Start_Date__c != oldC.Performance_Start_Date__c 
		                	|| c.Performance_End_Date__c != oldC.Performance_End_Date__c
		                	|| c.Agent__c != oldC.Agent__c
		                	|| (c.Submit_Coaching_Form__c && !oldC.Submit_Coaching_Form__c) ){
		                	system.debug('--Adding New Event');
		                    //addEvent(c.Agent__c, c.id, c.Performance_Start_Date__c);
		                    cIds.add(c.id);
		                }
		            }else{
		                
		                // blank goals???
		            }
	            //}
	        }
	        if(cIds.size()>0){
	    		string batchQuery;
				batchQuery = 'select id, Performance_Start_Date__c,Performance_End_Date__c,Agent__c,Agent__r.User_Record__c, (select id,Goal__c,Performance__c,CalculationProgressWeight__c from Goals__r) from Coaching_Form__c where id in :cIds';
				database.executeBatch(new Batch_CalculateCoachingForm(batchQuery,cIds),1);
			}
    		
    	}
    	
    }
    
    private void addEvent(String agentID, String recID, Date dt){
        
        events.add(new Event(Event.EventType.CF_CHANGE, agentID, recID, dt));
    }
    
    
    private void checkCFforRecalculation(list<Coaching_Form__c> triggerNew, map<id,Coaching_Form__c> oldMap){
    	
    	if(oldMap == null) oldMap = new map<id,Coaching_Form__c>();
    	for(Coaching_Form__c c : triggerNew){
	    	Coaching_Form__c oldC =  new Coaching_Form__c();
	    	if(oldMap.containsKey(c.id)) oldC = oldMap.get(c.id);
		
		    if(	/*((oldC.Recalculate__c != c.Recalculate__c) && c.Recalculate__c)
		    	||*/ 
		    	((c.Performance_Start_Date__c != null && c.Performance_Start_Date__c != oldC.Performance_Start_Date__c)
	        	|| (c.Performance_End_Date__c != null && c.Performance_End_Date__c != oldC.Performance_End_Date__c ))
	        	&& c.Agent__c != null
	        	)
	        	//c.Recalculate__c = true;
	        	if(c.IsValidForCalculating__c) c.isCalculating__c = true;
    	}
    }
    
    
    
    
    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
}