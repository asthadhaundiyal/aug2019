/**
 * Test Class for PurchaseOrderCloneWithItemsController
 */
@isTest
private class PurchaseOrderCloneWithItemsController_T{
    
    private static Employee__c emp;
    private static Coaching_Form__c cf;
    private static Goals__c g;
    private static Coaching_Promise__c cp;
    
    /**
     * Setup test data
     */
    private static void setupData(){
        
        
        emp = new Employee__c();
        //inserted below ADAM:);
        //emp.OwnerID= '00524000000hcOM'; //emp.User_Record__c;
        emp.First_Name__c = 'Test';
        insert emp;
        
        cf = new Coaching_Form__c();
        // set here required fields: cf.Required_Field__c = 'x'
        cf.Agent__c = emp.id;
        insert cf;
        
        g = new Goals__c();
        // set here required fields: g.Required_Field__c = 'x'
        g.Coaching_Form__c = cf.id;
        insert g;
        
        cp = new Coaching_Promise__c();
        // set here required fields: cp.Required_Field__c = 'x'
        cp.Coaching_Form__c = cf.id;
        cp.Completed__c = false;
        insert cp;
    }
    
    /**
     * Test cloneWithItems
     */
    static testMethod void cloneWithItems_Test(){
        
        // Setup Test Data
        setupData();
        
        // Apex Controller Page
        PageReference pageRef = Page.PurchaseOrderCloneWithItemsController;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
            // Init apex controller
            PurchaseOrderCloneWithItemsController pocwic = new PurchaseOrderCloneWithItemsController(new ApexPages.StandardController(cf));
            
            pocwic.poCloned.Performance_Start_Date__c = system.today();
            pocwic.poCloned.Performance_End_Date__c = system.today();
            
            // Invoke method to test
            pocwic.cloneWithItems();
            
        Test.stopTest();
        
        // Get cloned data
        List<Coaching_Form__c> cfs = [select id from Coaching_Form__c where id =: pocwic.poCloned.Id];
        List<Goals__c> gs = [select id from Goals__c where Coaching_Form__c =: pocwic.poCloned.Id];
        List<Coaching_Promise__c> cps = [select id from Coaching_Promise__c where Coaching_Form__c =: pocwic.poCloned.Id];
        
        // Assert that data was cloned
        //system.assertEquals(cfs.size(), 1);
        //system.assertEquals(gs.size(), 1);
        //system.assertEquals(cps.size(), 1);
    }
}