public with sharing class CallCustomLookController {
        public in_gage__Quality_Assessment_Form__c qa {get;set;}
        //public in_gage__Quality_Assessment_Form__c qaF {get;set;}
        public String employeeId {get;set;}
        public String agentId{get;set;}
        public string mode {get;set;}

        public CallCustomLookController(ApexPages.StandardController stdController){
                //qaF = new in_gage__Quality_Assessment_Form__c();
                this.qa= (in_gage__Quality_Assessment_Form__c)stdController.getRecord();
                
                if(this.qa!=null){
                        qa=[SELECT Agent__c, Agent__r.User_Record__c, case__c, Opportunity__c, Type__c from in_gage__Quality_Assessment_Form__c where Id=:qa.Id];
                
                        if((qa.Type__c == 'Case' || qa.Type__c == null) && qa.case__c != null){
                                mode ='maindetail';
                        }else if(qa.Type__c == 'Opportunity' && qa.Opportunity__c != null){
                                mode ='maindetail';
                        }else{
                                mode = 'edit';
                        }
                }
                
        }
        
        public PageReference saveCase(){
                update qa;
                
                PageReference pageRef = new PageReference('/' + qa.id);
                pageRef.setRedirect(true);
                mode = 'maindetail';
                return pageRef;
                
        }

        
        public PageReference editCase(){
                
                PageReference pageRef = new PageReference('/apex/CallCustomLookup?' + qa.id);
                mode = 'edit';
                return pageRef;
        }
        
        
}