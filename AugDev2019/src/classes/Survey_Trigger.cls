public with sharing class Survey_Trigger extends TriggerHandler {

    // *************************************
    // ******* Public Declarations *********
    // *************************************

    // ******************************
    // ******* Constructor **********
    // ******************************
    public Survey_Trigger() {}

    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {}

    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
    	checkUpdatedSurvey((List<Survey__c>) newObjects, (Map<Id, Survey__c>) oldMap);
    }

    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
    	checkDeletedSurvey((List<Survey__c>) newObjects);
    }

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}


    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************

	public static map<string,string> inGageDefaultSurveyMap(){
		return new map<string,string>{
			'Employee Engagement Survey' => 'Employee Engagement',
			'NPS Survey' => 'Net Promoter Score',
			'Won Opportunity Survey' => 'Won Opportunity'
		};
	}
	
	private void checkUpdatedSurvey(List<Survey__c> triggerNew, Map<Id, Survey__c> oldMap){
	// on update
		map<string,string> surveyMap = inGageDefaultSurveyMap();
		
		for(Survey__c s: triggerNew){
			Survey__c oldS = oldMap.get(s.id);
			
			if(surveyMap.containsKey(oldS.name) && !surveyMap.containsKey(s.name)){
				try{
					s.Type__c = surveyMap.get(oldS.name);
				}catch(exception e){
					s.addError('The default name fields are used by the In-Gage application and cannot be amended.');
				}
			}
			
			if(surveyMap.containsKey(oldS.type__c) && !surveyMap.containsKey(s.type__c)){
				try{
					s.Type__c = oldS.Type__c;
				}catch(exception e){}
			}
			
		}
	}

	private void checkDeletedSurvey(List<Survey__c> triggerNew){
	// on delete
		map<string,string> surveys = inGageDefaultSurveyMap();
	
		for(Survey__c s: triggerNew){
            boolean err = false;
			if(surveys.containsKey(s.name)){
				err = true;
			}
            for(string str : surveys.values()){
                if(s.Type__c != null && s.Type__c == str) err = true;
            }
            if(err == true) s.addError('The default survey records are used by the In-Gage application and cannot be deleted.');
		}
	}


}