@istest
public class AI_Batch_CreateQAF_T {
		
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static EmailMessage e1;
    private static Email_For_Processing_Data__c emailSetting;
    private static ingage_Application_Settings__c setting;
	
	private static void setupData(){
        

        emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
        insert emailSetting;
        
        setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
		insert setting;
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test', Language__c ='English');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'New');
		insert c;
        
        e1 = new EmailMessage(TextBody='Your account is unlocked and fully functional now', Incoming=false, ParentID = c.Id);
        insert e1;
        
	}
	
	private static testmethod void testQAAI() {
		
		setupData();
		c.status = 'Closed';
        update c;
        Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
		Test.startTest();
		// fire the batch process
		string cQuery = 'select id, IsClosed, Description, AI_Language__c, Employee__c from Case';
        database.executeBatch(new AI_Batch_CreateQAF(cQuery)); 
		
		Test.stopTest();
	}  
}