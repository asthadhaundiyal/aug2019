/* Create QA AI Feedback(Training) Data - Batch */

global class AI_Batch_CreateQATrainingData implements Database.Batchable<sObject>, Database.Stateful{
    
    global final String query;
    global final set<Id> qaAIFBIds = new set<Id>();
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();

   global AI_Batch_CreateQATrainingData(String q){
       query = q;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<QA_AI_Feedback__c> qaAIFList = new List<QA_AI_Feedback__c>();
        List<Case> caseList = (List<Case>)scope;
        Set<Id> caseIds = new Set<Id>();
        for(case c: caseList) caseIds.add(c.id);
        String aiCategory = setting.AI_Case_Category__c;
        String aiSubCategory = setting.AI_Case_SubCategory__c ;
        Map<Id,sObject> caseCategoryMap = getCaseCategoryMap(caseIds, aiCategory, aiSubCategory);
        map<Id,sObject> recordTypeEligibleMap = AIUtilityClass.getRecordTypeEligibleMap(caseIds);
        for(Case c :caseList){
            	String caseCategory = '';
            	String caseSubCategory = '';
                if(caseCategoryMap != null && caseCategoryMap.containsKey(c.id)){
                    caseCategory = (String)caseCategoryMap.get(c.Id).get(aiCategory);
                    caseSubCategory = (String)caseCategoryMap.get(c.Id).get(aiSubCategory);
                }
            	String aiLang = c.in_gage__AI_Language__c;
            	boolean forCC = checkIfAICCEligible(c, recordTypeEligibleMap, aiLang);
                if(forCC && c.Description != null){
                    //clean description on case
                    system.debug('c.Description: '+ c.Description);
                    String processedDesc = getProcessedData(c.Description);
                    String processedSub = getProcessedData(c.Subject);
                    system.debug('processedDesc: '+ processedDesc);
                    //create new QA_AI record
                    qaAIFList.add(new QA_AI_Feedback__c(Case__c = c.Id
                                                       ,Text_for_Training__c  = processedDesc
                                                       ,Subject_for_Training__c = processedSub
                                                       ,Language__c = aiLang
                                                       ,Category_Sent_for_Training__c = true
                                                       ,Category__c = caseCategory
                                                       ,Sub_Category__c = caseSubCategory));
                    
                    system.debug('qaAIFList: '+ qaAIFList);
                }          
        }
        Database.SaveResult[] srList = Database.insert(qaAIFList);
        for (Database.SaveResult sr : srList) qaAIFBIds.add(sr.getId());
    }
    private String getProcessedData(String description){
        String systemCleansed = '';
        String replacedData = '';
    	if(String.isNotBlank(description)){
            systemCleansed = removeSystemGeneratedData(description); 
            system.debug('systemCleansed: '+ systemCleansed);
            String url_pattern = 'https?://.*?\\s+';
        	String EMAIL_PATTERN = '([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)';                
        	replacedData = systemCleansed.replaceAll(url_pattern, '').replaceAll(EMAIL_PATTERN, ''); 
            system.debug('replacedData: '+ replacedData);
        }
        if(String.isNotBlank(replacedData))return replacedData.replaceAll('[0-9]', '').substringBefore('\n> ');   
        else return systemCleansed.replaceAll('[0-9]', '');
	}
    private String removeSystemGeneratedData(String content){
        map<string,Email_For_Processing_Data__c> domainMap = Email_For_Processing_Data__c.getall();
        for(string dName : domainMap.keyset()) content = content.substringBefore('@'+ dName).substringBefore('--------------- Original Message');
        
        map<string,AI_Ignore_Texts__c > ignoreTextMap = AI_Ignore_Texts__c.getall();
        for(string t : ignoreTextMap.keyset()) content = content.replaceAll(t, '');
        system.debug('content: '+ content);
        return content;
    }
    private boolean checkIfAICCEligible(Case c, map<Id,sObject> recordTypeMap, String aiLang){
        boolean eligible = true;
        map<string,Case_Languages_for_AI__c> langSettingMap = Case_Languages_for_AI__c.getall();
        map<string,AI_Ignore_Case_Origin__c> orignSettingMap = AI_Ignore_Case_Origin__c.getall();
        map<string,Case_Types_to_Ignore_AI__c> typeSettingMap = Case_Types_to_Ignore_AI__c.getall();
        map<string,Case_RTypes_to_Ignore_AI__c> rTypeSettingMap = Case_RTypes_to_Ignore_AI__c.getall();
        if(aiLang!=null && !langSettingMap.isEmpty() && !langSettingMap.containsKey(aiLang)){
        	eligible = false;
        }
        if(c.Origin!=null && !orignSettingMap.isEmpty() && orignSettingMap.containsKey(c.Origin)){
        	eligible = false;
        }
        if(c.Type!=null && !typeSettingMap.isEmpty() && typeSettingMap.containsKey(c.Type)){
         	eligible = false;
        }
        if(recordTypeMap != null && recordTypeMap.containsKey(c.id)){
        	string recordTypeId = (String)recordTypeMap.get(c.Id).get('RecordTypeId');
            if(recordTypeId!=null && !rTypeSettingMap.isEmpty() && rTypeSettingMap.containsKey(recordTypeId)) eligible = false; 
        }
        return eligible;
    }
    
    private Map<Id, sObject> getCaseCategoryMap(Set<Id> caseIds, String aiCategory, String aiSubCategory){
        
        Map<Id,sObject> caseCategoryMap = null;
        SObject so = Schema.getGlobalDescribe().get('Case').newSObject(); 
        string theQuery = '';
        if(aiCategory!=null && so.getSobjectType().getDescribe().fields.getMap().containsKey(aiCategory)){
			if(aiSubCategory!=null && so.getSobjectType().getDescribe().fields.getMap().containsKey(aiCategory)){
				theQuery = 'select Id, ' + aiCategory +', '+ aiSubCategory+' From Case WHERE Id IN';
        	}else theQuery = 'select Id, ' + aiCategory + ' From Case WHERE Id IN';     
            if(caseIds.size() > 0){
                theQuery += ':caseIds';
                caseCategoryMap = new Map<Id,sObject>((List<Case>)Database.query(theQuery));
            }
    	}
        return caseCategoryMap;
    }
    
    global void finish(Database.BatchableContext BC){
        //run lanugage detection batch
        Set<Id> qaAIFIds = new Set<Id>();
        for(QA_AI_Feedback__c qa : [select id, Language__c from QA_AI_Feedback__c where id IN :qaAIFBIds]){
            if(qa.Language__c == null) qaAIFIds.add(qa.Id);
        }        
        if(qaAIFIds.size() > 0){
			system.debug('executing AI_Batch_QATrainingLanguageDetection');
            database.executebatch(new AI_Batch_QATrainingLanguageDetection(qaAIFIds),1);
        }
    } 
   
}