public class QualityAIController {
    
    public QA_AI__c qaAI {get; set;}
    public QAAIResult qaAIResult {get;set;}
    public Integer aiQAScore {get;set;}
    public List<ProcessedSentence> psList {get; set;}
    public boolean qaSEAIEnabled {get;set;}
    public boolean qaAIEnabled {get;set;}

    ApexPages.StandardController sc2;
    
    public QualityAIController(ApexPages.StandardController sc) {
        
        if(!test.isRunningTest()){
            list<string> addFields = new list<string>{'AI_Sentiment_Response__c', 'FirstTopCategoryLabel__c', 'FirstTopSubCategoryLabel__c','FirstTopCategoryScore__c', 'Categorisation_Only__c', 'For_CC__c', 'For_SE__c', 'Email_Message_Id__c', 'AI_Score__c',
                 'Source__c','Employee__r.Full_Name__c', 'Language__c', 'Parent_Case__r.Id', 'Parent_Case__r.CaseNumber', 'Prediction_Completed__c', 'Text_to_Analyse__c', 'Sentiment_Label__c',
                 'Angry_Emotion__c', 'Very_Negative_Sentiment__c', 'Name__c', 'DOB__c', 'Address__c', 'in_gage__High_Risk_Comms__c',
                                    'in_gage__Low_Risk_Comms__c', 'in_gage__To_Investigate_Comms__c'};
            sc.addFields(addFields);
        }
        this.qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        this.qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        this.qaAI = (QA_AI__c) sc.getRecord();
        this.sc2 = sc;
        Decimal aiScore = qaAI.AI_Score__c;
        if(aiScore!=null) aiQAScore = Integer.valueOf(aiScore * 100);
        qaAIResult = new QAAIResult(qaAI.Employee__r.Full_Name__c, qaAI.Parent_Case__r.Id, qaAI.Parent_Case__r.CaseNumber, qaAI.Language__c, 
                                    qaAI.Source__c, aiQAScore, qaAI.Sentiment_Label__c, qaAI.FirstTopCategoryLabel__c, qaAI.FirstTopSubCategoryLabel__c);
        psList = new List<ProcessedSentence>();
        if(qaSEAIEnabled || qaAIEnabled) processSentenceSentiment();
    }
    
    
    
    private void processSentenceSentiment(){        
        if(qaAI.AI_Sentiment_Response__c != null){
            
            PredictionSentAndEmo p = (PredictionSentAndEmo)JSON.deserialize(qaAI.AI_Sentiment_Response__c, PredictionSentAndEmo.class); 
            system.debug('PredictionSentiment: '+ p );
            //proces AI sentiment
            for(PredictionSentence ps : p.sentences){
                //process sentiments
                Sentiment s = ps.sentiment;
                String sentiment = s.classification;
                
                ProcessedSentence sen = new ProcessedSentence();
                sen.text = ps.sentence;
                sen.sentLabel = sentiment;
                if(sentiment == 'VeryNegative' || sentiment == 'Very Negative' ){
                    sen.color = 'ff3333';
                    sen.sentLabel = 'Very Negative';
                }else if(sentiment == 'Negative'){
                    sen.color = '#FF8C00';
                }else if(sentiment == 'Positive'){
                    sen.color = '#4286f4';
                }else if(sentiment == 'VeryPositive' || sentiment == 'Very Positive'){
                    sen.color = '#3CB371';
                    sen.sentLabel = 'Very Positive';
                }else{
                    sen.color = 'black';
                }
                String color = sen.color;
                system.debug('SEN: '+ sen);
                //process emotions
                String emotion = '';
                List<Emotion> emotions = ps.emotions;
                system.debug('emotions: '+ emotions);
                if(!emotions.isEmpty()){
                    for(integer i=0;i<emotions.size();i++){//Emotion e : ps.emotions
                        Emotion e = emotions[i];
                        system.debug('e: '+ e);
                        system.debug('i: '+ i);
                    	//Decimal relevance = e.confidence!=null ? (Decimal.valueOf(e.confidence)).setScale(2) : 0.00;
                    	//if(i==0) sen.topEmoLabel = e.classification + '(' + relevance + ')';
                    	//else emotion += e.classification + '(' + relevance + ')' +', ';
                    	if(i==0) sen.topEmoLabel = e.classification;
                        system.debug('sen.topEmoLabel: '+ sen.topEmoLabel);
                	}
                	//sen.otherEmotions = emotion;   
                }

                psList.add(sen);
            }  
        }
        
    }
     
    public Class QAAIResult{
        public String empName{get;set;}
        public Id pCaseId{get;set;}
        public String pCaseNo{get;set;}
        public String lang{get;set;}
        public String source{get;set;}
        public Integer qaScore{get;set;}
        public String sentimentLabel{get;set;}
        public String aiCategory{get;set;}
        public String aiSubCategory{get;set;}
        
        public QAAIResult(String eName, Id pCaseId, String pCaseNo, String lang, String source, Integer qaScore, String sLabel, String aiCategory, String aiSubCategory){
            this.empName = eName;
            this.pCaseId = pCaseId;
            this.pCaseNo = pCaseNo;
            this.lang = lang;
            this.source = source;
            this.qaScore = qaScore;
            this.sentimentLabel = sLabel;
            this.aiCategory = aiCategory;
            this.aiSubCategory = aiSubCategory;                
                              
        }
    }
    public Class ProcessedSentence{
        public String text {get; set;}
        public String color {get; set;}
        public String sentLabel {get;set;}
        public String topEmoLabel {get;set;}
        public String otherEmotions {get;set;}
    }
     public class PredictionSentAndEmo{
        public List<PredictionSentence> sentences {get;set;}
    }
    public class PredictionSentence{
        public String sentence {get; set;}
        public Sentiment sentiment {get;set;}
        public List<Emotion> emotions {get;set;}
    }
    public class Sentiment{
        public String classification {get; set;}
        public String confidence {get; set;}
    }
    public class Emotion{
        public String classification {get; set;}
        public String confidence {get; set;}
    }
}