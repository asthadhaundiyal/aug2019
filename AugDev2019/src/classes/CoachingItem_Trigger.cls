public with sharing class CoachingItem_Trigger extends TriggerHandler {

    // *************************************
    // ******* Public Declarations *********
    // *************************************

    // ******************************
    // ******* Constructor **********
    // ******************************
    public CoachingItem_Trigger() {}

    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {
			system.debug('create coaching item');
			createEvents((List<Coaching_Items__c>) newObjects, null);
		}

    // **********************************
    // ******* Before Update ************
    // **********************************
    //public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {}

    // **********************************
    // ******* Before Delete ************
    // **********************************
    //public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Insert *************
    // **********************************
    //public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Update *************
    // **********************************
    //public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Delete *************
    // **********************************
    //public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    //public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}

    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    private void createEvents(List<Coaching_Items__c> newObjects, Map<Id, Coaching_Items__c> oldObjectsMap) {

        for(Coaching_Items__c c : newObjects){

					c.Manager_s_Email__c=c.Manager_Email__c;
        }
    }

    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
}