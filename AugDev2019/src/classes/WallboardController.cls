global class WallboardController {

    public static list<Wallboard__c> wallboardList {get; set;}
    public final integer displayTime {get; set;}
    public final integer refreshTime {get; set;}
    private final integer refreshFrequency;

    /*  // the original and full SOQLs for reference only.  Roy Lloyd, July 2017.
    private map<string,string> QUERY_MAP = new map<string,string>{
        'FCR1' => 'SELECT First_Case_Resolution__c FCR,count(id) FROM case WHERE ClosedDate = last_n_days:30 GROUP BY First_Case_Resolution__c',
        'FCR2' => 'SELECT Employee__r.Manager_Employee__c,First_Case_Resolution__c, count(id) FROM Case  WHERE First_Agent__c != null AND Employee__c != null AND Employee__r.Manager_Employee__c != null AND ClosedDate = last_n_days:30 GROUP BY Employee__r.Manager_Employee__c,First_Case_Resolution__c',
        'FCR3' => 'SELECT Employee__c,First_Case_Resolution__c, count(id) FROM Case  WHERE First_Agent__c != null AND Employee__c != null AND ClosedDate = last_n_days:30 GROUP BY Employee__c,First_Case_Resolution__c',
        'NPS1' => 'SELECT sum(NPS_Comp_Value__c), count(id) FROM NPS__c WHERE Completed_Date__c = last_n_days:7 AND Employee__r.Has_In_Gage_Licence__c = true',
        'NPS2' => 'SELECT Employee__r.Manager_Employee__c, sum(NPS_Comp_Value__c), count(id) FROM NPS__c WHERE Completed_Date__c = last_n_days:30 AND Employee__r.Has_In_Gage_Licence__c = true AND Employee__r.Manager_Employee__c != null GROUP BY Employee__r.Manager_Employee__c',
        'NPS3' => 'SELECT Employee__c, sum(NPS_Comp_Value__c), count(id) FROM NPS__c WHERE Completed_Date__c = last_n_days:30 AND Employee__r.Has_In_Gage_Licence__c = true GROUP BY Employee__c',
        'QUA1' => 'SELECT avg(Percentage_Completed__c) FROM Quality_Assessment_Form__c WHERE Submit_Form__c = true AND Submitted_Form_Date__c = last_n_days:30',
        'QUA2' => 'SELECT Agent__r.Manager_Employee__c, avg(Percentage_Completed__c) FROM Quality_Assessment_Form__c WHERE Submit_Form__c = true AND Submitted_Form_Date__c = last_n_days:30 AND Agent__r.Manager_Employee__c != null GROUP BY Agent__r.Manager_Employee__c',
        'QUA3' => 'SELECT Agent__c, avg(Percentage_Completed__c) FROM Quality_Assessment_Form__c WHERE Submit_Form__c = true AND Submitted_Form_Date__c = last_n_days:90 GROUP BY Agent__c'
    };
        */
    
    private map<string,string> QUERY_MAP = new map<string,string>{
        'FCR1' => 'SELECT in_gage__First_Case_Resolution__c FCR,count(id) FROM case {0} GROUP BY in_gage__First_Case_Resolution__c',
        'FCR2' => 'SELECT in_gage__Employee__r.in_gage__Manager_Employee__c,in_gage__First_Case_Resolution__c, count(id) FROM Case {0} GROUP BY in_gage__Employee__r.in_gage__Manager_Employee__c,in_gage__First_Case_Resolution__c',
        'FCR3' => 'SELECT in_gage__Employee__c,in_gage__First_Case_Resolution__c, count(id) FROM Case {0} GROUP BY in_gage__Employee__c,in_gage__First_Case_Resolution__c',
        'NPS1' => 'SELECT sum(in_gage__NPS_Comp_Value__c), count(id) FROM in_gage__NPS__c {0}',
        'NPS2' => 'SELECT in_gage__Employee__r.in_gage__Manager_Employee__c, sum(in_gage__NPS_Comp_Value__c), count(id) FROM in_gage__NPS__c {0} GROUP BY in_gage__Employee__r.in_gage__Manager_Employee__c',
        'NPS3' => 'SELECT in_gage__Employee__c, sum(in_gage__NPS_Comp_Value__c), count(id) FROM in_gage__NPS__c {0} GROUP BY in_gage__Employee__c',
        'QUA1' => 'SELECT avg(in_gage__Percentage_Completed__c) FROM in_gage__Quality_Assessment_Form__c {0}',
        'QUA2' => 'SELECT in_gage__Agent__r.in_gage__Manager_Employee__c, avg(in_gage__Percentage_Completed__c) FROM in_gage__Quality_Assessment_Form__c {0} GROUP BY in_gage__Agent__r.in_gage__Manager_Employee__c',
        'QUA3' => 'SELECT in_gage__Agent__c, avg(in_gage__Percentage_Completed__c) FROM in_gage__Quality_Assessment_Form__c {0} GROUP BY in_gage__Agent__c'
        };
    
    private map<string,string> colourMap = new map<string,string>{
        'red' => '#c95454,#ddd',
        'amber' => '#c9c954,#ddd',
        'green' => '#78c953,#ddd'
    };
    
    private map<id,Employee__c> eMap = new map<id,Employee__c>([select id, Full_Name__c, Team_Name__c from Employee__c where Has_In_Gage_Licence__c = true]);
    public map<string,Wallboard__c> wallboardMap {get; set;}
    
    public WallboardController() {
    
        wallboardList = [select id,Name,Logo_URL__c,(select Name, Sort_Values__c, Top_x__c,Gauge_Low_Range__c,Gauge_High_Range__c,SOQL__c from Wallboard_Reports__r order by Display_Order__c asc) from Wallboard__c];
        wallboardMap = new map<string,Wallboard__c>();
        for(Wallboard__c wb: wallboardList){
                string name = wb.Name.split(' ')[0];
                wallboardMap.put(name,wb);
        }
        
        displayTime = 20000; // milliseconds.
        refreshFrequency = 3;  // number of times the report is displayed before refreshing again.
        refreshTime = ((displayTime / 1000) * wallboardList.size() * refreshFrequency)-1;
    }
    
   public list<chartData> getFCRChart1(){
        
        Wallboard__c wallboard = wallboardMap.get('FCR');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[0];
    
        string query = queryString('FCR1',rpt.SOQL__c);
        
        decimal val=0,t=0,f=0;
        for(AggregateResult ar: database.query(query)){
            if(ar.get('FCR') == true){
                t = decimal.valueOf(''+ar.get('expr0'));
            }else{
                f = decimal.valueOf(''+ar.get('expr0'));
            }
        }
        list<chartData> returnData = new list<chartData>();
        decimal value =  t+f > 0 ? (t/(t+f))*100 : 0;
        value = value.setScale(0);
        string gaugeColour = colourMap.get('red');
        if(value >= rpt.Gauge_Low_Range__c) gaugeColour = value < rpt.Gauge_High_Range__c ? colourMap.get('amber') : colourMap.get('green');
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        returnData.add(new chartData('',value,gaugeColour,gaugeMin,gaugeMax));
        return returnData;
    
    }
    
    public list<chartData> getFCRChart2(){
    
        Wallboard__c wallboard = wallboardMap.get('FCR');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[1];
        
        string query = queryString('FCR2',rpt.SOQL__c);
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        
        list<chartData> returnData = fcrValuesByEmployee(query,'in_gage__Manager_Employee__c', gaugeMin, gaugeMax);
        system.debug('returnList = ' + returnData);      
        if(rpt.Sort_Values__c != null) returnData = sortList(rpt.Sort_Values__c,returnData).clone();
        if(rpt.Top_x__c != null) returnData = chopList(returnData, integer.valueOf(rpt.Top_x__c)).clone();
        
        return returnData;
        
    }
    
    
     public list<chartData> getFCRChart3(){
    
        Wallboard__c wallboard = wallboardMap.get('FCR');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[2];
        
        string query = queryString('FCR3',rpt.SOQL__c);
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        list<chartData> returnData = fcrValuesByEmployee(query,'in_gage__Employee__c', gaugeMin, gaugeMax);
                    
        if(rpt.Sort_Values__c != null) returnData = sortList(rpt.Sort_Values__c,returnData).clone();
        if(rpt.Top_x__c != null) returnData = chopList(returnData, integer.valueOf(rpt.Top_x__c)).clone();
        
        return returnData;
    }
    
    public list<chartData> getNPSChart1(){
    
        Wallboard__c wallboard = wallboardMap.get('NPS');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[0];
    
        string query = queryString('NPS1',rpt.SOQL__c);
        decimal comValue = 0,recCount = 0;
        for(AggregateResult ar: database.query(query)){
            comValue += ar.get('expr0') == null ? 0 : decimal.valueOf(''+ar.get('expr0'));
            recCount += decimal.valueOf(''+ar.get('expr1'));
        }
        system.debug(comValue);
        system.debug(recCount);
                
        list<chartData> returnData = new list<chartData>();
        decimal value = recCount ==0 ? 0 : (comValue/recCount)*100;
        value = value.setScale(0);
        string gaugeColour = colourMap.get('red');
        if(value >= rpt.Gauge_Low_Range__c) gaugeColour = value < rpt.Gauge_High_Range__c ? colourMap.get('amber') : colourMap.get('green');
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        returnData.add(new chartData('',value,gaugeColour,gaugeMin,gaugeMax));
        //returnData.add(new chartData('',value,gaugeColour));
        return returnData;
    
    }
    
    public list<chartData> getNPSChart2(){
    
        Wallboard__c wallboard = wallboardMap.get('NPS');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[1];
        
        string query = queryString('NPS2',rpt.SOQL__c);
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        list<chartData> returnData = npsValuesByEmployee(query,'in_gage__Manager_Employee__c', gaugeMin, gaugeMax);
        system.debug('returnList = ' + returnData);
                
        if(rpt.Sort_Values__c != null) returnData = sortList(rpt.Sort_Values__c,returnData).clone();
        if(rpt.Top_x__c != null) returnData = chopList(returnData, integer.valueOf(rpt.Top_x__c)).clone();
        
        return returnData;
        
    }
    
    public list<chartData> getNPSChart3(){
    
        Wallboard__c wallboard = wallboardMap.get('NPS');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[2];
        
        string query = queryString('NPS3',rpt.SOQL__c);
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        list<chartData> returnData = npsValuesByEmployee(query,'in_gage__Employee__c',gaugeMin, gaugeMax);
        system.debug('returnList = ' + returnData);
                
        if(rpt.Sort_Values__c != null) returnData = sortList(rpt.Sort_Values__c,returnData).clone();
        if(rpt.Top_x__c != null) returnData = chopList(returnData, integer.valueOf(rpt.Top_x__c)).clone();
        
        return returnData;
        
    }
    
    
     public list<chartData> getQualityChart1(){
        
        Wallboard__c wallboard = wallboardMap.get('Quality');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[0];
    
        string query = queryString('QUA1',rpt.SOQL__c);
        
        decimal value=0,total=0,recCount=0;
        for(AggregateResult ar: database.query(query)){
            value = ar.get('expr0') == null ? 0 : decimal.valueOf(''+ar.get('expr0'));
        }
        list<chartData> returnData = new list<chartData>();
        value = value.setScale(0);
        value = math.min(100,value);
        value = math.max(0,value);
        string gaugeColour = colourMap.get('red');
        if(value >= rpt.Gauge_Low_Range__c) gaugeColour = value < rpt.Gauge_High_Range__c ? colourMap.get('amber') : colourMap.get('green');
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        returnData.add(new chartData('',value,gaugeColour,gaugeMin,gaugeMax));
        //returnData.add(new chartData('',value,gaugeColour));
        return returnData;
    
    }
    
    public list<chartData> getQualityChart2(){
    
        Wallboard__c wallboard = wallboardMap.get('Quality');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[1];
        
        string query = queryString('QUA2',rpt.SOQL__c);
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        list<chartData> returnData = qualityValuesByEmployee(query,'in_gage__Manager_Employee__c',gaugeMin, gaugeMax);
        system.debug('returnList = ' + returnData);
                
        if(rpt.Sort_Values__c != null) returnData = sortList(rpt.Sort_Values__c,returnData).clone();
        if(rpt.Top_x__c != null) returnData = chopList(returnData, integer.valueOf(rpt.Top_x__c)).clone();
        
        return returnData;
    }
    
    public list<chartData> getQualityChart3(){
    
        Wallboard__c wallboard = wallboardMap.get('Quality');
        if(wallboard == null) return null;
        Wallboard_Report__c rpt = wallboard.Wallboard_Reports__r[2];
        
        string query = queryString('QUA3',rpt.SOQL__c);
        integer gaugeMin = 0;
        if(rpt.Gauge_Low_Range__c!=null)gaugeMin = rpt.Gauge_Low_Range__c.intValue();
        integer gaugeMax = 100;
        if(rpt.Gauge_High_Range__c!=null)gaugeMax = rpt.Gauge_High_Range__c.intValue();
        
        list<chartData> returnData = qualityValuesByEmployee(query,'in_gage__Agent__c',gaugeMin, gaugeMax);
        system.debug('returnList = ' + returnData);
                
        if(rpt.Sort_Values__c != null) returnData = sortList(rpt.Sort_Values__c,returnData).clone();
        if(rpt.Top_x__c != null) returnData = chopList(returnData, integer.valueOf(rpt.Top_x__c)).clone();
        
        return returnData;
    }
    
    
    private list<chartData> fcrValuesByEmployee(string q, string empIdField, integer gaugeMin, integer gaugeMax){
    
        system.debug('empIdField = ' + empIdField);
    
        map<string,map<integer,integer>> employeeFCRmap = new map<string,map<integer,integer>>();
        for(AggregateResult ar: database.query(q)){
        
            string eId = string.valueOf(ar.get(empIdField));
           
            map<integer,integer> trueFalseMap = employeeFCRmap.containsKey(eId) ? employeeFCRmap.get(eId) : new map<integer,integer>();
            integer trueValue= trueFalseMap.containsKey(1) ? trueFalseMap.get(1) : 0;
            integer falseValue= trueFalseMap.containsKey(0) ? trueFalseMap.get(0) : 0;
            
            if(ar.get('in_gage__First_Case_Resolution__c') == true){
                trueValue += integer.valueOf(''+ar.get('expr0'));   
                trueFalseMap.put(1,trueValue);
            }else{
                falseValue += integer.valueOf(''+ar.get('expr0'));
                trueFalseMap.put(0,falseValue);
            }
            employeeFCRmap.put(eId,trueFalseMap);
        
        }
        
        system.debug('employeeFCRmap = '+employeeFCRmap);
        
        list<chartData> returnData = new list<chartData>();
        for(string eId : employeeFCRmap.keySet()){
            if(eMap.containsKey(eId)){
                map<integer,integer> tfMap = employeeFCRmap.get(eId);
                integer empTrue = tfMap.containsKey(1) ? tfMap.get(1) : 0;
                integer empFalse = tfMap.containsKey(0) ? tfMap.get(0) : 0;
                decimal empScore = (decimal.valueOf(empTrue) / decimal.valueOf(empTrue + empFalse) )*100;
                system.debug('empScore for id '+eId+' = '+empScore+'. True = '+empTrue+'. False = '+empFalse);
                                if(empIdField == 'in_gage__Manager_Employee__c' && eMap.get(eId).Team_Name__c !=null)                
                        returnData.add(new chartData(eMap.get(eId).Team_Name__c,empScore.setScale(0), gaugeMin, gaugeMax ));
                else
                    returnData.add(new chartData(eMap.get(eId).Full_Name__c,empScore.setScale(0), gaugeMin, gaugeMax ));
            }
        }
    
        system.debug('returnList = ' + returnData);
    
        return returnData;
    }
    
    
    private list<chartData> npsValuesByEmployee(string q, string empIdField,integer gaugeMin,integer gaugeMax){
    
        list<chartData> returnData = new list<chartData>();
        for(AggregateResult ar: database.query(q)){
            
            string eId = string.valueOf(ar.get(empIdField));
            system.debug('Employee Id '+ eId);
        
            decimal comValue = 0,recCount = 0;
            comValue += ar.get('expr0') == null ? 0 : decimal.valueOf(''+ar.get('expr0'));
            recCount += decimal.valueOf(''+ar.get('expr1'));
            
            decimal empScore = recCount == 0 ? 0 : (comValue / recCount) *100;
            empScore = math.max(empScore,0);
            if(!eMap.isEmpty() && eId!=null && eMap.get(eId)!= null){
                if(empIdField == 'in_gage__Manager_Employee__c' && eMap.get(eId).Team_Name__c !=null)                
                        returnData.add(new chartData(eMap.get(eId).Team_Name__c,empScore.setScale(0),gaugeMin,gaugeMax));
                else
                        returnData.add(new chartData(eMap.get(eId).Full_Name__c,empScore.setScale(0),gaugeMin,gaugeMax));
            }
        
        }
        return returnData;
    
    }
    
    private list<chartData> qualityValuesByEmployee(string q, string empIdField, integer gaugeMin, integer gaugeMax){
    
        list<chartData> returnData = new list<chartData>();
        
        for(AggregateResult ar: database.query(q)){
        
            string eId = string.valueOf(ar.get(empIdField));
            system.debug('employee Id '+eId);
            
            decimal value = ar.get('expr0') == null ? 0 : decimal.valueOf(''+ar.get('expr0'));            
            if(!eMap.isEmpty() && eId!=null && eMap.get(eId)!= null){
                if(empIdField == 'in_gage__Manager_Employee__c' && eMap.get(eId).Team_Name__c !=null)
                        returnData.add(new chartData(eMap.get(eId).Team_Name__c,value.setScale(0), gaugeMin, gaugeMax ));
                else
                   returnData.add(new chartData(eMap.get(eId).Full_Name__c,value.setScale(0), gaugeMin, gaugeMax ));
            }

           
        }
        return returnData;
    }
    
    private static list<chartData> chopList(list<chartData> oldList, integer chop){
    
        list<chartData> returnData = new list<chartData>();
        if(chop < oldList.size()){
            integer startingPoint = math.max(oldList.size()-chop, 0);
            for(integer i = startingPoint  ; i < oldList.size(); i++){
                returnData.add(oldList[i]);
            }
        }else{
            returnData = oldList;
        }
        return returnData;
    }
    
    private static list<chartData> sortList(string direction, list<chartData> oldList){
    
        // get the scores
        set<decimal> scoreSet = new set<decimal>();
        for(integer i=0; i< oldList.size(); i++){ // removing the null values
            if(oldList[i].value != null) scoreSet.add(oldList[i].value);
        }
        list<decimal> scoreList = new list<decimal>(scoreSet);
        
        // sort the scores
        scoreList.sort();
        system.debug('scoreList = '+ scoreList);  // 2, 4, 6
        
        list<chartData> cdList = new list<chartData>();
        if(direction == 'Ascending'){  // we need to perform the opposite, so Descending action
            
            list<decimal> upsideDownScoreList = new list<decimal>();
            for(integer i = scoreList.size() ; i>0 ; i-- ){
                upsideDownScoreList.add(scoreList[i-1]);
            }
            scoreList.clear();
            scoreList = upsideDownScoreList;
            
            // sort the data based on the scores
            for(decimal d : scoreList){
                for(integer i=0; i< oldList.size(); i++){
                    if(oldList[i].value == d) cdList.add(oldList[i]);
                }
            }        
        }else{
            // just sort the data based on the scores
            for(decimal d : scoreList){
                for(integer i=0; i< oldList.size(); i++){
                    if(oldList[i].value == d) cdList.add(oldList[i]);
                }
            }        
        }
        system.debug('sortIds output size = '+ cdList.size() );
        return cdList;
    }
    
    
    private string queryString(string key, string whereClause){
    
                string query = QUERY_MAP.get(key);
                if(whereClause == null){
                        whereClause = '';
                }else {
                        whereClause = ' WHERE ' + whereClause + ' ';
                }
                return string.format(query, new list<string>{whereClause});
    }
    
    
    public class chartData{
        public string name{get;set;}
        public decimal value{get;set;}
        public string gaugeColour { get; set; }
        public integer gaugeMin {get;set;}
        public integer gaugeMax {get;set;}
        
        public chartData(){}
        
        public chartData(string n, decimal v){
            this.name =n;
            this.value = v == null? 0: v;
        }
        public chartData(string n, decimal v, integer gmin, integer gmax){
            this.name =n;
            this.value = v == null? 0: v;
            this.gaugeMin = gmin;
            this.gaugeMax = gmax;
        }
        
        public chartData(string n, decimal v, string c, integer gmin, integer gmax){
            this.name =n;
            this.value = v == null? 0: v;
            this.gaugeColour = c;
            this.gaugeMin = gmin;
            this.gaugeMax = gmax;
        }
    }
    
    
    public static void setupData(){
        // used for initial setup of data post deployment and also by test class.
        
                Employee__c emp = new Employee__c(First_Name__c = 'test', Last_Name__c = 'employee',User_Record__c = userInfo.getUserId());
                insert emp;
                
                list<Wallboard__c> wallboards = new list<Wallboard__c>();
                Wallboard__c fcrWB = new Wallboard__c(name ='FCR');
                Wallboard__c npsWB = new Wallboard__c(name ='NPS');
                Wallboard__c quaWB = new Wallboard__c(name ='Quality');
                wallboards.add(fcrWB);
                wallboards.add(npsWB);
                wallboards.add(quaWB);
                insert wallboards;
        
                list<Wallboard_Report__c> wallboardReports = new list<Wallboard_Report__c>{
                        new Wallboard_Report__c(
                                Name='First Case Resolution, Last 30 Days',
                                Sort_Values__c = null,
                                Top_x__c = null,
                                Gauge_Low_Range__c = 25,
                                Gauge_High_Range__c = 75,
                                SOQL__c = 'ClosedDate = last_n_days:30',
                                Display_Order__c = '1',
                                Wallboard__c = fcrWB.id
                        ),
                        new Wallboard_Report__c(
                                Name='FCR By Manager, Last 30 Days',
                                Sort_Values__c = 'Descending',
                                Top_x__c = null,
                                Gauge_Low_Range__c = null,
                                Gauge_High_Range__c = null,
                                SOQL__c = 'in_gage__First_Agent__c != null AND in_gage__Employee__c != null AND in_gage__Employee__r.in_gage__Manager_Employee__c != null AND ClosedDate = last_n_days:30',
                                Display_Order__c = '2',
                                Wallboard__c = fcrWB.id
                        ),
                        new Wallboard_Report__c(
                                Name='Top 10 FCR Performers, Last 30 Days',
                                Sort_Values__c = 'Descending',
                                Top_x__c = 10,
                                Gauge_Low_Range__c = null,
                                Gauge_High_Range__c = null,
                                SOQL__c = 'in_gage__First_Agent__c != null AND in_gage__Employee__c != null AND ClosedDate = last_n_days:30',
                                Display_Order__c = '3',
                                Wallboard__c = fcrWB.id
                        ),
                        
                        new Wallboard_Report__c(
                                Name='Customer NPS Last 7 Days',
                                Sort_Values__c = null,
                                Top_x__c = null,
                                Gauge_Low_Range__c = 20,
                                Gauge_High_Range__c = 50,
                                SOQL__c = 'in_gage__Completed_Date__c = last_n_days:7 AND in_gage__Employee__r.in_gage__Has_In_Gage_Licence__c = true',
                                Display_Order__c = '1',
                                Wallboard__c = npsWB.id
                        ),
                        new Wallboard_Report__c(
                                Name='NPS by Manager Last 30 Days',
                                Sort_Values__c = 'Descending',
                                Top_x__c = null,
                                Gauge_Low_Range__c = null,
                                Gauge_High_Range__c = null,
                                SOQL__c = 'in_gage__Completed_Date__c = last_n_days:30 AND in_gage__Employee__r.in_gage__Has_In_Gage_Licence__c = true AND in_gage__Employee__r.in_gage__Manager_Employee__c != null',
                                Display_Order__c = '2',
                                Wallboard__c = npsWB.id
                        ),
                        new Wallboard_Report__c(
                                Name='Top 10 NPS by Individual Last 30 days',
                                Sort_Values__c = 'Descending',
                                Top_x__c = 10,
                                Gauge_Low_Range__c = null,
                                Gauge_High_Range__c = null,
                                SOQL__c = 'in_gage__Completed_Date__c = last_n_days:30 AND in_gage__Employee__r.in_gage__Has_In_Gage_Licence__c = true',
                                Display_Order__c = '3',
                                Wallboard__c = npsWB.id
                        ),
                        
                        new Wallboard_Report__c(
                                Name='Quality Score last 30 days',
                                Sort_Values__c = null,
                                Top_x__c = null,
                                Gauge_Low_Range__c = 60,
                                Gauge_High_Range__c = 75,
                                SOQL__c = 'in_gage__Submit_Form__c = true AND in_gage__Submitted_Form_Date__c = last_n_days:30',
                                Display_Order__c = '1',
                                Wallboard__c = quaWB.id
                        ),
                        new Wallboard_Report__c(
                                Name='Quality Assessment Score by Manager, Last 30 Days',
                                Sort_Values__c = 'Descending',
                                Top_x__c = null,
                                Gauge_Low_Range__c = null,
                                Gauge_High_Range__c = null,
                                SOQL__c = 'in_gage__Submit_Form__c = true AND in_gage__Submitted_Form_Date__c = last_n_days:30 AND in_gage__Agent__r.in_gage__Manager_Employee__c != null',
                                Display_Order__c = '2',
                                Wallboard__c = quaWB.id
                        ),
                        new Wallboard_Report__c(
                                Name='Quality Scores by Individuals, Last 90 Days',
                                Sort_Values__c = 'Descending',
                                Top_x__c = null,
                                Gauge_Low_Range__c = null,
                                Gauge_High_Range__c = null,
                                SOQL__c = 'in_gage__Submit_Form__c = true AND in_gage__Submitted_Form_Date__c = last_n_days:90',
                                Display_Order__c = '3',
                                Wallboard__c = quaWB.id
                        )
                        
                };
                insert wallboardReports;
                
        }
   
}