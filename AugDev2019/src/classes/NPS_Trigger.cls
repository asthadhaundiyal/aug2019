public without sharing class NPS_Trigger extends TriggerHandler {
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    //AI
    boolean npsAIEnabled {get;set;}
    //AI 
    // *************************************
    // ******* Public Declarations *********
    // *************************************
    
    // ******************************
    // ******* Constructor **********
    // ******************************
    public NPS_Trigger() {
        //AI
       	this.npsAIEnabled = FeatureConsoleAPI.npsWithAIEnabled();
        //AI
    }
    
    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {
        updateLoyaltyFields((list<NPS__c>) newObjects);
        updateFields((list<NPS__c>) newObjects);
        //if(npsAIEnabled && setting.Update_Reason_For_New_Model__c) updateNPSReason((List<NPS__c>) newObjects);
    }
    
    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        updateFields((list<NPS__c>) newObjects);
        //if(npsAIEnabled && setting.Update_Reason_For_New_Model__c) updateNPSReason((List<NPS__c>) newObjects);
    }
    
    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}
    
    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
        //createEvents((List<NPS__c>) newObjects, null);  // Live Coaching Form update not required.  as per Rod 16/5/2016
        reviewAnalytics((List<NPS__c>) newObjects);
        //AI
        if(npsAIEnabled && !setting.Reduce_NPS_AI_Processing__c) createAIEvents((List<NPS__c>) newObjects);
        //AI
    }
    
    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
        
        //createEvents((List<NPS__c>) newObjects, (Map<Id, NPS__c>) oldObjectsMap);  // Live Coaching Form update not required.  as per Rod 16/5/2016
        reviewAnalytics((List<NPS__c>) newObjects);
    }
    
    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {
        
        // createEvents((List<NPS__c>) oldObjects, null);  // Live Coaching Form update not required.  as per Rod 16/5/2016
    }
    
    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {
        
        // createEvents((List<NPS__c>) objects, null);  // Live Coaching Form update not required.  as per Rod 16/5/2016
    }
    
    
    private void updateLoyaltyFields(list<NPS__c> triggerNew){
        system.debug('updateLoyaltyFields');
        ingage_Application_Settings__c setting= ingage_Application_Settings__c.getOrgDefaults();
        
        for(NPS__c nps : triggerNew){
            if(setting != null){
                nps.Loyalty_Threshold__c = 7; // the original default value.
                nps.Loyal_Customer_Flag__c = 0;  // default value
                if(setting.Loyal_Customer__c !=null ) nps.Loyalty_Threshold__c = setting.Loyal_Customer__c;
                if(nps.Answer_2__c >= nps.Loyalty_Threshold__c) nps.Loyal_Customer_Flag__c = 1;
            }
        }
    }
    
    private void updateFields(list<NPS__c> triggerNew){
        system.debug('updateFields');
        list<NPS__c> npsList = new list<NPS__c>();
        for(NPS__c nps : triggerNew){
            if(nps.NPS_Type__c == null && (nps.Case__c != null || nps.Opportunity__c != null)) npsList.add(nps);
        }
        if(!npsList.isEmpty()){
            // get the NPS Type records
            map<string,id> typeMap = new map<string,id>();
            for(NPS_Type__c type : [select id,name from NPS_Type__c]){
                typeMap.put(type.name,type.id);
            }
            system.debug('typeMap = ' + typeMap);
            
            // check for Sales and Service Type options
            list<NPS_Type__c> newTypeList = new list<NPS_Type__c>();
            if(!typeMap.containsKey('Service')) newTypeList.add(new NPS_Type__c(name = 'Service'));
            if(!typeMap.containsKey('Sales')) newTypeList.add(new NPS_Type__c(name = 'Sales'));
            if(!newTypeList.isEmpty()){
                insert newTypeList;
                for(NPS_Type__c type : newTypeList){
                    typeMap.put(type.name,type.id);
                }
            }
            system.debug('populate NPS Fields');
            // populate the NPS fields
            for(NPS__c nps : npsList){
                if(nps.Case__c != null) nps.NPS_Type__c = typeMap.get('Service');
                if(nps.Opportunity__c != null) nps.NPS_Type__c = typeMap.get('Sales');
                system.debug('NPS_Type__c = ' + nps.NPS_Type__c);
            }
            
        }
        system.debug('npsList = '+ npsList);
    }
    //AI
    private void createAIEvents(List<NPS__c> newObjects) { 
        system.debug('createNPSAIEvents');
        for(NPS__c n : newObjects){
            system.debug('n.Feedback_Comment__c: '+ n.Feedback_Comment__c);
            if(n.Feedback_Comment__c!= null){
				system.debug('events: '+ events);
                events.add(new Event(Event.EventType.CREATE_NPS_AI, n.id));                
             }
        }
    }//AI
    
    private void reviewAnalytics(list<NPS__c> triggerNew){
        system.debug('reviewAnalytics after nps is created '+ triggerNew);
        if(!setting.Reduce_FCR30_Calculations__c){
            if(runOnceNPS()){
                integer Analytic_Search_Days = 0;
                if(setting.No_of_Days_Lookup_for_Analytics__c!=null)
                    Analytic_Search_Days = (setting.No_of_Days_Lookup_for_Analytics__c).intValue();
                else Analytic_Search_Days = 30;
                date sDate = Date.today().addDays(-Analytic_Search_Days);
                string searchDate = sDate.year()+'-'+dt(sDate.month())+'-'+dt(sDate.day())+'T00:00:00Z';
                
                system.debug('searchDate: '+ searchDate);
                set<id> npsIds = new set<id>();
                for(NPS__c nps: triggerNew){
                    npsIds.add(nps.id);
                }
                system.debug('npsIds '+ npsIds);
                if(!npsIds.isEmpty()) reviewAnalyticsAtFuture(npsIds,searchDate);
            }
        } 
    }
    
    private static void reviewAnalyticsAtFuture(set<id> npsIds, string searchDate){
        if(system.isBatch() || system.isFuture()){
            reviewAnalyticsAtFuture2(npsIds,searchDate);
        }else{
            reviewAnalyticsAtFuture1(npsIds,searchDate);
        }       
    }
    
    @future
    private static void reviewAnalyticsAtFuture1(set<id> npsIds, string searchDate){
        system.debug('in reviewAnalyticsFuture '+ npsIds);
        reviewAnalyticsAtFuture2(npsIds,searchDate);
    }
    private static void reviewAnalyticsAtFuture2(set<id> npsIds,string searchDate){
        list<NPS__c> npsList = [select id, Account__c from NPS__c where id in: npsIds];
        map<string,Analytics__c> aMap = new map<string,Analytics__c>();
        map<id,Analytics__c> updateMap = new map<id,Analytics__c>();
        
        string query = 'SELECT id, NPS__c,Account__c FROM Analytics__c WHERE (CreatedDate >=' +searchDate+ 'OR LastModifiedDate>=' +searchDate+') AND (recordId__c LIKE \'';
        integer i=0;
        for(NPS__c nps: npsList){
            query += string.valueOf(nps.Account__c);
            if(i < npsList.size()-1) query += '%\' OR recordId__c LIKE \'';
            i++;
        }
        query+= '%\')';
        list<Analytics__c> analyticsQuery = database.query(query);
        system.debug('analyticsQuery: '+ analyticsQuery);
        
        for(NPS__c nps : npsList){
            for(Analytics__c a : analyticsQuery){
                if(nps.Account__c == a.Account__c){ 
                    a.NPS__c = nps.Id;
                    a.stat_Case_NPS__c = null;
                    a.stat_Oppty__c = null;
                    aMap.put(a.recordId__c,a);  
                } 
            }
        }
        system.debug('aMap '+ aMap);
        if(!aMap.isEmpty()){
            // prevent dupe Analytic records
            updateMap.putAll(aMap.values());
        }
        
        set<id> updateIds = new set<id>();
        system.debug('UpdateMap '+ updateMap);
        Database.SaveResult[] saveResults = database.update(updateMap.values(),false);
    }
    private static string dt(integer i){
        string s = string.valueOf(i);
        if(i<10) s='0'+s;
        return s;
    }
    
    /*private void updateNPSReason(List<NPS__c> triggerNew){
        Map<String, NPS_Feedback_Types__c> typeSettingMap = NPS_Feedback_Types__c.getAll();
        if(!typeSettingMap.isEmpty()){  
            for(NPS__c nps : triggerNew){
                String npsLang = nps.Language__c;
                if(npsLang!=null){
                    //create the fbMap for nps language
                    Map<String, Set<String>> fbMap = new Map<String, Set<String>>();
                    for(String feedbackName : typeSettingMap.keySet()){
                        if(npsLang.trim() == typeSettingMap.get(feedbackName).Language__c){
                            Set<String> valuesSet = new Set<String>();
                            String feedbackType = typeSettingMap.get(feedbackName).Type__c;
                            String searchText = typeSettingMap.get(feedbackName).Values__c;
                            List<String> splitText = searchText.split(',');
                            for(String s : splitText)valuesSet.add(s.trim());
                            fbMap.put(feedbackType, valuesSet);
                        }  
                    }
                    system.debug('NPS Feedback Types: '+ fbMap + ' for language: '+ npsLang);
                    //update nps reason
                    nps.Reason__c = '';
                    String npsReason = '';
                    Set<String> npsReasonsSet = null;
                    String fbComment = nps.Feedback_Comment__c;
                    if(fbComment!=null && !fbMap.isEmpty()){
                        List<String> partsList = fbComment.toLowerCase().split('[\'\"\\n\\s,;@&.?$+-]+');
                        for(String fbType : fbMap.keySet()){
                            if(!partsList.isEmpty()){
                                for(String s: partsList){
                                    if(fbMap.get(fbType).contains(s)){
                                        npsReason += fbType+', ';
                                    }
                                }
                            }
                        } 
                    }
                    if(npsReason!=null && npsReason!=''){
                        npsReasonsSet = new Set<String>(npsReason.split(', '));
                    }
                    if(npsReasonsSet!=null){
                        for(String s: npsReasonsSet)nps.Reason__c += s +', ';  
                    }
                }
                
            }
        }      
    }*/
    
    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    /*private map<id,user> GetManagerMapData()
{
return new map<id,user>([select id,managerid from user where isactive=true]);
}

private map<id,Employee__c> GetEmployeeMapData(boolean ByUserID)
{
map<id,Employee__c> retMap = new map<id,Employee__c>();
list<employee__c> emplist = [select id,User_Record__c from Employee__c where User_Record__r.IsActive = true];
for(employee__c e:emplist)
{
if (!ByUserID)
{
retmap.put(e.id,e);
}
else
{
retmap.put(e.User_Record__c,e);
}
}
return retmap;

}*/
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************

}