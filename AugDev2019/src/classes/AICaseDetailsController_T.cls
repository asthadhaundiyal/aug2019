@istest
public class AICaseDetailsController_T {

 	private static Employee__c emp;
	private static account a;
	private static contact con;
    private static EmailMessage e1;
    private static List<QA_AI__c> qaAIList;
    private static Email_For_Processing_Data__c emailSetting;
    private static ingage_Application_Settings__c setting;
    private static Case c;
	    
   private static testMethod void test1(){
		
       	User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
			FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
            FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQAwithAI();
            FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();

            emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
            
            setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
            insert setting;
        
            emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            a = new account(name = 'test', Language__c ='English');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
            
            QA_AI__c qaAI = new QA_AI__c(AI_Score__c = 0.8,
                                              Anger_Score__c = 0.15, Concern_Score__c = 0.25, 
                                              Happiness_Score__c = 0.45,Neutral_Score__c = 0.55,
                                              Urgeny_Score__c = 0.46, Frustration_Score__c = 0.56,   
                                         	  FirstTopCategoryLabel__c = 'TestCategory1', FirstTopCategoryScore__c = 0.5, FirstTopSubCategoryLabel__c = 'TestSubCategory1',
                                              Parent_Case__c = c.id, Source__c = 'Customer', Categorisation_Only__c = true);
            
            QA_AI__c qaAI1 = new QA_AI__c(AI_Score__c = 0.8,
                                              Anger_Score__c = 0.25, Concern_Score__c = 0.27,
                                              Happiness_Score__c = 0.42, Neutral_Score__c = 0.26,
                                              Urgeny_Score__c = 0.46, Frustration_Score__c = 0.56,
                                              Parent_Case__c = c.id, Source__c = 'Agent');
            
            qaAIList = new List<QA_AI__c>();
            qaAIList.add(qaAI);
            qaAIList.add(qaAI1);
            insert qaAIList;

            ApexPages.StandardController stdCont = new ApexPages.StandardController(c);
	    	AICaseDetailsController controller = new AICaseDetailsController(stdCont);
        }
        
        
	 }
}