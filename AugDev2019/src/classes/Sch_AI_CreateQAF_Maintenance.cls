global class Sch_AI_CreateQAF_Maintenance implements Schedulable {
    
    
    global void execute(SchedulableContext sc) {
        
        Datetime startDT = Datetime.now();    
        DateTime closeDT = startDT.addHours(-2);
        system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);

        String startDateTime = startDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String closeDateTime = closeDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        system.debug('startDateTime: '+startDateTime+ ' closeDateTime: '+closeDateTime); 

        string cQuery = 'select id, IsClosed, Employee__c, (select id from Quality_Assessment_Forms__r where AI_Created__c = true order by CreatedDate DESC limit 1) from Case where ClosedDate >=' + closeDateTime + 'AND ClosedDate <=' + startDateTime;
        system.debug('cQuery: '+ cQuery);
        database.executeBatch(new AI_Batch_CreateQAF(cQuery),1);  
        
        rescheduleJob(1); 
        
    }
    
    @testVisible
	@future 
	private static void rescheduleJob(integer frequency){
	
		CronJobDetail cronJob = [SELECT Id,JobType,Name FROM CronJobDetail WHERE Name = 'In-gage AI Create Quality Form Schedule'];
		if(cronJob != null){
			CronTrigger cron = [SELECT Id FROM CronTrigger WHERE CronJobDetailId = :cronJob.id];
			if(cron != null) System.abortJob(cron.id);
		}
            
		Sch_AI_CreateQAF_Maintenance aiQA = new Sch_AI_CreateQAF_Maintenance();
        
        Datetime startDT = Datetime.now();
        DateTime dt = startDT.addHours(frequency);
        system.debug('startDT: '+ startDT + ' dt: '+dt);
       
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
		system.schedule('In-gage AI Create Quality Form Schedule', schedule , aiQA);
	
	}
}