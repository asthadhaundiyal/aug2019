/**
 * Test Class for QualityAssessmentForm_Trigger
 */
@isTest
private class QualityAssessmentForm_Trigger_T {

    /**
     *
     */
     
    static Employee__c emp;
    static in_gage__Quality_Assessment_Form__c qaf;
    static Question_Set__c qs;
    static list<Question_Section__c> sections;
    static list<Question__c> questions;
    static integer sectionCount = 4;
    static integer questionsPerSection = 3;
    
    static{
    	
    	ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
    	insert setting;
    	
    	emp = new Employee__c(First_Name__c = 'Test');
    	insert emp;
    	//emp  = [select id from Employee__c limit 1];
        qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id, Submitted_Form_Date__c = system.today());
    	
    	qs = new Question_Set__c(Active__c = true);
    	insert qs;
    	
    	sections = new list<Question_Section__c>();
		for(integer i=1; i<sectionCount+1 ; i++){
			sections.add(new Question_Section__c(Section_Name__c = 'sec'+i, Section_Number__c = string.valueOf(i), Question_Set__c = qs.id));
    	}
    	insert sections;
    	
    	questions = new list<Question__c>();
    	for(Question_Section__c qSec : sections){
    		for(integer i=0; i<questionsPerSection ; i++){
    			questions.add(new Question__c(Question__c = 'Question'+i,Question_Number__c = i, Scoring__c = i, Question_Section__c = qSec.id));
    		}
    	}
    	insert questions;
    }
     
    static testMethod void test() {
    	
        Test.startTest();
        
        	insert qaf;
        	qaf.Submitted_Form_Date__c = system.today().addDays(1);
        		
        	update qaf;
        	delete qaf;
        	undelete qaf;
        
        Test.stopTest();
    }
    
    static testmethod void testCheckQuestionSet(){
    	
    	system.assertNotEquals(0,questions.size());
    	qaf.Question_Set__c = qs.id;
    	
    	test.startTest();
    	insert qaf;
    	test.stopTest();
    	
    	// check answer records created
    	/* NOW CONTROLLED BY VF PAGES
    	list<Quality_Assessment_Form_Answer__c> answers = [select id from Quality_Assessment_Form_Answer__c where in_gage__Quality_Assessment_Form__c = :qaf.id];
    	system.assertEquals(sectionCount * questionsPerSection, answers.size());
    	system.assertNotEquals(0,answers.size());
    	
    	qaf=[select id, Number_of_Answers__c,Question_Set__c,Agent__c from in_gage__Quality_Assessment_Form__c where id = :qaf.id];
    	system.assertNotEquals(0,qaf.Number_of_Answers__c);
    	system.assertEquals(answers.size(), qaf.Number_of_Answers__c);
    	*/
    	
		// check cannot change the Question Set
		/*  NOW CONTROLLED BY VF PAGE
		boolean qsErrorOccured =false;
		Question_Set__c qs2 = qs.clone();
		insert qs2;
		qaf.Question_Set__c = qs2.id;
		try{
			update qaf;
		}catch(exception e){
			qsErrorOccured = true;
		}
		system.assertEquals(true,qsErrorOccured);
		*/
        
		// check cannot change the Agent (Employee)
		boolean agentErrorOccured =false;
		Employee__c emp2 = new Employee__c(First_Name__c = 'Test2');
    	insert emp2;
		//Employee__c emp2 = [select id from Employee__c where id != :emp.id limit 1];
		try{
			qaf.Agent__c = emp2.id;
			update qaf;
		}catch(exception e){
			agentErrorOccured = true;
			// Errors should be based on one of two factors
			// 1. reparenting of Agent__c is not allowed
			// 2. trigger should cause error if reparenting restriction is removed
		}
		system.assertEquals(true,agentErrorOccured);
    }
    
    static testmethod void testCreateCoachingItems0percent(){
    	
    	system.assertNotEquals(null,emp.id);
    	system.assertNotEquals(0,questions.size());
    	qaf.Question_Set__c = qs.id;
    	insert qaf;
    	
    	// Now managed by VF Page save
    	PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('id', qaf.id);
		Test.setCurrentPage(pageRef);
    	QualityAssessmentFormController controller = new QualityAssessmentFormController(new ApexPages.StandardController(qaf));
		controller.saveQAF();
		list<Quality_Assessment_Form_Answer__c> answers = [select id from Quality_Assessment_Form_Answer__c where in_gage__Quality_Assessment_Form__c = :qaf.id];
    	system.assertNotEquals(0,answers.size());
    	
    	folder f = [select id from folder where DeveloperName = 'In_Gage_Templates' limit 1];
    	EmailTemplate template = [select id,DeveloperName from EmailTemplate where DeveloperName = 'QA_Critical_Error'];
		system.assertNotEquals(null,template);
		CollaborationGroup chatterGroup = new CollaborationGroup(Name = 'testInGageChatterGroup', CollaborationType='Public');
		insert chatterGroup;
		
		test.startTest();
    	
    	// 0 score test
    	qaf.Submit_Form__c = true;
    	update qaf;
    	
    	test.stopTest(); 
    
    }
    
    static testmethod void testCreateCoachingItemsGreaterThan0Percent(){
    	
    	system.assertNotEquals(0,questions.size());
    	qaf.Question_Set__c = qs.id;
    	insert qaf;
    	
    	// Now managed by VF Page save
    	PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('id', qaf.id);
		Test.setCurrentPage(pageRef);
    	QualityAssessmentFormController controller = new QualityAssessmentFormController(new ApexPages.StandardController(qaf));
		controller.saveQAF();
		list<Quality_Assessment_Form_Answer__c> answers = [select id from Quality_Assessment_Form_Answer__c where in_gage__Quality_Assessment_Form__c = :qaf.id];
    	system.assertNotEquals(0,answers.size());
    	
    	folder f = [select id from folder where DeveloperName = 'In_Gage_Templates' limit 1];
    	EmailTemplate template = [select id,DeveloperName from EmailTemplate where DeveloperName = 'QA_Critical_Error'];
		system.assertNotEquals(null,template);
		CollaborationGroup chatterGroup = new CollaborationGroup(Name = 'testInGageChatterGroup', CollaborationType='Public');
		insert chatterGroup;
		
		test.startTest();
    	
    	// <60% score test
    	qaf.Submit_Form__c = false;
    	update qaf;
    	for(integer i =0; i< 4; i++){
    		answers[i].Answer__c = true;
    	}
    	update answers;
    	qaf.Submit_Form__c = true;
    	update qaf;
    	
    	test.stopTest(); 
    
    }
    static testmethod void testCreateCoachingItems100percent(){
    	
    	system.assertNotEquals(0,questions.size());
    	qaf.Question_Set__c = qs.id;
    	insert qaf;
    	
    	// Now managed by VF Page save
    	PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('id', qaf.id);
		Test.setCurrentPage(pageRef);
    	QualityAssessmentFormController controller = new QualityAssessmentFormController(new ApexPages.StandardController(qaf));
		controller.saveQAF();
		list<Quality_Assessment_Form_Answer__c> answers = [select id from Quality_Assessment_Form_Answer__c where in_gage__Quality_Assessment_Form__c = :qaf.id];
    	system.assertNotEquals(0,answers.size());
    	
    	folder f = [select id from folder where DeveloperName = 'In_Gage_Templates' limit 1];
    	EmailTemplate template = [select id,DeveloperName from EmailTemplate where DeveloperName = 'QA_Critical_Error'];
		system.assertNotEquals(null,template);
		CollaborationGroup chatterGroup = new CollaborationGroup(Name = 'testInGageChatterGroup', CollaborationType='Public');
		insert chatterGroup;
		
		test.startTest();
    	
    	// 100% score test
    	qaf.Submit_Form__c = false;
    	update qaf;
   		for(Quality_Assessment_Form_Answer__c a:answers){
    		a.Answer__c = true;
    	}
    	update answers;
    	qaf.Submit_Form__c = true;
    	update qaf;
    	
    	test.stopTest(); 
    	//system.assertEquals(12,answers.size());
    	//system.assertEquals(100, [select Percentage_Completed__c from in_gage__Quality_Assessment_Form__c where id=:qaf.id limit 1].Percentage_Completed__c);
    	
    }
}