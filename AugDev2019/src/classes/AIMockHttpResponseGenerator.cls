@isTest
global class AIMockHttpResponseGenerator implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        if(req.getEndpoint() == 'https://ingage-ai.co.uk/nps/prediction/english/INGAI1802'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('{"predictions": [{"classification": "Resolution","confidence": 0.05}]}');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/quality/prediction/customer-data/english/INGAI1802'){
        	res.setHeader('Content-Type', 'application/json');
            res.setBody('{"sentences":[{"emotions":[{"classification":"Neutral","confidence":0.32805519532002736},{"classification":"Concern","confidence":0.26865262081162367},'
            + '{"classification":"Happiness","confidence":0.22444862423566506},{"classification":"Anger","confidence":0.16688179893702687}],"sentence":"This will not work.",'
            + '"sentiment":{"classification":"Negative","confidence":0.5627007487739374}},{"emotions":[{"classification":"Concern","confidence":0.35853713602724885},'
            + '{"classification":"Happiness","confidence":0.31913043637919153},{"classification":"Neutral","confidence":0.16798868995619418},{"classification":"Anger","confidence":0.13792689471944813}],'
            +'"sentence":"Please provide your address details","sentiment":{"classification":"Positive","confidence":0.5606512629704845}}]}');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/quality/prediction/customer-data/{/INGAI1802'){
        	res.setHeader('Content-Type', 'application/json');
            res.setBody('{"sentences":[{"emotions":[{"classification":"Neutral","confidence":0.32805519532002736},{"classification":"Concern","confidence":0.26865262081162367},'
            + '{"classification":"Happiness","confidence":0.22444862423566506},{"classification":"Anger","confidence":0.16688179893702687}],"sentence":"This will not work.",'
            + '"sentiment":{"classification":"Negative","confidence":0.5627007487739374}},{"emotions":[{"classification":"Concern","confidence":0.35853713602724885},'
            + '{"classification":"Happiness","confidence":0.31913043637919153},{"classification":"Neutral","confidence":0.16798868995619418},{"classification":"Anger","confidence":0.13792689471944813}],'
            +'"sentence":"Please provide your address details","sentiment":{"classification":"Positive","confidence":0.5606512629704845}}]}');
        	res.setStatusCode(200);
        }
        else if(req.getEndpoint() == 'https://ingage-ai.co.uk/category/prediction/english/INGAI1802'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('{"predictions": [{"category": "Account","confidence": 0.3529725191487184,"subcategory": "Account Closure"}]}');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/api/nps/training/english/INGAI1802'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('{"status": "success"}');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/api/sentiment/training/english'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('{"status": "success"}');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/api/emotions/training/english'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('{"status": "success"}');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/api/category/training/english/INGAI1802'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('{"status": "success"}');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/api/category/training/english/INGAI1802'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('Arabic');
        	res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://ingage-ai.co.uk/language/predict'){
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('{"status": "success"}');
        	res.setStatusCode(200);
        }
        return res; 

    }
}