@istest
public class NPSFeedbackController_T {

 	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static nps__c n;
    private static NPS_AI__c nAI;
    private static NPS_AI_Feedback__c naFB;
    private static ingage_Application_Settings__c setting;
    private static NPS_Feedback_Types__c npssetting;
	    
   private static testMethod void test1(){
		
       	User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
			FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', true);
        	FeatureConsoleAPI.enableNPSwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = false);
            insert setting;
            
            npssetting = new NPS_Feedback_Types__c(Name='English Fees',Type__c ='Fees', Values__c ='fee,rates', Language__c='English', AI_Training_Language__c = 'English');        
            insert npssetting;
        
            emp = new employee__c(User_Record__c = thisUser.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(accountId = a.id, contactId = con.id, Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
            insert c;
            
        	naFB = new NPS_AI_Feedback__c(Text_for_Training__c = 'text for training', Category__c = 'Testing1', Language__c = 'English');
            
            insert naFB;
            update naFB;
            delete naFB;
            
            PageReference pageRef = new PageReference('apex/NPSFeedbackForm');
        	pageRef.getParameters().put('id', naFB.id);
            pageRef.getParameters().put('retURL', 'testurl');
            pageRef.getParameters().put('rowIndex', '1');
			Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdCont = new ApexPages.StandardController(naFB);
	    	NPSFeedbackController controller = new NPSFeedbackController(stdCont);
            controller.addNPSFB();
            controller.cancelNPSFB();
            controller.deleteRow();

        }
        
        
	 }
}