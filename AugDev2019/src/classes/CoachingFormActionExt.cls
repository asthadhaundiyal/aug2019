public class CoachingFormActionExt {

    Coaching_Form__c cf;

    public CoachingFormActionExt(ApexPages.StandardController controller) {
        cf = (Coaching_Form__c)controller.getRecord();
    }

    public pagereference recalcCF(){        
        cf.Recalculate__c = true;
        update cf;
        
        pagereference refresh = new pagereference('/'+ApexPages.currentPage().getParameters().get('id') );
        refresh.setRedirect(true);
        return refresh;
    }
    
}