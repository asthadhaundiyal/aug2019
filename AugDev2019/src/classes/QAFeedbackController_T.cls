@isTest
private class QAFeedbackController_T {

    	
    private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static QA_AI__c qaAI;
    private static QA_AI_Feedback__c qaFB;
    private static ingage_Application_Settings__c setting;
	
    static testMethod void test() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
			FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQAwithAI();
            FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = false, AI_Case_Category__c = 'Type', AI_Case_SubCategory__c ='in_gage__Case_SubCategory__c');
            insert setting;
        
            emp = new employee__c(User_Record__c = thisUser.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New', in_gage__AI_Language__c = 'English');
            insert c;

            c.AccountId = a.id;
            update c;
            
            qaAI = new QA_AI__c(Text_to_Analyse__c = 'test', Text_to_Analyse_Subject__c = 'test', Language__c = 'English', Parent_Case__c = c.id, 
                                        Source__c = 'Customer', Categorisation_Only__c=true, For_SE__c = true, For_CC__c= true, AI_Score__c = 0.5);
            insert qaAI;
            
        	qaFB = new QA_AI_Feedback__c(QA_AI__c = qaAI.id, Text_for_Training__c = 'text for training', Category__c = 'Testing1', Language__c = 'English', Sub_Category__c ='test1', Emotion__c ='Happiness', Sentiment__c ='Positive');
            
            insert qaFB;
            update qaFB;
            delete qaFB;
            PageReference pageRef = new PageReference('apex/QAFeedbackForm');
        	pageRef.getParameters().put('id', qaFB.id);
            pageRef.getParameters().put('retURL', 'testurl');
            pageRef.getParameters().put('rowIndex', '1');
			Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdCont = new ApexPages.StandardController(qaFB);
	    	QAFeedbackController controller = new QAFeedbackController(stdCont);
            controller.addRow();
            controller.cancelQAFB();
			controller.updateSubCategory();
        }
    }
}