/**
 * Class that represents an event to update Goals in a Coaching Form
 */
public class Event{
    
    public enum EventType {CASE_CHANGE, FCR_CHANGE, NPS_CHANGE, QAF_CHANGE, CF_CHANGE, GET_AI_LANGUAGE, CREATE_NPS_AI, NPS_AI_TRAINING, 
                           CREATE_QA_AI_CASE, CREATE_QA_AI_EMAIL, QA_AI_TRAINING, CREATE_QAF}
    public EventType eventType;
    public String agentID;
    public String recordID;
    public Date dt;
    
    /**
     * Constructor
     *
     * @param    eventType - Type of the event
     * @param    agentID - Employee ID to match the Coaching Form
     * @param    recordID - ID of the source record changed
     * @param    dt - Date to match the Coaching Form
     */
    public Event(EventType eventType, String agentID, String recordID, Date dt){
        
        this.eventType = eventType;
        this.agentID = agentID;
        this.recordID = recordID;
        this.dt = dt;
    }
    //AI
    public Event(EventType eventType, String recordID){
       	this.eventType = eventType;
        this.recordID = recordID;
    }
    //AI
}