//Test class for Email Prediction
@istest
public class AI_Batch_EmailPrediction_T {
	
	
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static EmailMessage e1;
    private static Email_For_Processing_Data__c emailSetting;
    private static ingage_Application_Settings__c setting;
	
	private static void setupData(){
        

        emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
        insert emailSetting;
        
        setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
		insert setting;
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test', Language__c ='English');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', in_gage__AI_Language__c ='English', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'New');
		insert c;
        
        e1 = new EmailMessage(TextBody='Your account is unlocked and fully functional now', Incoming=false, ParentId = c.Id);
        insert e1;
        
	}
	
	private static testmethod void testQAAI() {
		
		setupData();
        Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
		Test.startTest();
		// fire the batch process
        string eQuery = 'select id, TextBody, Incoming, CreatedById, CreatedBy.Employee_ID__c, Parent.AI_Language__c, Parent.Employee__c, Parent.Source_Email_Id__c from EmailMessage'; 
        database.executeBatch(new AI_Batch_CreateQAForEmail(eQuery));
		
		Test.stopTest();
	}  
}