@istest
public class Batch_CalculateEmployeeFCR30_T {
	
	
	private static Employee__c emp;
	private static list<Case> caseList = new list<Case>();
    private static ingage_Application_Settings__c setting;
	
	private static void setupData(){
        
		setting = new ingage_Application_Settings__c();
        setting.Reduce_FCR30_Calculations__c = true;
        setting.No_of_Days_Lookup_for_Analytics__c = 30;
		insert setting;
        
		emp = new Employee__c(First_Name__c = 'Test', Has_In_Gage_Licence__c = true);
		insert emp;
		
		system.assertNotEquals(null,emp.id);
		
		caseList.add(new Case(Status = 'Closed',in_gage__First_Employee__c = emp.id));
		caseList.add(new Case(Status = 'Closed',in_gage__First_Employee__c = emp.id,in_gage__First_Case_Resolution__c = false));
		insert caseList;
		
		for(case c : [select closedDate from case]){
			system.assertNotEquals(null,c.ClosedDate);
		}
		
		// data check
		date mind = Date.today().addDays(EventHandler.FCR_DAYS);
		date maxd = Date.today().addDays(1);
		string mindate = mind.year()+'-'+dt(mind.month())+'-'+dt(mind.day())+'T00:00:00Z';
		string maxdate = maxd.year()+'-'+dt(maxd.month())+'-'+dt(maxd.day())+'T00:00:00Z';
		string query ='select id,FCR_30_days__c,FCR_30_Day_Case_Count__c, x30_Day_Case_Count__c, NON_FCR_30_day__c, (select id, ClosedDate, First_Case_Resolution__c from CasesFirstEmployee__r where ClosedDate >='+mindate+' and ClosedDate <='+maxdate+') from Employee__c';
		list<Employee__c> empList = database.query(query);
		system.assertNotEquals(null,empList);
		system.assertNotEquals(null,empList[0].in_gage__CasesFirstEmployee__r);	
		
	}
	
	private static string dt(integer i){
	    string s = string.valueOf(i);
	    if(i<10) s='0'+s;
	    return s;
	}
	
	
	private static testmethod void testFCR() {
		
		setupData();
		
		emp.FCR_30_days__c = null;
		emp.NON_FCR_30_day__c = null;
		update emp;
	
		Test.startTest();

		// schedule the maintenance job
		/*datetime dt = datetime.now();
		dt = dt.addHours(1);
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
		Sch_InGage_Maintenance schMain = new Sch_InGage_Maintenance();
        schMain.testFCRBatch = true;
        String jobId = system.schedule('In-gage Maintenance Schedule '+string.valueOf(date.today()), schedule, schMain);
		
		// check the schedule exists
		integer i = [SELECT count() FROM CronTrigger WHERE id = :jobId];
		System.assertEquals(1,i);*/
		
		// fire the batch process
		database.executebatch(new Batch_CalculateEmployeeFCR30());
		
		Test.stopTest();
	}
    
    private static testmethod void testNewEmployees() {
        // split to two testMethods because "System.UnexpectedException: No more than one executeBatch can be called from within a test method. Please make sure the iterable returned from your start method matches the batch size, resulting in one executeBatch invocation."
		
		setupData();
	
		Test.startTest();

		// schedule the maintenance job
		datetime dt = datetime.now();
		dt = dt.addHours(1);
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
		Sch_InGage_Maintenance schMain = new Sch_InGage_Maintenance();
		String jobId = system.schedule('In-gage Maintenance Schedule '+string.valueOf(date.today()), schedule, schMain);
		
		Test.stopTest();
	}
    
    
    
}