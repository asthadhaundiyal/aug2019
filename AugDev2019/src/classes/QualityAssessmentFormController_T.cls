@istest
public class QualityAssessmentFormController_T {

    static Employee__c emp;
    static in_gage__Quality_Assessment_Form__c qaf;
    static list<Quality_Assessment_Form_Answer__c> answers;
    static list<Question_Section__c> sections;
    static Question_Set__c qs;
    static ingage_Application_Settings__c setting;
    
    static{
    	
        Profile agentProfile = [SELECT Id FROM Profile Limit 1];
    	setting = new ingage_Application_Settings__c(Agent_Profile_ID__c= agentProfile.Id, Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
		insert setting;
        
        user mgr = [select id from user where isActive = true and id != :userInfo.getUserId() limit 1];
        
    	emp = new Employee__c(First_Name__c = 'Test', Manager_User__c = mgr.id);
    	insert emp;
    	user you = new user(id = userInfo.getUserId(), Employee_ID__c = emp.id);
        update you;
       
    	qs = new Question_Set__c(Active__c = true);
    	insert qs;
        
        Critical_Error__c ce1 = new Critical_Error__c(name = 'E1',Question_Set__c = qs.id);
        insert ce1;
        Critical_Error__c ce2 = new Critical_Error__c(name = 'E2',Question_Set__c = qs.id);
        insert ce2;
    	
    	sections = new list<Question_Section__c>();
		for(integer i=1; i<5 ; i++){
			sections.add(new Question_Section__c(Section_Name__c = 'sec'+i, Section_Number__c = string.valueOf(i), Question_Set__c = qs.id));
    	}
    	insert sections;
    	
    	list<Question__c> questions = new list<Question__c>();
    	for(Question_Section__c qSec : sections){
    		for(integer i=1; i<3 ; i++){
    			questions.add(new Question__c(Question__c = 'Question'+i,Question_Number__c = i, Scoring__c = i, Question_Section__c = qSec.id));
    		}
    	}
    	insert questions;
    	
    	qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id, Submitted_Form_Date__c = system.today(), Question_Set__c = qs.id, Type__c = 'Case');
    	
    }
    
    public static testMethod void testCreatingNewRecord(){
        // TESTING public PageReference newQAF()
      
        account a = new account(name ='test');
        insert a;
        case c = new case(subject = 'test',accountId = a.id);
        insert c;
        
        PageReference pageRef = new PageReference('/apex/qafNew');
        pageRef.getParameters().put('id', c.id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardSetController stdSetCont = new ApexPages.StandardSetController(new list<in_gage__Quality_Assessment_Form__c>{qaf});
	    QualityAssessmentFormController QAFcontroller = new QualityAssessmentFormController(stdSetCont);
        
        pageReference newPage = QAFcontroller.newQAF();
        string urlString = newPage.getUrl();
        system.debug('urlString: '+ urlString);
        system.assertEquals(true,urlString.contains('/apex/in_gage__qaf'));
        Test.setCurrentPage(newPage);
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController(qaf);
	    QualityAssessmentFormController QAFcontroller2 = new QualityAssessmentFormController(stdCont);
		
        
    }
    
    private static testMethod void testNewRecord(){
	 	
        PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('mode', 'edit');
		Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdCont = new ApexPages.StandardController(qaf);
	    QualityAssessmentFormController QAFcontroller = new QualityAssessmentFormController(stdCont);
		
		system.assertEquals(null, QAFcontroller.qa.id);
        
        test.startTest();
        
        QAFcontroller.qa.Agent__c = emp.id;
        QAFcontroller.saveQAF();
		
        test.stopTest();
        
        system.assertNotEquals(null, QAFcontroller.qa.id);
	 }
    
    private static testMethod void testRecordFunctionSave(){
	 	
        insert qaf;
        
        PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('clone', '1');
        pageRef.getParameters().put('id', qaf.id);
		Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdCont = new ApexPages.StandardController(qaf);
	    QualityAssessmentFormController QAFcontroller = new QualityAssessmentFormController(stdCont);
		
		test.startTest();
       
        QAFcontroller.saveQAF();
		
        test.stopTest();
        
        system.assertNotEquals(null, QAFcontroller.qa.id);
	 }

    
    
	private static testMethod void testRecordFunctionClone(){
	 	
        insert qaf;
        
        PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('id', qaf.id);
		Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdCont = new ApexPages.StandardController(qaf);
	    QualityAssessmentFormController QAFcontroller = new QualityAssessmentFormController(stdCont);
		
        QAFcontroller.saveQAF();
		
        test.startTest();
       
        Test.setCurrentPage(QAFcontroller.cloneQAF());
        
        test.stopTest();
        
	 }
        
    private static testMethod void testRecordFunctionCancel(){
	 	
        insert qaf;
        
        PageReference pageRef = new PageReference('/apex/qaf');
        //pageRef.getParameters().put('mode', 'edit');
		Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdCont = new ApexPages.StandardController(qaf);
	    QualityAssessmentFormController QAFcontroller = new QualityAssessmentFormController(stdCont);
		
		test.startTest();
        
        QAFcontroller.editQAF();
        system.assertEquals('edit',QAFcontroller.detailmode);
        
        QAFcontroller.cancelQAF();
        //system.assertNotEquals('edit',QAFcontroller.detailmode);
        
        string retURL = string.valueOf(apexPages.currentPage().getURL());
		pageRef.getParameters().put('retURL', retURL);   
        QAFcontroller.cancelQAF();
        
        pageRef.getParameters().put('clone', '1');   
        try{
            QAFcontroller.cancelQAF();
        }catch(exception e){
            // error expected because the test record no longer exists.
            string errorExpected ='Missing id at index: 0';
            //system.assertEquals(errorExpected, e.getMessage());
        }
        
        test.stopTest();
        
	 }
    

	 private static testMethod void testControllerExtensionExistingRecord(){
	 	
        insert qaf;
        
        //PageReference pageRef = new PageReference('/' + qaf.id);
	 	PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('id', qaf.id);
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController stdCont = new ApexPages.StandardController(qaf);
	    QualityAssessmentFormController QAFcontroller = new QualityAssessmentFormController(stdCont);
		system.assertNotEquals('edit',QAFcontroller.detailmode);
		
		QAFcontroller.editQAF();
		system.assertEquals('edit',QAFcontroller.detailmode);
		
		// Answer records now created by VF page on Save
		QAFcontroller.saveQAF();
		answers = [select id,Answer__c,Question__r.Scoring__c,Question_Set__c,Question_Number__c from Quality_Assessment_Form_Answer__c where in_gage__Quality_Assessment_Form__c =: qaf.id];	
    	system.assertNotEquals(0,answers.size());
    	
		system.assertNotEquals(0, QAFcontroller.getsectionList().size());
		QAFcontroller.displayAnswerRecords();
		system.assertNotEquals(0, QAFcontroller.qAndAlist.size());
		
		test.startTest();
		
		QAFcontroller.editQAF();
		map<string,decimal> answerMap = new map<string,decimal>();
		
		Quality_Assessment_Form_Answer__c answer1 = QAFcontroller.qAndAlist.get(sections[0].id)[0].answer;
		Quality_Assessment_Form_Answer__c answer2 = QAFcontroller.qAndAlist.get(sections[0].id)[1].answer;
		
		answer1.Answer__c = true;
		answer2.Answer__c = true;
		answerMap.put(answer1.Question_Number__c, answer1.Question__r.Scoring__c);
		answerMap.put(answer2.Question_Number__c, answer2.Question__r.Scoring__c);

		QAFcontroller.saveQAF();
		
		Test.stopTest();
		
		// check answers saved OK
		decimal totalScore = 0;
		list<Quality_Assessment_Form_Answer__c> checkAnswers = [select id,Answer__c,Score__c,Question_Number__c from Quality_Assessment_Form_Answer__c where in_gage__Quality_Assessment_Form__c =: qaf.id and Question_Number__c in: answerMap.keySet() and Answer__c = true];	
		for(Quality_Assessment_Form_Answer__c check : checkAnswers){
			if(answerMap.containsKey(check.Question_Number__c)) system.assertEquals(answerMap.get(check.Question_Number__c), check.Score__c);
			system.assertEquals(true, check.Answer__c);
			totalScore += check.Score__c;
		}
		system.AssertNotEquals(0, checkAnswers.size());
		
		// check score OK
		system.assertEquals(totalScore, [select id, Total_Score__c from in_gage__Quality_Assessment_Form__c where id = :qaf.id].Total_Score__c);
		
	 }
    
    private static testMethod void test12(){
		
       	User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
            
			FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();

            Email_For_Processing_Data__c emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test', Language__c ='English');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            case c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
            
            QA_AI__c qaAI = new QA_AI__c(AI_Score__c = 0.87,
                                              Anger_Score__c = 0.15, Concern_Score__c = 0.25,
                                              Frustration_Score__c = 0.35, Happiness_Score__c = 0.45,
                                              Neutral_Score__c = 0.55, Urgeny_Score__c = 0.16, Surprise_Score__c = 0.17, 
                                         	  FirstTopCategoryLabel__c = 'TestCategory1', FirstTopCategoryScore__c = 0.5, FirstTopSubCategoryLabel__c = 'TestSubCategory1',
                                              Parent_Case__c = c.id, Source__c = 'Customer', Categorisation_Only__c = true);
            
            QA_AI__c qaAI1 = new QA_AI__c(AI_Score__c = 0.87,
                                              Anger_Score__c = 0.25, Concern_Score__c = 0.27,
                                              Frustration_Score__c = 0.35, Happiness_Score__c = 0.42,
                                              Neutral_Score__c = 0.26, Urgeny_Score__c = 0.39, Surprise_Score__c = 0.66,
                                              Parent_Case__c = c.id, Source__c = 'Agent', Employee__c = emp.id);
            
            List<QA_AI__c> qaAIList = new List<QA_AI__c>();
            qaAIList.add(qaAI);
            qaAIList.add(qaAI1);
            insert qaAIList;
            
			in_gage__Quality_Assessment_Form__c qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id 
                                                                ,Type__c = 'Case'
                                                                ,Case__c = c.id
                                                                ,AI_Created__c = true); 
            insert qaf;
            
            PageReference pageRef = new PageReference('/apex/qaf');
        	pageRef.getParameters().put('id', qaf.id);
			Test.setCurrentPage(pageRef);

			ApexPages.StandardController stdCont = new ApexPages.StandardController(qaf);
	    	QualityAssessmentFormController QAFcontroller = new QualityAssessmentFormController(stdCont);
    	
            
        }
        
        
	 }
}