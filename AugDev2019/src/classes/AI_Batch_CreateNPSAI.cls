global class AI_Batch_CreateNPSAI implements Database.Batchable<sObject>, Database.Stateful{
    
    global final String query;
    global final set<id> npsIds;
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    decimal aiNPSFrequency = setting.NPS_AI_Batch_Frequency__c;
    boolean batchNPSAI = setting.Reduce_NPS_AI_Processing__c;

    //for real time processing
    global AI_Batch_CreateNPSAI(set<id> npsIds){
        this.npsIds = npsIds;
        query = getNPSQuery() + ' where id in :npsIds';
    }
	
    //for batch processing
    global AI_Batch_CreateNPSAI(string q){
		query=q;
	}
    
	private string getNPSQuery(){
        return 'select id, Feedback_Comment__c, AI_Language__c from NPS__c';
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<NPS__c> npsList = (List<NPS__c>)scope;
		List<NPS_AI__c> npsAIList = new List<NPS_AI__c>();
       system.debug('npsList: '+ npsList);
        for(NPS__c nps :npsList){
            if(checkIfAIEligible(nps)){
                //clean feedback comment on nps
                String fComment = nps.Feedback_Comment__c;
                String aiLang = nps.AI_Language__c;
                
                if(fComment != null && aiLang != null){
                    String processedData = getProcessedFeedback(fComment);
                    system.debug('Cleaned Data: '+ processedData);
                    //create new NPS_AI record
                    npsAIList.add(new NPS_AI__c(NPS__c = nps.Id
                                                    ,Text_to_Analyse__c = processedData
                                                    ,Language__c = aiLang));  
                }  
            }          
        }
        if(!npsAIList.isEmpty()) insert npsAIList;
    }
    private String getProcessedFeedback(String npsFeedback){
        String systemCleansed = '';
        String processedFeedback = '';
    	if(String.isNotBlank(npsFeedback)){
            systemCleansed = removeSystemGeneratedData(npsFeedback);
            system.debug('systemCleansed: '+ systemCleansed);
            String url_pattern = 'https?://.*?\\s+';
        	String EMAIL_PATTERN = '([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)';                
        	processedFeedback = systemCleansed.replaceAll(url_pattern, '').replaceAll(EMAIL_PATTERN, ''); 
        }
        if(String.isNotBlank(processedFeedback))return processedFeedback.replaceAll('[0-9]', '');   
        else return systemCleansed.replaceAll('[0-9]', '');
	}
    private String removeSystemGeneratedData(String content){
        map<string,AI_Ignore_Texts__c > ignoreTextMap = AI_Ignore_Texts__c.getall();
        for(string t : ignoreTextMap.keyset()) content = content.replaceAll(t, '');
        system.debug('content: '+ content);
        return content;
    }
    
    global void finish(Database.BatchableContext BC){
        
        //run NPS Prediction batch
        if(batchNPSAI && aiNPSFrequency!=null){
            system.debug('Batch run');            
            /*DateTime startDT = DateTime.now();
			DateTime closeDT = startDT.addDays(-(aiNPSFrequency.intValue()+1));*/
            
            Integer batchFrequency = aiNPSFrequency.intValue() + 1;
            Datetime startDT = Datetime.now();    
            DateTime closeDT = startDT.addHours(-batchFrequency);
            system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);
    
            List<NPS_AI__c> npsAIList = [select id from NPS_AI__c where AI_Category__c = null and CreatedDate >= :closeDT AND CreatedDate <= :startDT]; 
        	set<Id> npsAIIds = new set<Id>();
        	for(NPS_AI__c n: npsAIList)npsAIIds.add(n.id);
        	if(!npsAIList.isEmpty()) database.executebatch(new AI_Batch_NPSPrediction(npsAIIds),1); 
        }else{
            system.debug('Real time');
            List<NPS_AI__c> npsAIList = [select id from NPS_AI__c where NPS__c in :npsIds]; 
        	set<Id> npsAIIds = new set<Id>();
        	for(NPS_AI__c n: npsAIList)npsAIIds.add(n.id);
        	if(!npsAIList.isEmpty()) database.executebatch(new AI_Batch_NPSPrediction(npsAIIds),1);           
        }
        system.debug('batch finished');  
    }
	private boolean checkIfAIEligible(NPS__c n){
        boolean eligible = true;
        map<string,Languages_to_Support_for_AI_NPS__c> langSettingMap = Languages_to_Support_for_AI_NPS__c.getall();
		if(n.AI_Language__c!=null && !langSettingMap.isEmpty() && !langSettingMap.containsKey(n.AI_Language__c)){
        	eligible = false;
        }
        return eligible;
    }    
}