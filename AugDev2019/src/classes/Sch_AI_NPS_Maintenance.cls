global class Sch_AI_NPS_Maintenance implements Schedulable {
		
    global void execute(SchedulableContext sc) {
		
        ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    	decimal aiNPSFrequency = setting.NPS_AI_Batch_Frequency__c;
    	boolean batchNPSAI = setting.Reduce_NPS_AI_Processing__c;

        
        // Run NPS Batch
		//======================
        if(batchNPSAI && aiNPSFrequency!=null){
        	/*DateTime startDT = DateTime.now();
			DateTime closeDT = startDT.addHours(-(aiNPSFrequency.intValue()+1));
        	string startDateTime = startDT.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            string closeDateTime = closeDT.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');*/

            Integer batchFrequency = aiNPSFrequency.intValue() + 1;
            Datetime startDT = Datetime.now();    
            DateTime closeDT = startDT.addHours(-batchFrequency);
            system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);
            
            String startDateTime = startDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            String closeDateTime = closeDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            system.debug('startDateTime: '+startDateTime+ ' closeDateTime: '+closeDateTime);
            
            //run NPS Prediction batch
            string cQuery = 'select id, Feedback_Comment__c, Language__c, AI_Language__c FROM NPS__c where id NOT IN (SELECT NPS__c FROM NPS_AI__c) and CreatedDate >=' + closeDateTime + ' AND CreatedDate <=' + startDateTime;
            system.debug('cQuery: '+ cQuery);
            database.executeBatch(new AI_Batch_CreateNPSAI(cQuery),1); 
            
            //run NPS Training batch
            string fQuery = 'select id, Text_for_Training__c, Category__c, Language__c, Added_for_Training__c, Case_Category__c, NPS_Comp_Value__c from NPS_AI_Feedback__c where Added_for_Training__c!=true and CreatedDate >=' + closeDateTime + 'AND CreatedDate <' + startDateTime; 
			system.debug('fQuery: '+ fQuery);
            database.executeBatch(new AI_Batch_NPSTraining(fQuery),100); 
            
            rescheduleJob(aiNPSFrequency.intValue());
        }
	}
    
    private static string dt(integer i){
        string s = string.valueOf(i);
        if(i<10) s='0'+s;
        return s;
    }
    
    @testVisible
	@future
	private static void rescheduleJob(integer frequency){
	
		CronJobDetail cronJob = [SELECT Id,JobType,Name FROM CronJobDetail WHERE Name = 'In-gage AI NPS Schedule'];
		if(cronJob != null){
			CronTrigger cron = [SELECT Id FROM CronTrigger WHERE CronJobDetailId = :cronJob.id];
			if(cron != null) System.abortJob(cron.id);
		}
            
		Sch_AI_NPS_Maintenance aiNPS = new Sch_AI_NPS_Maintenance();
 
        Datetime startDT = Datetime.now();
        DateTime dt = startDT.addHours(frequency);
        system.debug('startDT: '+ startDT + ' dt: '+dt);
        
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
		system.schedule('In-gage AI NPS Schedule', schedule , aiNPS);
	
	}
	
}