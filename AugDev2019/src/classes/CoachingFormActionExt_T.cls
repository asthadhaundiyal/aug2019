@isTest
public class CoachingFormActionExt_T {
    
    static Employee__c emp;
    static Coaching_Form__c  cf;
    
    static{
    	emp = new Employee__c(First_Name__c = 'Test');
    	insert emp;
        
        cf = new Coaching_Form__c(Agent__c  = emp.id);
        insert cf;
    }
    

    static testMethod void testAction(){
        
        pagereference pr = new pagereference('/'+cf.id);
        test.setCurrentPageReference(pr);
        
        ApexPages.StandardController con = new ApexPages.StandardController(cf);
        CoachingFormActionExt ext = new CoachingFormActionExt(con);
        
        integer i = [select count() from AsyncApexJob];
        system.debug('Async Apex Job > Before = ' + i);
        test.startTest();
        ext.recalcCF();
        integer j = [select count() from AsyncApexJob];
        test.stopTest();
        system.debug('Async Apex Job > After = ' + j);
        system.assertEquals(1, j);
        
    }
    
}