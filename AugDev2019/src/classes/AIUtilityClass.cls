public class AIUtilityClass{
    
    public class MyPickListInfo
    {
        public String validFor;
    }
    
    public static Map<String, List<String>> getFieldDependencies(String objectName, String controllingField, String dependentField)
    {
        Map<String, List<String>> controllingInfo = new Map<String, List<String>>();
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        
        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get(controllingField).getDescribe();
        Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get(dependentField).getDescribe();
        
        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();
        
        for(Schema.PicklistEntry currControllingValue : controllingValues)
        {
            System.debug('ControllingField: Label:' + currControllingValue.getLabel());
            controllingInfo.put(currControllingValue.getLabel(), new List<String>());
        }
        
        for(Schema.PicklistEntry currDependentValue : dependentValues)
        {
            String jsonString = JSON.serialize(currDependentValue);
            
            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);
            
            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();
            
            System.debug('DependentField: Label:' + currDependentValue.getLabel() + ' ValidForInHex:' + hexString + ' JsonString:' + jsonString);
            
            Integer baseCount = 0;
            
            for(Integer curr : hexString.getChars())
            {
                Integer val = 0;
                
                if(curr >= 65)
                {
                    val = curr - 65 + 10;
                }
                else
                {
                    val = curr - 48;
                }
                
                if((val & 8) == 8)
                {
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 0].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 0].getLabel()).add(currDependentValue.getLabel());
                }
                if((val & 4) == 4)
                {
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 1].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 1].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 2) == 2)
                {
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 2].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 2].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 1) == 1)
                {
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 3].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 3].getLabel()).add(currDependentValue.getLabel());                    
                }
                
                baseCount += 4;
            }            
        } 
        
        System.debug('ControllingInfo: ' + controllingInfo);
        
        return controllingInfo;
    }
    
    public static Map<Id,Id> getCaseWithQAFMap(Set<Id> caseIds){    
        Map<Id, Id> empQAFMap = new Map<Id, Id>();
        List<Case> caseList = [select id, IsClosed, (select id, Agent__c from in_gage__Quality_Assessment_Forms__r 
                               where AI_Created__c = true) 
                               from Case where id in :caseIds];
        for(Case c: caseList){
            if(c.in_gage__Quality_Assessment_Forms__r!=null){
                for(in_gage__Quality_Assessment_Form__c qaf : c.in_gage__Quality_Assessment_Forms__r) empQAFMap.put(qaf.Agent__c, qaf.id);
            }
        }
        return empQAFMap;
    }
    public static Map<Id, sObject> getRecordTypeEligibleMap(Set<Id> caseIds){
        
        Map<Id,sObject> recordTypeEligibleMap = null;
        SObject so = Schema.getGlobalDescribe().get('Case').newSObject();
        
        if(so.getSobjectType().getDescribe().fields.getMap().containsKey('RecordTypeId')){
            string theQuery = 'select Id, RecordTypeId from Case WHERE Id IN';      
            if(caseIds.size() > 0){
                theQuery += ':caseIds';
                recordTypeEligibleMap = new Map<Id,sObject>((List<Case>)Database.query(theQuery));
            }
        }
        return recordTypeEligibleMap;
    }
    
    public static Boolean isDecimal(String s){
        if(s == null) return false;
        else if(s.contains('.') && s.indexOf('.') == s.lastIndexOf('.'))
            s = s.replace('.','');
        return s.isNumeric();
    }
    
}