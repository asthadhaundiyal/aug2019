global class AI_Batch_QAPrediction implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    // called in batch size of 1
    
    global final String query;
    global final set<id> qaAIIds;
	ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    decimal aiQAFrequency = setting.QA_AI_Batch_Frequency__c;
    boolean batchQAAI = setting.Reduce_QA_AI_Procesing__c;
    boolean qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
    boolean qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();

    global AI_Batch_QAPrediction(set<id> qaAIIds){
        this.qaAIIds = qaAIIds;
        query = getQAAIForRealTime() + ' where id in :qaAIIds';
    }

    global AI_Batch_QAPrediction(string q){
        this.query = q;
    }
    
	private string getQAAIForRealTime(){
        return 'select id, Text_to_Analyse__c, Text_to_Analyse_Subject__c, AI_Category__c , Categorisation_Only__c , Language__c, For_SE__c, For_CC__c, Parent_Case__r.Origin, Parent_Case__r.Type  from QA_AI__c ';
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<QA_AI__c> qaAIList = (List<QA_AI__c>)scope;
        List<QA_AI__c> qaWithPredictionList = new List<QA_AI__c>();
        Set<Id> caseIds = new Set<Id>();
        for(QA_AI__c qaAI :qaAIList) caseIds.add(qaAI.Parent_Case__r.Id);
        map<Id,sObject> recordTypeEligibleMap = AIUtilityClass.getRecordTypeEligibleMap(caseIds);
        
        for(QA_AI__c qaAI :qaAIList){
            String language = qaAI.Language__c;
            boolean forAIComp = checkIfAICompEligible(qaAI, recordTypeEligibleMap);
            String textToAnalyse = stringToJSONforQAPrediction(qaAI.Text_to_Analyse__c, qaAI.Text_to_Analyse_Subject__c, forAIComp);
            
            if(String.isNotBlank(textToAnalyse) && String.isNotBlank(language)){
                
                String customerId = setting.AI_Customer_ID__c;
                String aiLang = language.toLowerCase();
                system.debug('qaSEAIEnabled: '+ qaSEAIEnabled);
                system.debug('qaAI.For_SE__c: '+ qaAI.For_SE__c);
               
                system.debug('qaAI.Categorisation_Only__c: '+ qaAI.Categorisation_Only__c);
                system.debug('qaAI.For_CC__c: '+ qaAI.For_CC__c);
                
                //AI Sentiment and Emotion
                if((qaSEAIEnabled && qaAI.For_SE__c) || Test.isRunningTest() || (qaAIEnabled && qaAI.For_CC__c && qaAI.Categorisation_Only__c)){
                    HttpRequest req = new HttpRequest();
                    //String endPoint = 'https://ingage-ai.co.uk/quality/prediction/customer-data/english/'+ customerId;
                    String endPoint = 'https://ingage-ai.co.uk/quality/prediction/customer-data/';
                    if(aiLang!=null) endPoint += aiLang + '/'+ customerId;
                    else endPoint += customerId;
                    system.debug('QA Text processing: '+ endPoint);
                    req.setEndpoint(endPoint);             
                    req.setMethod('POST');
                    req.setHeader('Content-Type', 'application/json');
                    req.setBody(textToAnalyse);
                    req.setTimeout(120000);
                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    
                    if (res.getStatusCode() != 200) {
                        System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                    }else {
                        System.debug(res.getBody());
                        qaAI.AI_Sentiment_Response__c = res.getBody();
                        PredictionSentAndEmo p = (PredictionSentAndEmo)JSON.deserialize(res.getBody(), PredictionSentAndEmo.class); 
                        system.debug('p: '+ p);
                        //proces AI sentiment
                        Integer vNegCount = 0;
                        Integer vPosCount = 0;
                        Integer negCount = 0;
                        Integer posCount = 0;
                        Integer neuSentCount = 0;
                        //process AI emotions
                        Decimal angerTotal = 0;
                        Decimal concernTotal = 0;
                        Decimal neutralTotal = 0;
                        Decimal hapTotal = 0;
                        Decimal frusTotal = 0;
                   		Decimal urgTotal = 0;
                        Integer angerCount = 0;
                        Integer concernCount = 0;
                        Integer neutralCount = 0;
                        Integer hapCount = 0;
                        Integer frusCount = 0;
                        Integer urgCount = 0;
                        Decimal senScore = 0.0;
                        String overallSentimentLabel = '';
                        Decimal overallSentimentScore = 0;
                        for(PredictionSentence sen : p.sentences){
                            //process AI sentiments
                            Sentiment s = sen.sentiment;
                            String sentiment = s.classification;
                            system.debug('sentiment: '+ sentiment);
                            //AI Compliance
                            if(sentiment == 'Very Negative'){
                                qaAI.Very_Negative_Sentiment__c = true;
                                vNegCount++; 
                            }//AI Compliance   
                            else if(sentiment == 'Negative') negCount++;
                            else if(sentiment == 'Neutral') neuSentCount++;
                            else if(sentiment == 'Positive') posCount++;
                            else if(sentiment == 'Very Positive') vPosCount++;
                            //process AI emotions
                            String emotion = '';
                            for(Emotion e : sen.emotions){
                                //AI Compliance
                                String topEmotion = sen.emotions[0].classification;
                                system.debug('topEmotion: '+ topEmotion);
                                if(topEmotion == 'Anger') qaAI.Angry_Emotion__c = true;
                                //AI Compliance
                                emotion += e.classification;
                                Decimal relevance = e.confidence!=null ? (Decimal.valueOf(e.confidence)).setScale(2) : 0.00;
                                if(e.classification == 'Neutral'){
                                    neutralTotal += relevance;
                                    neutralCount++;
                                } 
                                if(e.classification == 'Concern'){
                                    concernTotal  += relevance;
                                    concernCount ++;
                                } 
                                if(e.classification == 'Happiness'){
                                    hapTotal += relevance; 
                                    hapCount++;
                                } 
                                if(e.classification == 'Anger'){
                                    angerTotal += relevance;
                                    angerCount++;
                                }
                                if(e.classification == 'Frustration'){
                                    frusTotal += relevance;
                                    frusCount++;
                                }
                                if(e.classification == 'Urgency'){
                                    urgTotal += relevance;
                                    urgCount++;
                                } 
                                
                                if(neutralCount>0) qaAI.Neutral_Score__c = neutralTotal/neutralCount;
                                if(concernCount>0) qaAI.Concern_Score__c  = concernTotal/concernCount;
                                if(hapCount>0) qaAI.Happiness_Score__c = hapTotal/hapCount;
                                if(angerCount>0) qaAI.Anger_Score__c  = angerTotal/angerCount;
                                if(frusCount>0) qaAI.Frustration_Score__c = frusTotal/frusCount;
                                if(urgCount>0) qaAI.Urgeny_Score__c  = urgTotal/urgCount;
                                qaAI.AI_Emotions__c = emotion;
                            }
                        }
                        Integer totalCount = vNegCount + negCount + neuSentCount + posCount + vPosCount;
                        decimal totalSum = (vNegCount * 0) + (negCount * 1) +  (neuSentCount * 2) + (posCount * 3) +  (vPosCount * 4);
                        if(totalCount > 0) senScore = totalSum/totalCount;
                        if(0 <= senScore &&  senScore < 0.5) overallSentimentLabel = 'Very Negative';
                        else if(0.5 <= senScore && senScore < 1.5) overallSentimentLabel = 'Negative';
                        else if(1.5 <= senScore && senScore < 2.5) overallSentimentLabel = 'Neutral';
                        else if(2.5 <= senScore && senScore < 3.5) overallSentimentLabel = 'Positive';
                        else if(3.5 <= senScore) overallSentimentLabel = 'Very Positive';
                        qaAI.Sentiment_Label__c = overallSentimentLabel;
                        qaAI.AI_Score__c  = senScore/3;
                        //AI Complaince
                        if(p.aicomp!=null){
                            if(p.aicomp.name == 1)qaAI.Name__c = true;
                            if(p.aicomp.dob == 1)qaAI.DOB__c = true;
                            if(p.aicomp.address == 1)qaAI.Address__c = true;
                            if(p.aicomp.highRisk == 1) qaAI.in_gage__High_Risk_Comms__c  = true;
                            if(p.aicomp.lowRisk == 1) qaAI.in_gage__Low_Risk_Comms__c = true;
                            if(p.aicomp.to_invetigate == 1) qaAI.in_gage__To_Investigate_Comms__c  = true;
                        }
                    }
                }if(qaAI.Categorisation_Only__c && qaAI.For_CC__c){//AI Case Categorisation
                    
                    Map<Decimal, String> categoryMap = new Map<Decimal, String>();
                    Map<Decimal, String> subCategoryMap = new Map<Decimal, String>();
                    HttpRequest cReq = new HttpRequest();
                    String cEndPoint = 'https://ingage-ai.co.uk/category/prediction/'+ aiLang + '/'+ customerId;
                	system.debug('QA Category Prediction EndPoint: '+ cEndPoint);
                	creq.setEndpoint(cEndPoint);  
                    cReq.setMethod('POST');
                    cReq.setHeader('Content-Type', 'application/json');
                    cReq.setBody(textToAnalyse);
                    cReq.setTimeout(120000);
                    Http chttp = new Http();
                    HTTPResponse cRes = chttp.send(cReq);
                    
                    if (cRes.getStatusCode() != 200) {
                        System.debug('The status code returned was not expected: ' + cRes.getStatusCode() + ' ' + cRes.getStatus());
                    }else {
                        System.debug(cRes.getBody());
                        PredictionForCategory results = (PredictionForCategory)JSON.deserialize(cRes.getBody(), PredictionForCategory.class);
                        System.debug('results: '+ results);
                        for(PredictCategory p : results.predictions){
                            Decimal relevance = Decimal.valueOf(p.confidence);
                            categoryMap.put(relevance, p.category);
                            subCategoryMap.put(relevance, p.subcategory);
                        }
                        system.debug('categoryMap: '+ categoryMap);
                        system.debug('subCategoryMap: '+ subCategoryMap);
                        
                        if(!categoryMap.isEmpty() && !subCategoryMap.isEmpty()){
                               List<Decimal> aList = new List<Decimal>();
                                aList.addAll(categoryMap.keySet());
                                //aList.sort();
                            system.debug('aList: '+ aList);
                                //if(!aList.isEmpty() && aList.size() >= 3){
                                if(!aList.isEmpty()){
                                    
                                    decimal top1 = aList[0];
                                    system.debug('top1: '+ top1);
                                    qaAI.FirstTopCategoryScore__c = top1;
                                    qaAI.FirstTopCategoryLabel__c = categoryMap.get(top1);
                                    qaAI.FirstTopSubCategoryLabel__c = subCategoryMap.get(top1);
                                    qaAI.in_gage__Original_AI_Category__c = categoryMap.get(top1);
                                    qaAI.in_gage__Original_AI_Sub_Category__c = subCategoryMap.get(top1); 
                                }
                            }
                    } 
                }
                qaAI.Prediction_Completed__c = true;
                qaWithPredictionList.add(qaAI);
                
            }
        }
        if(!qaWithPredictionList.isEmpty()) update qaWithPredictionList;
    }
    
    //New Prediction Class For Sentiment
    public class PredictionSentAndEmo{
        public AICompPrediction aicomp {get;set;}
        public List<PredictionSentence> sentences {get;set;}
    }
    public class PredictionSentence{
        public String sentence {get; set;}
        public Sentiment sentiment {get;set;}
        public List<Emotion> emotions {get;set;}
    }
    public class Sentiment{
        public String classification {get; set;}
        public String confidence {get; set;}
    }
    public class Emotion{
        public String classification {get; set;}
        public String confidence {get; set;}
    }
    //New Prediction Class For Sentiment
    
    //For Prediction For Category
    public class PredictionForCategory {
    	List<PredictCategory> predictions {get; set;}
    }
    public class PredictCategory{
        String category {get; set;}
        String confidence {get; set;}
        String subcategory {get; set;}
    }
    //For AI Complaince Prediction
    public class AICompPrediction{
        integer address {get; set;}
        integer dob {get; set;}
        integer name {get; set;}
        integer highRisk {get; set;}
        integer lowRisk {get; set;}
        integer to_invetigate {get; set;}
    }
        
    private static String stringToJSONforQAPrediction(String description, String subject, boolean aiComp){
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
        generator.writeStringField('text', description);
        if(subject!=null) generator.writeStringField('subject', subject);
        if(aiComp) generator.writeStringField('aiComp', 'true');
        else generator.writeStringField('aiComp', 'false');
        generator.writeEndObject();
             system.debug('generator.getAsString(): '+ generator.getAsString());
		return generator.getAsString();
	}
    private boolean checkIfAICompEligible(QA_AI__c qaAI, map<Id,sObject> recordTypeMap){
        boolean eligible = true;
        map<string,Case_Origins_to_Ignore_AI_Comp__c> orignSettingMap = Case_Origins_to_Ignore_AI_Comp__c.getall();
        map<string,Case_RTypes_to_Ignore_AI_Comp__c> rTypeSettingMap = Case_RTypes_to_Ignore_AI_Comp__c.getall();
        map<string,Case_Types_to_Ignore_AI_Comp__c> typeSettingMap = Case_Types_to_Ignore_AI_Comp__c.getall();
		
        if(!qaSEAIEnabled) eligible = false;
        
        if(qaAI.Parent_Case__r.Origin!=null && !orignSettingMap.isEmpty() && orignSettingMap.containsKey(qaAI.Parent_Case__r.Origin)){
        	eligible = false;
        }
        if(qaAI.Parent_Case__r.Type!=null && !typeSettingMap.isEmpty() && typeSettingMap.containsKey(qaAI.Parent_Case__r.Type)){
         	eligible = false;
        }
        if(recordTypeMap != null && recordTypeMap.containsKey(qaAI.Parent_Case__r.id)){
        	string recordTypeId = (String)recordTypeMap.get(qaAI.Parent_Case__r.Id).get('RecordTypeId');
            if(recordTypeId!=null && !rTypeSettingMap.isEmpty() && rTypeSettingMap.containsKey(recordTypeId)) eligible = false; 
        }
        return eligible;
    }
    
    global void finish(Database.BatchableContext BC){
        //run QA Prediction batch
        if(batchQAAI && aiQAFrequency!=null && qaSEAIEnabled){
        	/*DateTime startDT = DateTime.now();
			DateTime closeDT = startDT.addHours(-(aiQAFrequency.intValue()+1));*/
            
            Integer batchFrequency = aiQAFrequency.intValue() + 1;
            Datetime startDT = Datetime.now();    
            DateTime closeDT = startDT.addHours(-batchFrequency);
            system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);
            
			List<Case> caseList = [select id, IsClosed from Case where IsClosed = true and ClosedDate >= :closeDT AND ClosedDate <= :startDT];
            Set<Id> caseIds = new Set<Id>();
            for(Case c : caseList) caseIds.add(c.id);
            if(!caseList.isEmpty()) database.executeBatch(new AI_Batch_CreateQAF(caseIds),1);
        }
    } 
}