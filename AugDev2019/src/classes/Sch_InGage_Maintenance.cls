global class Sch_InGage_Maintenance implements Schedulable {
	
	/* To schedule ->
	Sch_InGage_Maintenance igm = new Sch_InGage_Maintenance();
  	//seconds, minutes, hour of day, day of month, month, day of week, Year
	String schedule = '0 0 22 ? * MON-SUN'; // run the schedule job Mon--SUN  at midnight
	system.schedule('In-gage Maintenance Schedule', schedule , igm);
  	*/
	
	public boolean testFCRBatch = false;
    public ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    public integer analyticsBatchSize = setting.Analytics_Batch_Size__c  == null ? 200 : integer.valueOf(setting.Analytics_Batch_Size__c);
    public integer mainbatchSize = setting.Reminder_Survey_Batch_Size__c == null ? 20 : integer.valueOf(setting.Reminder_Survey_Batch_Size__c);
    //Feature & QA changes
    public Boolean qualityEnabled = FeatureConsoleAPI.qaEnabled();
    public Boolean npsEnabled = FeatureConsoleAPI.npsEnabled();
    public Boolean eeEnabled = FeatureConsoleAPI.eeEnabled();
    public Boolean fcrEnabled = FeatureConsoleAPI.fcrEnabled();
    
		
    global void execute(SchedulableContext sc) {
	// suggested timing is 2200hrs to run the FCR30 batch.
		
		// Employee record update
		//=======================
		// creates Employee records for licenced users, where they do not exist.
        if(!test.isRunningTest() || !testFCRBatch ){  // test method can only call one batch at a time.
        	map<id,user> userMap = new map<id,user>([select id from User where isActive = true]);
			User_Trigger.createEmployee(userMap.keySet());  // runs @Future due to CPU timeouts
			//User_Trigger.createEmployeeRecord(userMap.keySet()); // ran synchronously
        }        
		// Check Licenced Users and update Has_In_Gage_Licence__c on the Employee table
		//=============================================================================
		set<id> licencedUserIds = User_Trigger.licencedUsers();
		List<Employee__c> allEmployees = new List<Employee__c>();
		try{
			allEmployees=[Select ID,Has_In_Gage_Licence__c,User_Record__c,Employee_Start_Date__c from Employee__c /*where user_record__r.isactive=:true*/ Limit 10000];
		}catch(exception e){
			system.debug(e);
		}
		list<Employee__c> updateEmployees = new list<Employee__c>();
        set<id> userIDs=new set<id>();
        for(Employee__c e: allEmployees){
          userIDs.add(e.User_Record__c);
        }
        system.debug('userIDs: '+ userIDs);
        Map<id,User> userMap= new Map<ID, User>([SELECT ID, CreatedDate from user where ID in: userIDs]);
		system.debug('userMap: '+ userMap);
        for(Employee__c e:allEmployees){
            //update employee db date filters
            Date startDate = Date.today();
            e.DB_Start_Date__c = startDate.addDays(-30);
            e.DB_End_Date__c = startDate;
            e.QA_Start_Date__c = startDate.addDays(-30);
            e.QA_End_Date__c = startDate;
			boolean hasLicence = licencedUserIds.contains(e.User_Record__c);
            system.debug('hasLicence: '+ hasLicence);
			if(hasLicence){
				if(!e.Has_In_Gage_Licence__c) e.Has_In_Gage_Licence__c = true;
                if(e.Employee_Start_Date__c == null && userMap.containsKey(e.User_Record__c))
		        	e.Employee_Start_Date__c  = userMap.get(e.User_Record__c).CreatedDate;
				updateEmployees.add(e);  // update all licenced employees
			}else{
				if(e.Has_In_Gage_Licence__c){
					e.Has_In_Gage_Licence__c = false;
					updateEmployees.add(e); // update only the employees we need to disable.
				}
			}
		}
        
        //reset Bulk Case Upload Checkbox on Ingage Custom Setting
        /*boolean doCaseBulkUpload = setting.Bulk_Case_Upload__c;
        system.debug('resetBulkCaseUpload');
        setting.Bulk_Case_Upload__c = false;
        try{ 
            update setting;
        }catch(exception e){
			system.debug(e);
		}*/
		//UpdateUserPhotoSchedule
		//=======================	
		// this schedulable class updates all Employees (max 10000). The update operation will fire the
		// Employee Trigger which in turn will bring User.SmallPhotoURL copied to Employee record.
		// User.SmallPhotoURL field is not available in the formulas currently -:)
		system.debug('updateEmployees: '+ updateEmployees);
        database.update(updateEmployees,false); // probably best not to alert users if there are validation errors
		// Sch_BatchCalculateEmployeeFCR30
		//================================
		
        //update CoachingForms
		string batchQuery;
		batchQuery = 'select id, Performance_Start_Date__c,Performance_End_Date__c,Agent__c,Agent__r.User_Record__c, (select id,Goal__c,Performance__c,CalculationProgressWeight__c from Goals__r) from Coaching_Form__c where Submit_Coaching_Form__c = false AND (Performance_End_Date__c = null OR Performance_End_Date__c >= LAST_N_DAYS:30) AND Agent__c != null AND Performance_Start_Date__c != null';
		database.executeBatch(new Batch_CalculateCoachingForm(batchQuery),1);  // reduced from 25 when Custom Goals launched.
        
        
        if(fcrEnabled){
            // updates FCR30 values for employee records
            if(!test.isRunningTest() || testFCRBatch){  // test method can only call one batch at a time.
                if(setting.Reduce_FCR30_Calculations__c)
                    database.executebatch(new Batch_CalculateEmployeeFCR30(),1); 
            }
            // Batch_CalculateEmployeeFCRbyCaseType
            //================================
            // updates Highest and Lowest FCR for Employee DB for all employees 
            if(!test.isRunningTest() || testFCRBatch){  // test method can only call one batch at a time.
                database.executebatch(new Batch_CalculateEmployeeFCRbyCaseType(),1); 
            }
        }
        
        if(npsEnabled){                
            // Sch_BatchCalculateEmployeeNPS30
            //================================
            // updates Number of surveys in 30/60/90 values for employee records
            if(!test.isRunningTest() || testFCRBatch){  // test method can only call one batch at a time.  Uncomment this
                database.executebatch(new Batch_CalculateEmployeeNPS30(),1); 
            }
            // Sch_Batch_OpportunityReviewAnalytics
            //=========================
            // inserts or updates 
            if(!test.isRunningTest() || testFCRBatch){  // test method can only call one batch at a time.
                if(setting.Reduce_FCR30_Calculations__c)
                    database.executebatch(new Batch_OpportunityReviewAnalytics(),analyticsBatchSize); 
            }
            // Sch_Batch_CaseReviewAnalytics
            //=========================
            // inserts or updates 
            if(!test.isRunningTest() || testFCRBatch){  // test method can only call one batch at a time.
                if(setting.Reduce_FCR30_Calculations__c)
                    database.executebatch(new Batch_CaseReviewAnalytics(),analyticsBatchSize); 
            }       
            // Sch_Batch_NPSReviewAnalytics
            //=========================
            // inserts or updates 
            if(!test.isRunningTest() || testFCRBatch){  // test method can only call one batch at a time.
                if(setting.Reduce_FCR30_Calculations__c)
                    database.executebatch(new Batch_NPSReviewAnalytics(),analyticsBatchSize); 
            }
        }
		
		// Send Reminder Surveys
		//======================
		string d = string.valueOf(date.today());
        string last10days = string.valueOf(Date.today().addDays(-10));
		String q;
        if(eeEnabled){
        	q='SELECT Id, Send_Engagement_Survey__c FROM Employee__c WHERE Survey_Reminder_Date__c >=' + last10days + 'AND Survey_Reminder_Date__c <='+d;
        	database.executeBatch(new Batch_Send_Survey_Reminders(q),mainbatchSize);
        }
        
        if(npsEnabled){
        	q='SELECT Id, Opportunity_Won_Survey_Sent__c FROM Opportunity WHERE Survey_Reminder_Date__c >=' + last10days + 'AND Survey_Reminder_Date__c <='+d;
			database.executeBatch(new Batch_Send_Survey_Reminders(q),mainbatchSize);
        	q='SELECT Id, ownerId, Survey_Reminder_Date__c, Owner.IsActive, Employee__r.Has_In_Gage_Licence__c FROM Case WHERE Owner.IsActive = true and Employee__r.Has_In_Gage_Licence__c = true and Survey_Reminder_Date__c >=' + last10days + 'AND Survey_Reminder_Date__c <='+d;
			database.executeBatch(new Batch_Send_Survey_Reminders(q),mainbatchSize);
        }
        //Reset EmployeeDB health bar and quality date filters
        
	}
	
}