/**
 * Test Class for NPSAI_Trigger
 */
@isTest
private class NPSAI_Trigger_T {

    	
    private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static nps__c n;
    private static NPS_AI__c npsAI;
    private static NPS_Feedback_Types__c npssetting;
    private static ingage_Application_Settings__c setting;
	
    static testMethod void test() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
			FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', true);
        	FeatureConsoleAPI.enableNPSwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            npssetting = new NPS_Feedback_Types__c(Name='English Fees',Type__c ='Fees', Values__c ='fee,rates', Language__c='English', AI_Training_Language__c = 'English' );        
            insert npssetting;
            
            setting = new ingage_Application_Settings__c(in_gage__AI_Customer_ID__c = 'INGAI1802', Reduce_NPS_AI_Processing__c = false);
            insert setting;
        
            emp = new employee__c(User_Record__c = thisUser.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(accountId = a.id, contactId = con.id, Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
            insert c;
            
            n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English');
            insert n;
            
            npsAI = new NPS_AI__c(NPS__c = n.id, Text_to_Analyse__c = 'test', Language__c = 'English');
            insert npsAI;
            
            Test.startTest();
            npsAI.AI_Category__c = 'Service, Fees';
            update npsAI;
            Test.stopTest();
            
        }
    }
}