@isTest
public class SurveyQuestion_Trigger_T {

	static list<string> surveys;
	static Survey__c defaultSurvey;
	static Survey__c newSurvey;
    static  map<string,string> surveyMap;

	private static void setData() {
		
        surveyMap = Survey_Trigger.inGageDefaultSurveyMap();
		surveys = new list<string>(surveyMap.Keyset());
		
		defaultSurvey = new Survey__c();
		defaultSurvey.name = surveys[0];
		defaultSurvey.Submit_Response__c = 'Thanks';
		insert defaultSurvey;
		
		newSurvey = new Survey__c();
		newSurvey.name = 'newSurvey Test';
		newSurvey.Submit_Response__c = 'Thanks';
		insert newSurvey;
        
	}
	
	private static testmethod void testSurveyDMLs(){
		
		setData();
		
		// test changing name of default survey
		string surveyType = surveyMap.get(defaultSurvey.name);
        defaultSurvey.name ='new Name';
        update defaultSurvey;
        system.assertEquals(surveyType, [select id,Type__c from Survey__c where id =: defaultSurvey.id].Type__c);
		
		// test changing name of normal survey
		newSurvey.name ='new name';
		update newSurvey;
        system.assertEquals(null, [select id,Type__c from Survey__c where id =: newSurvey.id].Type__c);
		
		// test deleting surveys
		try{
			delete defaultSurvey;
		}
		catch(exception e){  //failure expected
		}
		delete newSurvey;  // no failure expected
		
		// expect only the default survey to be remaining.
		system.assertEquals(1, [select count() from Survey__c]);
	}
	
	private static testmethod void testSurveyQuestion(){
		
		setData();
		boolean dmlFail;
		
		Survey_Question__c sqDefault1 = new Survey_Question__c(
			name ='How did you hear about us',
			Question__c='How did you hear about us',
			OrderNumber__c =0,
			Choices__c = 'Friends or Family recommendation',
			Survey__c = defaultSurvey.id
		);
		insert sqDefault1;
		
		Survey_Question__c sqDefault2 = new Survey_Question__c(
			name ='How did you hear about us',
			Question__c='How did you hear about us',
			OrderNumber__c =1,
			Choices__c = 'Friends or Family recommendation',
			Survey__c = newSurvey.id
		);
		insert sqDefault2;
		
		Survey_Question__c sqNormal = new Survey_Question__c(
			name ='What is the Question?',
			Question__c='What is the Question?',
			OrderNumber__c =2,
			Choices__c = 'That could depend on the Answer',
			Survey__c = newSurvey.id
		);
		insert sqNormal;
			
		test.startTest();
		
			// update test
			// to fail
			
			//1
				dmlFail = false;
				sqDefault1.name ='new Name';
				try{
					update sqDefault1;
				}catch(exception e){
					dmlFail = true;
				}
				system.assertEquals(true,dmlFail);
			
			//2
				dmlFail = false;
				sqDefault1.Choices__c = 'new choice';
				try{
					update sqDefault1;
				}catch(exception e){
					dmlFail = true;
				}
				system.assertEquals(true,dmlFail);
			
			// to work
			//3 
				sqDefault1.name = 'How did you hear about us';
				sqDefault1.Choices__c = 'Friends or Family recommendation';
				sqDefault1.Guidance__c ='new guidance';
				update sqDefault1;
			//4	
				sqDefault2.name ='new Name2';
				sqDefault2.Choices__c ='new choice';
				update sqDefault2;
			//5	
				sqNormal.name ='new Name2';
				sqNormal.Choices__c ='new choice';
				update sqNormal;
		
		
			// delete test
			// to fail
			
			//1
				dmlFail = false;
				try{
					delete sqDefault1;
				}catch(exception e){
					dmlFail = true;
				}
				system.assertEquals(true,dmlFail);
			
			// to work
			
			//2
				delete sqDefault2;
			
			//3
				delete sqNormal;
				
				
		test.stopTest();
	
	}
	

}