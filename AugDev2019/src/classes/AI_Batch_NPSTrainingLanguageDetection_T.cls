@istest
public class AI_Batch_NPSTrainingLanguageDetection_T {
		
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static nps__c n;
    private static NPS_AI__c nAI;
    private static NPS_AI_Feedback__c naFB;
    private static ingage_Application_Settings__c setting;
    private static NPS_Feedback_Types__c npssetting;
	
	private static void setupData(){
        
       // Feature_InsertForTest.insertNPSAIFeature();
        setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = true, NPS_AI_Batch_Frequency__c = 1);
		insert setting;
        
        npssetting = new NPS_Feedback_Types__c(Name='English Fees',Type__c ='Fees', Values__c ='fee,rates', Language__c='English', AI_Training_Language__c = 'English');        
        insert npssetting;
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
		insert c;
        
        n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high');
		insert n;
        nAI = new NPS_AI__c(Text_to_Analyse__c ='text for training', AI_Category__c  = 'Testing', NPS__c  = n.id);
		insert nAI;
        naFB = new NPS_AI_Feedback__c(NPS_AI__c = nAI.id, Text_for_Training__c = 'text for training', Category__c = 'Testing1');
		insert naFB;
	}
	
	private static testmethod void testTraining() {
		
		setupData();
        Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
		Test.startTest();
		// fire the batch process        
        string fQuery = 'select id, Text_for_Training__c, Language__c from NPS_AI_Feedback__c'; 
		database.executeBatch(new AI_Batch_NPSTrainingLanguageDetection(fQuery),1);
		
		Test.stopTest();
	}  
}