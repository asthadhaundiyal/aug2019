@isTest
private class FeatureConsoleControllerTest {

    static testMethod void testController() {
        resetParams();
        
        // create an instance of the controller and retrieve the list of Feature objects
        FeatureConsoleController controller = new FeatureConsoleController();
        List<FeatureConsoleController.Feature> features = controller.getFeatures();
        // assert that there are 8 features
        System.assertEquals(8, features.size(), 'Unexpected number of features.');
        
        // test that we cannot currently enable either feature
        for(FeatureConsoleController.Feature feature:features) {
            feature.enabled = true;
        }
        // attempt save our changes in the controller
        controller.save();
        // verify that both features are still disabled
        boolean npsEnabled = FeatureConsoleAPI.npsEnabled();
        boolean qaEnabled = FeatureConsoleAPI.qaEnabled();
        boolean fcrEnabled = FeatureConsoleAPI.fcrEnabled();
        boolean eeEnabled = FeatureConsoleAPI.eeEnabled();
        boolean npsAIEnabled = FeatureConsoleAPI.npsWithAIEnabled();
        boolean qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        boolean qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        boolean clAutoDetectEnabled = FeatureConsoleAPI.caseLangAutoDetectEnabled();
        System.assert(!npsEnabled, 'NPS feature is enabled and should not be.');
        System.assert(!qaEnabled, 'QA feature feature is enabled and should not be.');
        System.assert(!fcrEnabled, 'FCR feature is enabled and should not be.');
        System.assert(!eeEnabled, 'EE feature is enabled and should not be.');
        System.assert(!npsAIEnabled, 'NPS AI feature is enabled and should not be.');
        System.assert(!qaAIEnabled, 'QA AI Case Categorisation feature feature is enabled and should not be.');
        System.assert(!qaSEAIEnabled, 'QA AI Emotion and Sentiment feature is enabled and should not be.');
        System.assert(!clAutoDetectEnabled, 'AI Case Language Auto Detect feature is enabled and should not be.');

        FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', true);
        FeatureManagement.setPackageBooleanValue('QA_Feature_Activated', true);
        FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
        FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        FeatureManagement.setPackageBooleanValue('FCR_Feature_Activated', true);
        FeatureManagement.setPackageBooleanValue('EE_Feature_Activated', true);
        FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        // now enable the feature and save the controller
        for(FeatureConsoleController.Feature feature:features) {
            if(feature.name.equals('Enable Customer Survey & ROI Feature')) {
                feature.enabled = true;
            }if(feature.name.equals('Enable Quality Audit Feature')) {
                feature.enabled = true;
            }if(feature.name.equals('Enable AI for Customer Feedback Feature')) {
                feature.enabled = true;
            }
        }        
        controller.save();
        
        // verify that the NPS, QA, NPS AI features is enabled, but not the FCR, EE, QA CC, QA SE features
        npsEnabled = FeatureConsoleAPI.npsEnabled();
        qaEnabled = FeatureConsoleAPI.qaEnabled();
        fcrEnabled = FeatureConsoleAPI.fcrEnabled();
        eeEnabled = FeatureConsoleAPI.eeEnabled();
        npsAIEnabled = FeatureConsoleAPI.npsWithAIEnabled();
        qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        System.assert(npsEnabled, 'NPS feature is disabled and should not be.');
        System.assert(qaEnabled, 'QA feature is disabled and should not be.');
        System.assert(npsAIEnabled, 'NPS AI feature is disabled and should not be.');
        System.assert(!fcrEnabled, 'FCR feature is disabled and should not be.');
        System.assert(!eeEnabled, 'EE feature is disabled and should not be.');
        System.assert(!qaAIEnabled, 'QA AI Case Categorisation is disabled and should not be.');
        System.assert(!qaSEAIEnabled, 'QA AI Emotion and Sentiment feature is disabled and should not be.');
        System.assert(!clAutoDetectEnabled, 'AI Case Language Auto Detect feature is disabled and should not be.');
    }
    
    private static void resetParams() {
        // turn off the gating feature params, just in case they're turned on.
        FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', false);
        FeatureManagement.setPackageBooleanValue('QA_Feature_Activated', false);
        FeatureManagement.setPackageBooleanValue('FCR_Feature_Activated', false);
        FeatureManagement.setPackageBooleanValue('EE_Feature_Activated', false);
        FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', false);
        FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', false);
        FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', false);
        FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', false);
    }
}