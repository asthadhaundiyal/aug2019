@isTest
public with sharing class SurveyFeedback_Trigger_Test
{
    public static testMethod void test1()
    {

        Profile p = [select id FROM Profile WHERE Name ='System Administrator'];
        Profile agentProfile = [SELECT Id FROM Profile WHERE Name='Agent Profile'];
        ingage_Application_Settings__c setting=new ingage_Application_Settings__c();
            setting.Manager_Profile_ID__c=agentProfile.Id;
            setting.Agent_Profile_ID__c=agentProfile.Id;
            //insert setting;

        
        

        Employee__c emp;
        Employee__c emp2;
        Employee__c emp3;

        System.runAs ([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]) {

            Test.startTest();

            User manageruser = new User();
            manageruser.isActive = true;
            manageruser.Username = 'ManagerOne' + '@testme.org';
            manageruser.Email = 'ManagerOne' + '@testme.org';
            manageruser.LastName = 'MLastName1';
            manageruser.FirstName = 'MFirstName1';
            manageruser.Alias = 'Manag';
            manageruser.CommunityNickname ='man1';
            manageruser.ProfileId =p.id;
            manageruser.EmailEncodingKey='UTF-8';
            manageruser.LanguageLocaleKey='en_US';
            manageruser.LocaleSidKey='en_US';
            manageruser.TimeZoneSidKey='America/Los_Angeles';
            manageruser.Country = 'US';
            manageruser.Title = 'Territory Manager';
            manageruser.managerid = (id)UserInfo.getUserId() ;
            insert manageruser;

            User user1 = new User();
            user1.isActive = true;
            user1.Username = 'UserOne' + '@testme.org';
            user1.Email = 'UserOne' + '@testme.org';
            user1.LastName = 'ULastName1';
            user1.FirstName = 'UFirstName1';
            user1.Alias = 'User';
            user1.CommunityNickname ='use1';
            user1.ProfileId =p.id;
            user1.EmailEncodingKey='UTF-8';
            user1.LanguageLocaleKey='en_US';
            user1.LocaleSidKey='en_US';
            user1.TimeZoneSidKey='America/Los_Angeles';
            user1.Country = 'US';
            user1.Title = 'Territory Manager';
            user1.managerid = manageruser.id ;
            insert user1;

        

                emp2 = new Employee__c();
                emp2.Department__c= 'xx';
                emp2.First_Name__c = 'First';
                emp2.User_Record__c = UserInfo.getUserId();
                insert emp2;

                emp3 = new Employee__c();
                emp3.Department__c= 'xx';
                emp3.First_Name__c = 'First';
                emp3.User_Record__c = manageruser.id;
                insert emp3;

                emp = new Employee__c();
                emp.Department__c= 'xx';
                emp.Manager_User__c = UserInfo.getUserId() ;
                emp.First_Name__c = 'First';
                emp.User_Record__c = user1.id;
              
                insert emp;

                Engagement_Survey_Feedback__c sfb = new Engagement_Survey_Feedback__c();
                sfb.Employee_lookup__c = emp.id;
                sfb.Additional_Comments_1__c = 'Comments';
                sfb.Completion_Status__c = 'status';
                sfb.Question_1_1__c = 1;
                insert sfb;

                sfb.Additional_Comments_1__c = 'Comments1';
                update sfb;

                delete sfb;
                undelete sfb;

            Test.stopTest();
        }
    }
}