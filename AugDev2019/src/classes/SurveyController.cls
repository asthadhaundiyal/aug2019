public class SurveyController {
    @AuraEnabled
    public static String initialize(Id surveyId) {
        Survey__c survey = [
            SELECT Id, Submit_Response__c, First_Question__c, Background_Image_URL__c
            FROM Survey__c
            WHERE Id = :surveyId
        ];
        Map<Id, Survey_Question__c> surveyQuestionMap = new Map<Id, Survey_Question__c>([
            SELECT Id, Question__c, Is_Parent__c, Type__c, Default_Next_Question__c, Required__c, Slider_Min_Range__c, Slider_Max_Range__c, Parent_Question__c
            FROM Survey_Question__c
            WHERE Survey__c = :surveyId ORDER BY OrderNumber__c
        ]);
        List<Survey_Option__c> surveyOptionList = [
            SELECT Id, Option__c, Survey_Question__c, Next_Question__c
            FROM Survey_Option__c WHERE Survey_Question__c IN :surveyQuestionMap.keySet() ORDER BY Order_Number__c ASC
        ];
        Map<Id, Map<Id, Survey_Option__c>> mapOfQuestionIdToOptionList = new Map<Id, Map<Id, Survey_Option__c>>();
        Map<Id, List<CustomSelectOption>> mapOfIdToOptionList = new Map<Id, List<CustomSelectOption>>();
        Map<Id, Survey_Option__c> tempOptionList;
        Map<Id, Id> mapOfOptionIdToQuestionId = new Map<Id, Id>();
        for(Survey_Option__c surveyOption : surveyOptionList) {
            mapOfOptionIdToQuestionId.put(surveyOption.Id, surveyOption.Survey_Question__c);
            if(mapOfQuestionIdToOptionList.containsKey(surveyOption.Survey_Question__c)) {
                tempOptionList = mapOfQuestionIdToOptionList.get(surveyOption.Survey_Question__c);
                tempOptionList.put(surveyOption.Id, surveyOption);
                mapOfQuestionIdToOptionList.put(surveyOption.Survey_Question__c, tempOptionList);
                List<CustomSelectOption> options = mapOfIdToOptionList.get(surveyOption.Survey_Question__c);
                options.add(new CustomSelectOption(surveyOption.Id, surveyOption.Option__c));
                mapOfIdToOptionList.put(surveyOption.Survey_Question__c, options);
            } else {
                mapOfQuestionIdToOptionList.put(surveyOption.Survey_Question__c, new Map<Id, Survey_Option__c>{surveyOption.Id => surveyOption});
                mapOfIdToOptionList.put(surveyOption.Survey_Question__c, new List<CustomSelectOption>{new CustomSelectOption(surveyOption.Id, surveyOption.Option__c)});
            }
        }
        Map<Id, ResponseWrapper> mapOfQuestionIdToWrapper = new Map<Id, ResponseWrapper>();
        ResponseWrapper wrapper;
        ResponseWrapper childWrapper;
        for(Survey_Question__c question : surveyQuestionMap.values()) {
            if(question.Parent_Question__c == null) {
                wrapper = mapOfQuestionIdToWrapper.get(question.Id);
                if(wrapper == null) {
                    wrapper = new ResponseWrapper();
                    wrapper.question = question;
                    wrapper.surveyOptionsMap = mapOfQuestionIdToOptionList.get(question.Id);
                    wrapper.surveyOptions = mapOfIdToOptionList.get(question.Id);
                    wrapper.isSkipped = true;
                    if(wrapper.surveyOptions != null) {
                        wrapper.optionIds = new String[wrapper.surveyOptions.size()];
                    }
                    mapOfQuestionIdToWrapper.put(question.Id, wrapper);
                }
            } else {
                wrapper = mapOfQuestionIdToWrapper.get(question.Parent_Question__c);
                if(wrapper == null) {
                    wrapper = new ResponseWrapper();
                    wrapper.question = surveyQuestionMap.get(question.Parent_Question__c);
                    wrapper.surveyOptionsMap = mapOfQuestionIdToOptionList.get(question.Parent_Question__c);
                    wrapper.surveyOptions = mapOfIdToOptionList.get(question.Parent_Question__c);
                    wrapper.isSkipped = true;
                    if(wrapper.surveyOptions != null) {
                        wrapper.optionIds = new String[wrapper.surveyOptions.size()];
                    }
                    mapOfQuestionIdToWrapper.put(question.Parent_Question__c, wrapper);
                }
                childWrapper = new ResponseWrapper();
                childWrapper.question = surveyQuestionMap.get(question.Id);
                childWrapper.surveyOptionsMap = mapOfQuestionIdToOptionList.get(question.Id);
                childWrapper.surveyOptions = mapOfIdToOptionList.get(question.Id);
                if(childWrapper.surveyOptions != null) {
                    childWrapper.optionIds = new String[childWrapper.surveyOptions.size()];
                }
                if(wrapper.childQuestionList == null) {
                    wrapper.childQuestionList = new List<ResponseWrapper>{childWrapper};
                } else {
                    wrapper.childQuestionList.add(childWrapper);
                }
            }
        }
        Integer i = mapOfQuestionIdToWrapper.values().size();
        Id currentQuestion = survey.First_Question__c;
        while(i > 0) {
            i--;
            mapOfQuestionIdToWrapper.get(currentQuestion).isSkipped = false;
            currentQuestion = mapOfQuestionIdToWrapper.get(currentQuestion).question.Default_Next_Question__c;
            if(currentQuestion == null) {
                break;
            }
        }
        MainWrapper wrap = new MainWrapper();
        wrap.mapOfOptionIdToQuestionId = mapOfOptionIdToQuestionId;
        wrap.survey = survey;
        wrap.listOfWrapper = mapOfQuestionIdToWrapper.values();
        return JSON.serialize(wrap);
    }

    @AuraEnabled
    public static void submitSurveyResponse(String wrapperStr){
        MainWrapper wrapper = (MainWrapper)JSON.deserialize(wrapperStr, MainWrapper.class);
        SurveyTaker__c taker = new SurveyTaker__c();
        taker.User__c = UserInfo.getUserId();
        taker.Survey__c = wrapper.survey.Id;
        insert taker;
        List<SurveyQuestionResponse__c> listOfSurveyResponse = new List<SurveyQuestionResponse__c>();
        SurveyQuestionResponse__c surveyResponse;
        for(ResponseWrapper wrap : wrapper.listOfWrapper) {
            if(!wrap.isSkipped){
                if(!wrap.question.Is_Parent__c) {
                    surveyResponse = new SurveyQuestionResponse__c();
                    surveyResponse.Survey_Question__c = wrap.question.Id;
                    if(wrap.optionId != null) {
                        surveyResponse.Response__c = getResponseFromOption(wrap.optionId, wrap.surveyOptions);
                        if('Other'.equalsIgnoreCase(surveyResponse.Response__c)){
                            surveyResponse.Response__c = wrap.response;
                        }
                    } else if(wrap.response != null) {
                        surveyResponse.Response__c = wrap.response;
                    } else {
                        surveyResponse.Response__c = getResponseFromOption(wrap.optionIds, wrap.surveyOptions);
                    }
                    if(String.isNotBlank(surveyResponse.Response__c)) {
                        surveyResponse.SurveyTaker__c = taker.Id;
                        listOfSurveyResponse.add(surveyResponse);
                    }
                } else {
                    for(ResponseWrapper resWrap : wrap.childQuestionList) {
                        surveyResponse = new SurveyQuestionResponse__c();
                        surveyResponse.Survey_Question__c = resWrap.question.Id;
                        if(resWrap.optionId != null) {
                            surveyResponse.Response__c = getResponseFromOption(resWrap.optionId, resWrap.surveyOptions);
                            if('Other'.equalsIgnoreCase(surveyResponse.Response__c)){
                                surveyResponse.Response__c = resWrap.response;
                            }
                        } else if(resWrap.response != null) {
                            surveyResponse.Response__c = resWrap.response;
                        } else {
                            surveyResponse.Response__c = getResponseFromOption(resWrap.optionIds, resWrap.surveyOptions);
                        }
                        if(String.isNotBlank(surveyResponse.Response__c)) {
                            surveyResponse.SurveyTaker__c = taker.Id;
                            listOfSurveyResponse.add(surveyResponse);
                        }
                    }
                }
            }
        }
        if(listOfSurveyResponse.size() > 0) {
            insert listOfSurveyResponse;
        }
    }

    @testVisible
    private static String getResponseFromOption(String[] selectedIds, List<CustomSelectOption> selectOptions) {
        if(selectedIds == null) {
            return '';
        }
        List<String> ids = new List<String>();
        for(String str : selectedIds) {
            for(CustomSelectOption option : selectOptions) {
                if(str == option.value) {
                    ids.add(option.label);
                    continue;
                }
            }
        }
        return String.join(ids, '; ');
    }

    @testVisible
    private static String getResponseFromOption(String selectedId, List<CustomSelectOption> selectOptions) {
        for(CustomSelectOption option : selectOptions) {
            if(selectedId == option.value) {
                return option.label;
            }
        }
        return null;
    }
    
    public class ResponseWrapper {
        public Survey_Question__c question;
        public String optionId;
        public String[] optionIds;
        public Map<Id, Survey_Option__c> surveyOptionsMap;
        public List<CustomSelectOption> surveyOptions;
        public String response;
        public Boolean isSkipped;
        public List<ResponseWrapper> childQuestionList;
    }

    public class CustomSelectOption{
        public String value;
        public String label;
        public CustomSelectOption(String value, String label) {
            this.value = value;
            this.label = label;
        }
    }

    public class MainWrapper{
        public Survey__c survey;
        public List<ResponseWrapper> listOfWrapper;
        public Map<Id, Id> mapOfOptionIdToQuestionId;
    }
}