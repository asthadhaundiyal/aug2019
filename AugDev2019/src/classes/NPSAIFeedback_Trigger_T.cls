/**
 * Test Class for NPSAIFeedback_Trigger
 */
@isTest
private class NPSAIFeedback_Trigger_T {

    	
    private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static nps__c n;
    private static NPS_AI__c nAI;
    private static NPS_AI_Feedback__c naFB;
    private static ingage_Application_Settings__c setting;
	
    static testMethod void test() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
			FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', true);
        	FeatureConsoleAPI.enableNPSwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = false);
            insert setting;
        
            emp = new employee__c(User_Record__c = thisUser.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(accountId = a.id, contactId = con.id, Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
            insert c;
            
            //n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English');
			//insert n;
        	//nAI = new NPS_AI__c(Text_to_Analyse__c ='text for training', AI_Category__c  = 'Testing', Language__c  = 'English', NPS__c  = n.id);
			//insert nAI;
        	naFB = new NPS_AI_Feedback__c(Text_for_Training__c = 'text for training', Category__c = 'Testing1', Language__c = 'English');
			//insert naFB;
            Test.startTest();
            insert naFB;
            update naFB;
            delete naFB;
            Test.stopTest();
            
        }
    }
}