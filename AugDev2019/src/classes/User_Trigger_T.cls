/**
 * Test Class for User_Trigger
 */
@isTest
private class User_Trigger_T {

    /**
     *
     */
    static testMethod void createEmployeeTest() {

        Profile p = [SELECT Id FROM Profile WHERE Name='Agent Profile'];
        ingage_Application_Settings__c setting=new ingage_Application_Settings__c();
        setting.Manager_Profile_ID__c=p.Id;
        setting.Agent_Profile_ID__c=p.Id;
        insert setting;


        ID managerProfileId=setting.Manager_Profile_ID__c;


        //Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        UserRole ur = [SELECT Id FROM UserRole limit 1];
        // Create new user with a non-null user role ID
        User u = new User(
            Alias = 'alias',
            Email = 'email@email.email',
            Emailencodingkey = 'UTF-8',
            FirstName = 'FirstName',
            LastName = 'LastName',
            LanguageLocaleKey='en_US',
            LocalesIdKey='en_US',
            ProfileId = managerProfileId,
            UserRoleid = ur.Id,
            TimezonesIdKey='America/Los_Angeles',
            Username = String.valueOf(system.today())+'@email.email'
        );

        System.runAs ([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]) {

            Test.startTest();

                insert u;
                u.FirstName = 'newFirstName';
                update u;
                
                User_Trigger.createEmployeeRecord(new set<id>{u.id});  // because employee is created in daily batch
             Test.stopTest();
        }

        system.assert([select id from Employee__c where User_Record__c =: u.id].size() == 1);
    }
}