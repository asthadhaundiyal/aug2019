@istest
public class QAFSearchController_T {

    static Employee__c emp;
    static in_gage__Quality_Assessment_Form__c qaf;
    static list<Quality_Assessment_Form_Answer__c> answers;
    static list<Question_Section__c> sections;
    static Question_Set__c qs;
    static ingage_Application_Settings__c setting;
    
    static{
    	
        Profile agentProfile = [SELECT Id FROM Profile Limit 1];
    	setting = new ingage_Application_Settings__c(Agent_Profile_ID__c= agentProfile.Id);
		insert setting;
        
        user mgr = [select id from user where isActive = true and id != :userInfo.getUserId() limit 1];
        
    	emp = new Employee__c(First_Name__c = 'Test', Manager_User__c = mgr.id);
    	insert emp;
    	user you = new user(id = userInfo.getUserId(), Employee_ID__c = emp.id);
        update you;
        account a = new account(name ='test');
        insert a;
        case c = new case(subject = 'test',accountId = a.id);
        insert c;
    	
    	qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id, Submitted_Form_Date__c = system.today(), Type__c = 'Case', Case__c = c.id);
    	
    }
    
    public static testMethod void testCreatingNewRecord(){
        // TESTING public PageReference newQAF()
      

        
        PageReference pageRef = new PageReference('/apex/QAFSearchPage');
        Test.setCurrentPage(pageRef);
        
	    QAFSearchController controller = new QAFSearchController();
        Test.startTest();
        controller.startDate = Date.Today().addDays(-1);
        controller.endDate = Date.Today();
        controller.empCallLookup();
        controller.mgrCallLookup();
        controller.returnSelectedEmp();
        controller.returnSelectedMgr();
        controller.clearAll();
        controller.setCaseType();
        controller.setSentimentType();
        controller.setStartDate();
        controller.setEndDate();
        controller.setQAFType();
        controller.searchQAF();
        controller.editQAF();
        controller.first();
        controller.last();
        controller.previous();
        controller.next();
        controller.newQAF();
        Test.stopTest();
        
    }
}