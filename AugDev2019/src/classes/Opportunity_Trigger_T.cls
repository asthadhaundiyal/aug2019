/**
 * Test Class for Opportunity_Trigger
 */
@isTest
private class Opportunity_Trigger_T {

	static testMethod void testSendDefaultSurvey() {
        User thisUser = [SELECT Id, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
            setting.Opportunity_Won_Survey__c ='1';
            setting.Reminder_Survey_Opp__c = 1;
            insert setting;
            
            /*Survey_Settings__c surveySetting = new Survey_Settings__c();
            surveySetting.Send_Opportunity_Won_Survey__c = true;
            insert surveySetting;*/
            
            Survey_Settings__c surveySetting = new Survey_Settings__c(SetupOwnerId = thisUser.ProfileId, Send_Opportunity_Won_Survey__c = true);
            insert surveySetting;
    
            Opp_Types_to_Run_Opp_Trigger__c oTypeSetting = new Opp_Types_to_Run_Opp_Trigger__c();
            oTypeSetting.Name = 'Other';
            insert oTypeSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            opportunity o = new opportunity(accountid = a.id, closeDate = date.today(), stagename = 'Prospecting',Survey_Contact__c = con.id, name = 'test Oppty', type='Other');
            insert o;
            
            Test.startTest();
            o.StageName = 'Closed Won';
            update o;
            Test.stopTest();
        }
    }
    
    static testMethod void testSendCustomSurvey() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS(); 
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
            setting.Opportunity_Won_Survey__c ='1';
            setting.Reminder_Survey_Opp__c = 1;
            setting.Is_Custom_Survey__c = true;
            insert setting;
            
            Survey_Settings__c surveySetting = new Survey_Settings__c();
            surveySetting.Send_Opportunity_Won_Survey__c  = true;
            insert surveySetting;
    
            Opp_Types_to_Run_Opp_Trigger__c oTypeSetting = new Opp_Types_to_Run_Opp_Trigger__c();
            oTypeSetting.Name = 'Other';
            insert oTypeSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            opportunity o = new opportunity(accountid = a.id, closeDate = date.today(), stagename = 'Prospecting',Survey_Contact__c = con.id, name = 'test Oppty', type='Other');
            insert o;
            
            Test.startTest();
            o.StageName = 'Closed Won';
            update o;
            Test.stopTest();
        }
    }
    
    static testMethod void testSendDefaultReminderSurvey() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
            setting.Opportunity_Won_Survey__c ='1';
            setting.Reminder_Survey_Opp__c = 1;
            insert setting;
            
            /*Survey_Settings__c surveySetting = new Survey_Settings__c();
            surveySetting.Send_Opportunity_Won_Survey__c = true;
            insert surveySetting;*/
            
            Survey_Settings__c  surveySetting = new Survey_Settings__c(SetupOwnerId=Userinfo.getUserId());
            surveySetting.Send_Opportunity_Won_Survey__c = true;
            insert surveySetting;
    
            Opp_Types_to_Run_Opp_Trigger__c oTypeSetting = new Opp_Types_to_Run_Opp_Trigger__c();
            oTypeSetting.Name = 'Other';
            insert oTypeSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            opportunity o = new opportunity(accountid = a.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con.id, name = 'test Oppty', type='Other'
                                            ,Opportunity_Won_Survey_Sent__c=true, Can_Send_Opportunity_Won_Survey__c = true);
            insert o;
            
            Test.startTest();
            o.Opportunity_Won_Survey_Sent__c = false;
            update o;
            Test.stopTest();
        }
    }
    
    static testMethod void testSendReminderCustomSurvey() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
            setting.Opportunity_Won_Survey__c ='1';
            setting.Reminder_Survey_Opp__c = 1;
            setting.Is_Custom_Survey__c = true;
            insert setting;
            
            Survey_Settings__c surveySetting = new Survey_Settings__c();
            surveySetting.Send_Opportunity_Won_Survey__c = true;
            insert surveySetting;
    
            Opp_Types_to_Run_Opp_Trigger__c oTypeSetting = new Opp_Types_to_Run_Opp_Trigger__c();
            oTypeSetting.Name = 'Other';
            insert oTypeSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            opportunity o = new opportunity(accountid = a.id, closeDate = date.today(), stagename = 'Closed Won',Survey_Contact__c = con.id, name = 'test Oppty', type='Other'
                                            ,Opportunity_Won_Survey_Sent__c = true, Can_Send_Opportunity_Won_Survey__c=true);
            insert o;
            
            Test.startTest();
            o.Opportunity_Won_Survey_Sent__c = false;
            update o;
            Test.stopTest();
        }
    }
    
    
}