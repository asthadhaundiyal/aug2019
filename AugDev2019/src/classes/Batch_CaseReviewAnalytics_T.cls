@istest
public class Batch_CaseReviewAnalytics_T {
	
	
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static ingage_Application_Settings__c setting;
	
	private static void setupData(){
        
        setting = new ingage_Application_Settings__c();
		setting.Customer_NPS_Survey_ID__c ='1';
        setting.Reduce_FCR30_Calculations__c = true;
        setting.No_of_Days_Lookup_for_Analytics__c = 30;
		insert setting;
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'Closed', Closed_Date__c=Date.today() );
		insert c;
        
        for(case c : [select isClosed from case]){
			system.assertEquals(true,c.isClosed);
		}
		
	}
	
	private static testmethod void testFCR() {
		
		setupData();
	
		Test.startTest();

		// schedule the maintenance job
		datetime dt = datetime.now();
		dt = dt.addHours(1);
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
		Sch_InGage_Maintenance schMain = new Sch_InGage_Maintenance();
        schMain.testFCRBatch = true;
        String jobId = system.schedule('In-gage Maintenance Schedule '+string.valueOf(date.today()), schedule, schMain);
		
		// check the schedule exists
		integer i = [SELECT count() FROM CronTrigger WHERE id = :jobId];
		System.assertEquals(1,i);
		
		// fire the batch process
		database.executebatch(new Batch_CaseReviewAnalytics());
		
		Test.stopTest();
	} 
}