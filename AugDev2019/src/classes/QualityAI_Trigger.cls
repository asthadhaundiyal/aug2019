public without sharing class QualityAI_Trigger extends TriggerHandler {
    
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    boolean qaAIEnabled {get;set;}
    boolean qaSEAIEnabled {get;set;}
    
    public QualityAI_Trigger() {
        this.qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        this.qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
    }
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
        if(qaAIEnabled) updateCaseCategory((List<QA_AI__c>) newObjects, (Map<Id, QA_AI__c>) oldObjectsMap);
        if(qaSEAIEnabled) updateQAFBehaviouralScore((List<QA_AI__c>) newObjects, (Map<Id, QA_AI__c>) oldObjectsMap);
        if(qaAIEnabled) updateEmotionHighlightsOnCase((List<QA_AI__c>) newObjects, (Map<Id, QA_AI__c>) oldObjectsMap);
        if(qaSEAIEnabled) updateAIComplainceOnCase((List<QA_AI__c>) newObjects, (Map<Id, QA_AI__c>) oldObjectsMap);
    }
    private void updateCaseCategory(List<QA_AI__c> newObjects, Map<Id, QA_AI__c> oldMap){
        set<id> caseIds = new set<id>();
        for(QA_AI__c qaAI : newObjects){
            if(qaAI.FirstTopCategoryLabel__c != oldMap.get(qaAI.id).FirstTopCategoryLabel__c) caseIds.add(qaAI.Parent_Case__c);
        }
        List<Case> caseList = [select id, in_gage__AICaseCategory__c, in_gage__Original_AI_Category__c, in_gage__AI_Case_SubCategory__c, in_gage__Original_AI_Sub_Category__c, (select id,FirstTopCategoryLabel__c, FirstTopSubCategoryLabel__c, in_gage__Original_AI_Category__c, in_gage__Original_AI_Sub_Category__c from QA_AI__r) from Case where id in : caseIds];
        if(!caseList.isEmpty()){           
            for(Case c: caseList){
                for(QA_AI__c qaAI : c.QA_AI__r){
                    c.in_gage__AICaseCategory__c = qaAI.FirstTopCategoryLabel__c;
                    c.in_gage__Original_AI_Category__c = qaAI.in_gage__Original_AI_Category__c;
                    c.in_gage__AI_Case_SubCategory__c = qaAI.FirstTopSubCategoryLabel__c;
                    c.in_gage__Original_AI_Sub_Category__c = qaAI.in_gage__Original_AI_Sub_Category__c;
                }
            }
            update caseList;
        }
    }
    private void updateEmotionHighlightsOnCase(List<QA_AI__c> newObjects, Map<Id, QA_AI__c> oldMap){
        set<id> caseIds = new set<id>();
        for(QA_AI__c qaAI : newObjects){
            if(qaAI.Categorisation_Only__c && qaAI.AI_Sentiment_Response__c!= oldMap.get(qaAI.id).AI_Sentiment_Response__c) caseIds.add(qaAI.Parent_Case__c);
        }
        List<Case> caseList = [select id, in_gage__Does_any_sentence_say_Anger__c, in_gage__Does_any_sentence_say_Frustration__c, in_gage__Does_any_sentence_say_Urgent__c
                               ,in_gage__Does_any_sentence_say_Concern__c, in_gage__Does_any_sentence_say_Happiness__c, in_gage__Does_any_sentence_say_Emotion_Neutral__c, in_gage__Does_any_sentence_say_Very_Negative__c,
                               in_gage__Does_any_sentence_say_Negative__c, in_gage__Does_any_sentence_say_Positive__c, in_gage__Does_any_sentence_say_Very_Positive__c, in_gage__Does_any_sentence_say_Sentiment_Neutral__c,
                               (select id, AI_Sentiment_Response__c from QA_AI__r where Categorisation_Only__c = true) 
                               from Case where id in : caseIds];
        system.debug('caseList: '+ caseList);
        if(!caseList.isEmpty()){           
            for(Case c: caseList){
                for(QA_AI__c qaAI : c.QA_AI__r){
                    if(qaAI.AI_Sentiment_Response__c != null){                        
                        PredictionSentAndEmo p = (PredictionSentAndEmo)JSON.deserialize(qaAI.AI_Sentiment_Response__c, PredictionSentAndEmo.class); 
                        system.debug('PredictionSentiment: '+ p );
                        
                        for(PredictionSentence ps : p.sentences){
                            Sentiment s = ps.sentiment;
                            String sentiment = s.classification;
                            if(sentiment == 'Very Negative' || sentiment == 'VeryNegative') c.in_gage__Does_any_sentence_say_Very_Negative__c = true;
                            else if(sentiment == 'Negative') c.in_gage__Does_any_sentence_say_Negative__c = true;
                            else if(sentiment == 'Positive') c.in_gage__Does_any_sentence_say_Positive__c = true;
                            else if(sentiment == 'Very Positive'  || sentiment == 'VeryPositive') c.in_gage__Does_any_sentence_say_Very_Positive__c = true;
                            else if (sentiment == 'Neutral') c.in_gage__Does_any_sentence_say_Sentiment_Neutral__c = true;
                            
                            //process emotions
                            List<Emotion> emotions = ps.emotions;
                            system.debug('emotions: '+ emotions);
                            if(!emotions.isEmpty() && emotions.size()>0){
                                for(integer i=0;i<emotions.size();i++){
                                    String topEmotion = emotions[0].classification;
                                    system.debug('topEmotion: '+ topEmotion);
                                    if(topEmotion == 'Happiness') c.in_gage__Does_any_sentence_say_Happiness__c = true;
                                    if(topEmotion == 'Anger') c.in_gage__Does_any_sentence_say_Anger__c = true;
                                    if(topEmotion == 'Frustration') c.in_gage__Does_any_sentence_say_Frustration__c = true; 
                                    if(topEmotion == 'Urgency') c.in_gage__Does_any_sentence_say_Urgent__c = true;
                                    if(topEmotion == 'Concern') c.in_gage__Does_any_sentence_say_Concern__c = true;
                                    if(topEmotion == 'Neutral') c.in_gage__Does_any_sentence_say_Emotion_Neutral__c = true;
                                } 
                            }
                        }  
                    } 
                }
            }
            update caseList;
        }
    }
    private void updateQAFBehaviouralScore(List<QA_AI__c> newObjects, Map<Id, QA_AI__c> oldMap){
        
        //For Agent
        Map<Id,Decimal> qafAIScoreMap = new Map<Id, Decimal>();
        Map<Id,String> qafAgentSentMap = new Map<Id,String>();
        Set<id> qafIds = new Set<id>();
        Set<id> caseIds = new Set<id>();
        for(QA_AI__c qaAI : newObjects){
            if(qaAI.in_gage__Quality_Assessment_Form__c != oldMap.get(qaAI.id).in_gage__Quality_Assessment_Form__c) {
                qafIds.add(qaAI.in_gage__Quality_Assessment_Form__c);
                caseIds.add(qaAI.Parent_Case__c);
            }
        }
        if(qafIds!=null){
            List<AggregateResult> agList = [select AVG(AI_Score__c), in_gage__Quality_Assessment_Form__c qaId from QA_AI__c 
                                            where in_gage__Quality_Assessment_Form__c IN :qafIds AND Source__c = 'Agent' AND AI_Score__c!=null
                                            group by in_gage__Quality_Assessment_Form__c];
            for(AggregateResult q : agList){                
                Decimal totalScore = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                String qafId  = (String)q.get('qaId');
                qafAIScoreMap.put(qafId,totalScore*100);
                String aSentLabel = '';
                Decimal actualScore = totalScore * 3;
                if(0 <= actualScore &&  actualScore < 0.5) aSentLabel = 'Very Negative';
                else if(0.5 <= actualScore && actualScore < 1.5) aSentLabel = 'Negative';
                else if(1.5 <= actualScore && actualScore < 2.5) aSentLabel = 'Neutral';
                else if(2.5 <= actualScore && actualScore < 3.5) aSentLabel = 'Positive';
                else if(3.5 <= actualScore) aSentLabel = 'Very Positive';
                qafAgentSentMap.put(qafId,aSentLabel);
            }
        }
        system.debug('qafAIScoreMap: '+ qafAIScoreMap);
        system.debug('qafAgentSentMap: '+ qafAgentSentMap);
        
        //For Customer
        Map<Id,String> qafCustSentMap = new Map<Id,String>();
        if(caseIds!=null){
            List<AggregateResult> agList = [select AVG(AI_Score__c), Parent_Case__c cId from QA_AI__c 
                                            where Parent_Case__c in :caseIds
                                            and Source__c = 'Customer' AND AI_Score__c!=null group by Parent_Case__c];
            for(AggregateResult q : agList){
                
                Decimal totalScore = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                String cId  = (String)q.get('cId');
                String cSentLabel = '';
                Decimal actualScore = totalScore * 3;
                if(0 <= actualScore &&  actualScore < 0.5) cSentLabel = 'Very Negative';
                else if(0.5 <= actualScore && actualScore < 1.5) cSentLabel = 'Negative';
                else if(1.5 <= actualScore && actualScore < 2.5) cSentLabel = 'Neutral';
                else if(2.5 <= actualScore && actualScore < 3.5) cSentLabel = 'Positive';
                else if(3.5 <= actualScore) cSentLabel = 'Very Positive';
                qafCustSentMap.put(cId,cSentLabel);
            } 
        }
        system.debug('qafCustSentMap: '+ qafCustSentMap);
        List<in_gage__Quality_Assessment_Form__c> qafList = [select id, AI_Behavioural_Score__c, Overall_Agent_Sentiment_Label__c, Overall_Customer_Sentiment_Label__c, Case__c
                                                             from in_gage__Quality_Assessment_Form__c where id in :qafIds];
        if(!qafList.isEmpty()){           
            for(in_gage__Quality_Assessment_Form__c qaf: qafList){
                if(!qafAIScoreMap.isEmpty()) qaf.AI_Behavioural_Score__c = qafAIScoreMap.get(qaf.id);
                if(!qafAgentSentMap.isEmpty()) qaf.Overall_Agent_Sentiment_Label__c = qafAgentSentMap.get(qaf.id);
                if(!qafCustSentMap.isEmpty()) qaf.Overall_Customer_Sentiment_Label__c = qafCustSentMap.get(qaf.Case__c); 
            }
            update qafList;
        }
    }
    
    private void updateAIComplainceOnCase(List<QA_AI__c> newObjects, Map<Id, QA_AI__c> oldMap){
        set<id> caseIds = new set<id>();
        for(QA_AI__c qaAI : newObjects) caseIds.add(qaAI.Parent_Case__c);
        List<Case> caseList = [select id, Name__c, DOB__c, Address__c, High_Risk_Agent_Comms__c,
                               High_Risk_Customer_Comms__c, Low_Risk_Agent_Comms__c, Low_Risk_Customer_Comms__c,
                               To_Investigate_Agent_Comms__c, To_Investigate_Customer_Comms__c,
                               (select id, Source__c, Name__c, DOB__c, Address__c, High_Risk_Comms__c, 
                               Low_Risk_Comms__c, To_Investigate_Comms__c from QA_AI__r) 
                               from Case where id in : caseIds];
        system.debug('caseList: '+ caseList);
        if(!caseList.isEmpty()){           
            for(Case c: caseList){
                for(QA_AI__c qaAI : c.QA_AI__r){
                    	if(!c.Name__c)c.Name__c = qaAI.Name__c;
                        if(!c.DOB__c)c.DOB__c = qaAI.DOB__c;
                        if(!c.Address__c)c.Address__c = qaAI.Address__c;
                        if(!c.High_Risk_Agent_Comms__c && qaAI.Source__c == 'Agent')c.High_Risk_Agent_Comms__c = qaAI.High_Risk_Comms__c;
                        if(!c.High_Risk_Customer_Comms__c && qaAI.Source__c == 'Customer')c.High_Risk_Customer_Comms__c = qaAI.High_Risk_Comms__c;
                        if(!c.Low_Risk_Agent_Comms__c && qaAI.Source__c == 'Agent')c.Low_Risk_Agent_Comms__c = qaAI.Low_Risk_Comms__c;
                        if(!c.Low_Risk_Customer_Comms__c && qaAI.Source__c == 'Customer')c.Low_Risk_Customer_Comms__c = qaAI.Low_Risk_Comms__c;
                        if(!c.To_Investigate_Agent_Comms__c && qaAI.Source__c == 'Agent')c.To_Investigate_Agent_Comms__c = qaAI.To_Investigate_Comms__c;
                        if(!c.To_Investigate_Customer_Comms__c && qaAI.Source__c == 'Customer')c.To_Investigate_Customer_Comms__c = qaAI.To_Investigate_Comms__c; 
                }
            }
            update caseList;
        }
    }
    public class PredictionSentAndEmo{
        public List<PredictionSentence> sentences {get;set;}
    }
    public class PredictionSentence{
        public String sentence {get; set;}
        public Sentiment sentiment {get;set;}
        public List<Emotion> emotions {get;set;}
    }
    public class Sentiment{
        public String classification {get; set;}
        public String confidence {get; set;}
    }
    public class Emotion{
        public String classification {get; set;}
        public String confidence {get; set;}
    }
}