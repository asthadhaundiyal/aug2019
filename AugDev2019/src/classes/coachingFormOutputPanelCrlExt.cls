global without sharing class coachingFormOutputPanelCrlExt {

	private final Coaching_Form__c cf;
	public static decimal progressValue {get;set;}
	public string cfId {get;set;}
	
    public coachingFormOutputPanelCrlExt(ApexPages.StandardController stdController) {
        this.cf = (Coaching_Form__c)stdController.getRecord();
    }
    
    @RemoteAction
    global static decimal getProgress(string cfId) {
    	progressValue = [select CalculatingProgress__c from Coaching_Form__c where id =: cfId].CalculatingProgress__c;
    	system.debug('progressValue: '+ progressValue);
        return progressValue;
    }
   
}