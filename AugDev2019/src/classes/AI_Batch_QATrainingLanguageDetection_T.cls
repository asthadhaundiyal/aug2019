@istest
public class AI_Batch_QATrainingLanguageDetection_T {
		
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static QA_AI__c qaAI;
    private static QA_AI_Feedback__c qaFB;
    private static ingage_Application_Settings__c setting;
	
	private static void setupData(){
        
        setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = true, NPS_AI_Batch_Frequency__c = 1);
		insert setting;
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
		insert c;
        
        qaAI = new QA_AI__c(Parent_Case__c = c.id);
		insert qaAI;
        
        qaFB = new QA_AI_Feedback__c(QA_AI__c = qaAI.id, Text_for_Training__c = 'text for training', Category__c = 'Testing1', Sub_Category__c ='test1', Emotion__c ='Happiness', Sentiment__c ='Positive');
		insert qaFB;
	}
	
	private static testmethod void testTraining() {
		
		setupData();
        Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
		Test.startTest();
		// fire the batch process       
        string fQuery = 'select id, Language__c, Text_for_Training__c from QA_AI_Feedback__c'; 
		database.executeBatch(new AI_Batch_QATrainingLanguageDetection(fQuery),1); 
		
		Test.stopTest();
	}  
}