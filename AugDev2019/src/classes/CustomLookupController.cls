public with sharing class CustomLookupController {

  public list<case> caseResults{get;set;}
  public list<opportunity> opptyResults{get;set;}
  public string searchString{get;set;} // search keyword
  public string sortBy;
  public string sortingDirection = 'DESC';
  public string searchType {get; set;}
  
    public Quality_Assessment_Form__c qaf{get;set;}
    public String agentId{get;set;}
    
    public CustomLookupController(ApexPages.StandardController stdController) {
        //cs = new Case();
        this.qaf= (Quality_Assessment_Form__c)stdController.getRecord();
        if(this.qaf!=null){
            //qaf=[SELECT Days_for_Record_Search__c, Agent__c, Agent__r.User_Record__c, case__c, Opportunity__c, Type__c from Quality_Assessment_Form__c where Id=:qaf.Id];
        }
        if(System.currentPageReference().getParameters().get('txt') != null) searchType = System.currentPageReference().getParameters().get('txt').contains('Case') ? 'Case' : 'Opportunity';
        searchString = System.currentPageReference().getParameters().get('lksrch');
        agentId=System.currentPageReference().getParameters().get('agentId');
        System.debug('***' + agentId);
        runSearch();
    }
    
  
   public String sortingBy{
     get{
        return sortingBy;
     }
     set{
       if (value == sortingBy)
         sortingDirection = (sortingDirection == 'ASC') ? 'DESC' : 'ASC';
       else
         sortingDirection = 'DESC';
       sortingBy = value;
     }
   }

 public String getSortingDirection(){
    if (sortingBy == null || sortingBy == '')
      return 'DESC';
    else
     return sortingDirection;
 }

 public void setSortingDirection(String value){  
   sortingDirection = value;
 }
  
 
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }

  // prepare the query and issue the search command
  private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
    list<sObject> allResults = performSearch(searchString);
    if(searchType =='Opportunity'){
        opptyResults = (list<opportunity>)allResults;
    }else{
        caseResults = (list<case>)allResults;
    }
     
  }

  // run the search and return the records found.
  private List<sObject> performSearch(string searchString) {

    String query;
    string closeDateField;
        ingage_Application_Settings__c setting= ingage_Application_Settings__c.getOrgDefaults();
        integer days = setting.No_of_Days_Searched__c != null ? Integer.valueOf(setting.No_of_Days_Searched__c ) * -1 : -30;
        system.debug('days:'+ days);
    
    if(searchType =='Opportunity'){
        query = 'select id, Name, Account.Name, isWon, CloseDate from Opportunity where isWon=true ';
        if(searchString != '' && searchString != null) query += ' and Name LIKE \'%' + searchString +'%\'';
        closeDateField = 'CloseDate';
        if(days != null) query += ' AND '+ closeDateField +' >= '+ string.valueOf(date.today().addDays(days));
        if(sortingBy == null) sortingBy = closeDateField;
                
    }else{      
        query = 'select id, Origin, Reason, CaseNumber, Account.Name, IsClosed, ClosedDate from Case where isclosed=true ';
        if(searchString != '' && searchString != null) query += ' and CaseNumber LIKE \'%' + searchString +'%\'';
        closeDateField = 'ClosedDate';
        if(days != null) query += ' AND '+ closeDateField +' >= '+ string.valueOf(date.today().addDays(days)) + 'T00:00:00Z ';
        if(sortingBy == null) sortingBy = closeDateField;
    }
    
        if(agentId!=null && agentId!='' && query.contains('where')) query = query + ' AND OwnerId=\'' + agentId+'\'';
        
        //if(closeDateField != null) query += ' order by '+ closeDateField +' desc limit 50';
        //query += ' order by ' + sortingBy == null? closeDateField : sortingBy;
        //query += ' order by ' + closeDateField;
        query += ' order by ' + sortingBy;
        query += ' ' + sortingDirection;
        if(sortingDirection == 'DESC') query += ' NULLS LAST ';
        //query += ' limit 50';
        query += ' limit 500';
        
        
    System.debug(query);
    return database.query(query);

  }

/*
  // save the new account record
  public PageReference saveCase() {
    insert cs;
    // reset the account
    cs = new Case();
    return null;
  }*/

  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }

  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }

}