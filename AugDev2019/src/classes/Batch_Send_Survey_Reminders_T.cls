@istest
public class Batch_Send_Survey_Reminders_T {
	
	static employee__c e;
	static opportunity o;
	static case c;
	static account a;
	static contact con;
	static ingage_Application_Settings__c setting;
	static Survey_Settings__c surveySetting;
	
	static{
		setting = new ingage_Application_Settings__c();
		setting.Reminder_Survey_EES__c = 1;
		setting.Reminder_Survey_NPS__c = 1;
		setting.Reminder_Survey_Opp__c = 1;
        //setting.Is_Custom_Survey__c = true;
		setting.Customer_NPS_Survey_ID__c ='1';
		setting.Employee_Engagement_Survey__c = '1';
		setting.Opportunity_Won_Survey__c ='1';
		setting.Case_Types_for_Survey__c = 'Other;';
		insert setting;
		
		surveySetting = new Survey_Settings__c();
		surveySetting.Send_NPS_Survey__c = true;
		surveySetting.Send_Opportunity_Won_Survey__c = true;
		insert surveySetting;
		
		a = new account(name = 'test');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		e = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert e;
		o = new opportunity(StageName = 'Open', CloseDate = date.today(), name ='test', accountid = a.id, Survey_Contact__c = con.id);
		insert o;
		c = new case(accountId = a.id, contactId = con.id, subject ='test',type = 'Other', in_gage__Survey_Reminder_Date__c=Date.today());	
		insert c;
	}
	
	
	private static testmethod void testEES(){
		
		// send emp survey
		e.Send_Engagement_Survey__c = true;
		update e;
		system.assertNotEquals(null,[select Survey_Reminder_Date__c from Employee__c where id = :e.id].Survey_Reminder_Date__c);
		
		test.startTest();
		string q='SELECT Id, Send_Engagement_Survey__c FROM Employee__c WHERE Survey_Reminder_Date__c != null';
		database.executeBatch(new Batch_Send_Survey_Reminders(q));
		test.stopTest();
		
		system.assertEquals(null,[select Survey_Reminder_Date__c from Employee__c where id = :e.id].Survey_Reminder_Date__c);
		
	}
    
    private static testmethod void testNPS(){
		
		// send NPS survey
		c.Status = 'Closed';
		update c;
		system.assertEquals(true, [select isClosed from case where id = :c.id].isClosed);
		//system.assertNotEquals(null,[select Survey_Reminder_Date__c from Case where id = :c.id].Survey_Reminder_Date__c);
		
		test.startTest();
		string q='SELECT Id,OwnerId FROM Case WHERE Survey_Reminder_Date__c != null';
		database.executeBatch(new Batch_Send_Survey_Reminders(q));
		test.stopTest();
		
		//system.assertEquals(null,[select Survey_Reminder_Date__c from Case where id = :c.id].Survey_Reminder_Date__c);
		
	}
	
	private static testmethod void testOppty(){
		
		// send Oppty Won survey
		o.StageName = 'Closed Won';
		update o;
		//system.assertNotEquals(null,[select Survey_Reminder_Date__c from Opportunity where id = :o.id].Survey_Reminder_Date__c);
		
		test.startTest();
		string q='SELECT Id, Opportunity_Won_Survey_Sent__c FROM Opportunity WHERE Survey_Reminder_Date__c != null';
		database.executeBatch(new Batch_Send_Survey_Reminders(q));
		test.stopTest();
		
		//system.assertEquals(null,[select Survey_Reminder_Date__c from Opportunity where id = :o.id].Survey_Reminder_Date__c);
		
	}
    
}