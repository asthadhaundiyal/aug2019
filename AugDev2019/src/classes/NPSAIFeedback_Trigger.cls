public without sharing class NPSAIFeedback_Trigger extends TriggerHandler {
    
	ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    boolean npsAIEnabled {get;set;}
    
    public NPSAIFeedback_Trigger() {
        this.npsAIEnabled = FeatureConsoleAPI.npsWithAIEnabled();
    }

    public override void beforeInsert(List<SObject> newObjects) {
        //if(npsAIEnabled) updateNPSAIOriginalData((List<NPS_AI_Feedback__c>) newObjects); 
    }
    
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
        //if(npsAIEnabled) updateNPSAIWithTraining((List<NPS_AI_Feedback__c>) newObjects);
        if(npsAIEnabled && !setting.Reduce_NPS_AI_Processing__c) createAIEvents((List<NPS_AI_Feedback__c>) newObjects);
    }

    private void createAIEvents(List<NPS_AI_Feedback__c> newObjects) {       
        for(NPS_AI_Feedback__c n : newObjects){  
            if(n.Text_for_Training__c != null && n.Language__c != null && n.Category__c != null){
				events.add(new Event(Event.EventType.NPS_AI_TRAINING, n.id));
             }
        }
    }
    /*private void updateNPSAIWithTraining(List<NPS_AI_Feedback__c> newObjects) {       
        set<id> npsAIIds = new set<id>();
        for(NPS_AI_Feedback__c npsAIF : newObjects) npsAIIds.add(npsAIF.NPS_AI__c);
        List<NPS_AI__c> npsAIList = [select id, AI_Category__c, 
                                            (select id,Category__c  from NPS_AI_Feedback__r) 
                                            from NPS_AI__c where id in : npsAIIds];
        if(!npsAIList.isEmpty()){           
            for(NPS_AI__c npsAI: npsAIList){
                for(NPs_AI_Feedback__c npsAIF : npsAI.NPS_AI_Feedback__r){
                   npsAI.AI_Category__c = npsAIF.Category__c;
                }                 
            }
         update npsAIList;
        }
    }
    private void updateNPSAIOriginalData(List<NPS_AI_Feedback__c> newObjects) { 
        set<id> npsAIIds = new set<id>();
        Map<Id, NPS_AI__c> npsAIMap = new Map<Id, NPS_AI__c>();
        for(NPS_AI_Feedback__c npsAIF : newObjects) npsAIIds.add(npsAIF.NPS_AI__c);
        List<NPS_AI__c> npsAIList = [select id, AI_Category__c
                                            from NPS_AI__c where id in : npsAIIds];
        for(NPS_AI__c npsAI : npsAIList) npsAIMap.put(npsAI.Id, npsAI);
        for(NPS_AI_Feedback__c npsAIF : newObjects){
            String category = npsAIMap.get(npsAIF.NPS_AI__c).AI_Category__c;
            npsAIF.Original_AI_Category__c = category;
        }
    }*/
}