@istest
public class ProcessBuilderChatterGroup_T {

	private static user u;
	private static case c;
	private static opportunity o;
	private static NPS__c nps;
	
	static{
		Profile sysAdmin = [select id from Profile where name ='System Administrator' limit 1];
		u = [select id, firstName from user where isActive = true and ProfileId = :sysAdmin.Id limit 1];
		
		c = new case(ownerId = u.id, subject = 'test');
		insert c;
		
		o = new opportunity(ownerId = u.id, name = 'testOppty',StageName = 'Prospecting', CloseDate = Date.today());
		insert o;
		
		Employee__c e = new Employee__c(User_Record__c = u.id, First_Name__c = 'testFname');
		insert e;
		
		nps = new NPS__c(Employee__c = e.id, Answer_1__c = 10, Answer_2__c = 5);
		
		CollaborationGroup grp = new CollaborationGroup(name = 'test', CollaborationType = 'Public');
		insert grp;
		ingage_Application_Settings__c setting= new ingage_Application_Settings__c();
		setting.Chatter_Group_for_NPS__c = grp.id;
		insert setting;
		
	}

	private static testMethod void apexCodeTestCASE(){
		set<id> ids = new set<id>();
		ids.add(c.id);
		ProcessBuilderChatterGroup.postToChatter(ids);
	}

	private static testMethod void apexCodeTestOPPTY(){
		set<id> ids = new set<id>();
		ids.add(o.id);
		ProcessBuilderChatterGroup.postToChatter(ids);
	}
	
	private static testMethod void processBuilderTestCASE(){
		nps.Case__c = c.id;
		insert nps;
	}
	
	private static testMethod void processBuilderTestOPPTY(){
		nps.Opportunity__c = o.id;
		insert nps;
	}
	
	
}