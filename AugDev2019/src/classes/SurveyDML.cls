global without sharing class SurveyDML {
    
    webService static Id insertESF(Engagement_Survey_Feedback__c esf) {
        insert esf;
        return esf.id;
    }
    
    webService static Id insertNPS(NPS__c nps) {
        insert nps;
        return nps.id;
    }
    
    webService static boolean updateEmployee(Employee__c emp) {
    	update emp;
    	return true;
    }
    
    webService static boolean updateOppty(Opportunity opp) {
    	update opp;
    	return true;
    }
    
    webService static boolean updateCase(Case c) {
    	update c;
    	return true;
    }
    
    
    
/*  site guest user unable to access chatter    
    webservice static boolean insertFeed(list<feeditem> fis){
    	insert fis;
    	return true;
    }
*/
}