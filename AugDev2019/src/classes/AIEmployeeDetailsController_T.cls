@istest
public class AIEmployeeDetailsController_T {

    private static Employee__c emp;
    private static Employee__c mgr;
    private static account a;
    private static contact con;
    private static EmailMessage e1;
    private static List<QA_AI__c> qaAIList;
    private static Email_For_Processing_Data__c emailSetting;
    private static ingage_Application_Settings__c setting;
    private static Case c;
    private static NPS__c n;
    private static NPS_Feedback_Types__c npssetting;
    
        
   private static testMethod void agentTest(){
      System.runAs ([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]) {
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
       system.debug('profile: '+ p);
            UserRole ur = [SELECT Id FROM UserRole limit 1];
            User u = new User(
                Alias = 'alias',
                Email = 'email@email.email',
                Emailencodingkey = 'UTF-8',
                FirstName = 'FirstName',
                LastName = 'LastName',
                LanguageLocaleKey='en_US',
                LocalesIdKey='en_US',
                ProfileId = p.Id,
                UserRoleid = ur.Id,
                TimezonesIdKey='America/Los_Angeles',
                Username = String.valueOf(system.today())+'@email.email'
            );
       insert u;
       
            Agent_Profiles__c aProfileSetting = new Agent_Profiles__c();
            aProfileSetting.Name = p.Id;
            insert aProfileSetting;
            
            setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
            insert setting;
        
            emp = new employee__c(User_Record__c = u.Id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);

            Coaching_Form__c cf = new Coaching_Form__c(in_gage__Agent__c = emp.id, in_gage__Performance_Start_Date__c = system.today(), in_gage__Performance_End_Date__c = system.today().addMonths(1), in_gage__Manager__c  = emp.Id);
            insert cf;
            
            Goals__c g1 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='NPS %', Performance_Text__c = '50%', Target_Text__c = '100%');
            insert g1;
            Goals__c g2 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='Quality %', Performance_Text__c = '50%', Target_Text__c = '100%');
            insert g2;
            Goals__c g3 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='NPS %', Performance_Text__c = '50%', Target_Text__c = '100%');
            insert g3;
            Goals__c g4 = new Goals__c(Coaching_Form__c = cf.id, Goal_Name__c  ='Custom goal', Performance_Text__c = '50%' , Target_Text__c = '100%');
            insert g4;
          
            Coaching_Promise__c prom = new Coaching_Promise__c (Coaching_Form__c = cf.id, Subject__c = 'Test promise' , Completion_Date__c= Date.Today());
            insert prom;
          
            emp.Latest_Coaching_Form__c = cf.id;
            update emp;
            
            a = new account(name = 'test', Language__c ='English');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
            
            n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English', Reason__c ='Fees', Completed_Date__c = Date.Today());
            insert n;
            

            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdCont1 = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController QAFcontroller1 = new AIEmployeeDetailsController(stdCont1);
            
           Test.startTest();
           QAFcontroller1.editResult();
           QAFcontroller1.submitResult();
           Test.stopTest();
        
       }
     }
    private static testMethod void managerTest(){
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            UserRole ur = [SELECT Id FROM UserRole limit 1];
            User u = new User(
                Alias = 'alias',
                Email = 'email@email.email',
                Emailencodingkey = 'UTF-8',
                FirstName = 'FirstName',
                LastName = 'LastName',
                LanguageLocaleKey='en_US',
                LocalesIdKey='en_US',
                ProfileId = p.Id,
                UserRoleid = ur.Id,
                TimezonesIdKey='America/Los_Angeles',
                Username = String.valueOf(system.today())+'@email.email'
            );
       
            Manager_Profiles__c mProfileSetting = new Manager_Profiles__c();
            mProfileSetting.Name = p.Id;
            insert mProfileSetting;

            emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
            
            setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
            insert setting;
        
            emp = new employee__c(User_Record__c = u.Id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.debug('User_Record_Profile__c: '+ emp.User_Record_Profile__c);
            system.assertNotEquals(null,emp.id);
            
            Coaching_Form__c cf = new Coaching_Form__c(in_gage__Agent__c = emp.id, in_gage__Performance_Start_Date__c = system.today(), in_gage__Performance_End_Date__c = system.today().addMonths(1), in_gage__Manager__c  = emp.Id);
            insert cf;
        
         	Goals__c g1 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='NPS %', Performance_Text__c = '50%', Target_Text__c = '100%');
            insert g1;
            Goals__c g2 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='Quality %', Performance_Text__c = '50%', Target_Text__c = '100%');
            insert g2;
            Goals__c g3 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='NPS %', Performance_Text__c = '50%', Target_Text__c = '100%');
            insert g3;
            Goals__c g4 = new Goals__c(Coaching_Form__c = cf.id, Goal_Name__c  ='Custom goal', Performance_Text__c = '50%' , Target_Text__c = '100%');
            insert g4;
          
            Coaching_Promise__c prom = new Coaching_Promise__c (Coaching_Form__c = cf.id, Subject__c = 'Test promise' , Completion_Date__c= Date.Today());
            insert prom;
          
            emp.Latest_Coaching_Form__c = cf.id;
            update emp;
            
            
            a = new account(name = 'test', Language__c ='English');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;

            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
        
            ApexPages.StandardController stdCont = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont);
        
            Test.startTest();
                controller.editResult();
                controller.submitResult();
            Test.stopTest();
        
        
     }
    private static testMethod void srManagerTest(){
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            UserRole ur = [SELECT Id FROM UserRole limit 1];
            User u = new User(
                Alias = 'alias',
                Email = 'email@email.email',
                Emailencodingkey = 'UTF-8',
                FirstName = 'FirstName',
                LastName = 'LastName',
                LanguageLocaleKey='en_US',
                LocalesIdKey='en_US',
                ProfileId = p.Id,
                UserRoleid = ur.Id,
                TimezonesIdKey='America/Los_Angeles',
                Username = String.valueOf(system.today())+'@email.email'
            );
       
            SManager_Profiles__c smProfileSetting = new SManager_Profiles__c();
            smProfileSetting.Name = p.Id;
            insert smProfileSetting;

            emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
            
            setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
            insert setting;
        
            emp = new employee__c(User_Record__c = u.Id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            Coaching_Form__c cf = new Coaching_Form__c(Agent__c = emp.id, Performance_Start_Date__c = system.today(), Performance_End_Date__c = system.today().addMonths(1));
            insert cf;
            
            Goals__c g1 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='NPS %', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g1;
            Goals__c g2 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='Quality %', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g2;
            Goals__c g3 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='First Contact Resolution %', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g3;
            Goals__c g4 = new Goals__c(Coaching_Form__c = cf.id, Goal_Name__c ='Custom goal', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g4;
            
            Coaching_Promise__c prom = new Coaching_Promise__c(Coaching_Form__c = cf.id, Subject__c='Test Sub', Completion_Date__c = Date.Today());
            insert prom;
            
            
            emp.Latest_Coaching_Form__c = cf.id;
            update emp;
            
            a = new account(name = 'test', Language__c ='English');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
            
            in_gage__Quality_Assessment_Form__c  qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id, Submitted_Form_Date__c = system.today(), Case__c = c.id);
            insert qaf;

            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
        
            ApexPages.StandardController stdCont = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont);
        
            Test.startTest();
                controller.editResult();
                controller.submitResult();
            Test.stopTest();
        
        
     }
    private static testMethod void qaAgentTest(){
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            UserRole ur = [SELECT Id FROM UserRole limit 1];
            User u = new User(
                Alias = 'alias',
                Email = 'email@email.email',
                Emailencodingkey = 'UTF-8',
                FirstName = 'FirstName',
                LastName = 'LastName',
                LanguageLocaleKey='en_US',
                LocalesIdKey='en_US',
                ProfileId = p.Id,
                UserRoleid = ur.Id,
                TimezonesIdKey='America/Los_Angeles',
                Username = String.valueOf(system.today())+'@email.email'
            );
       
            QA_Profiles__c qaProfileSetting = new QA_Profiles__c();
            qaProfileSetting.Name = p.Id;
            insert qaProfileSetting;
            
            npssetting = new NPS_Feedback_Types__c(Name='English Fees',Type__c ='Fees', Values__c ='fee,rates', Language__c='English');        
            insert npssetting;
            

            emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
            
            setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c  = true, QA_AI_Batch_Frequency__c = 1);
            insert setting;
        
            emp = new employee__c(User_Record__c = u.Id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            Coaching_Form__c cf = new Coaching_Form__c(Agent__c = emp.id, Performance_Start_Date__c = system.today(), Performance_End_Date__c = system.today().addMonths(1));
            insert cf;
            
            emp.Latest_Coaching_Form__c = cf.id;
            update emp;
            
            Goals__c g1 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='NPS %', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g1;
            Goals__c g2 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='Quality %', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g2;
            Goals__c g3 = new Goals__c(Coaching_Form__c = cf.id, Goal_Description__c ='First Contact Resolution %', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g3;
            Goals__c g4 = new Goals__c(Coaching_Form__c = cf.id, Goal_Name__c ='Custom goal', Performance_Text__c ='50%', Target_Text__c ='100%');
            insert g4;
            
            Coaching_Promise__c prom = new Coaching_Promise__c(Coaching_Form__c = cf.id, Subject__c='Test Sub', Completion_Date__c = Date.Today());
            insert prom;
            
            in_gage__Quality_Assessment_Form__c  qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id, Submitted_Form_Date__c = system.today());
            insert qaf;
            
            a = new account(name = 'test', Language__c ='English');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
             
            n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English', Completed_Date__c = Date.Today());
            insert n;
        
            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
        
            ApexPages.StandardController stdCont = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont);
        
            Test.startTest();
                controller.editResult();
                controller.submitResult();
            Test.stopTest();
     }
    private static testMethod void test33(){
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
            FeatureConsoleAPI.enableQAwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());

            Email_For_Processing_Data__c emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test', Language__c ='English');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            Test.startTest();
            case c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
            
            /*QA_AI__c qaAI = new QA_AI__c(AI_Score__c = 0.87,
                                              Anger_Score__c = 0.15, Concern_Score__c = 0.25,
                                              Enthusiasm_Score__c = 0.35, Happiness_Score__c = 0.45,
                                              Neutral_Score__c = 0.55, Relief_Score__c = 0.16, Surprise_Score__c = 0.17, 
                                              FirstTopCategoryLabel__c = 'TestCategory1', FirstTopCategoryScore__c = 0.5, FirstTopSubCategoryLabel__c = 'TestSubCategory1',
                                              Parent_Case__c = c.id, Source__c = 'Customer', Categorisation_Only__c = true, Prediction_Completed__c = true);*/
            
            QA_AI__c qaAI1 = new QA_AI__c(AI_Score__c = 0.87,
                                              Anger_Score__c = 0.25, Concern_Score__c = 0.27,
                                              Enthusiasm_Score__c = 0.35, Happiness_Score__c = 0.42,
                                              Neutral_Score__c = 0.26, Urgeny_Score__c = 0.39, Frustration_Score__c = 0.66,
                                              Parent_Case__c = c.id, Source__c = 'Agent', Employee__c = emp.id, Prediction_Completed__c = true);
            
            List<QA_AI__c> qaAIList = new List<QA_AI__c>();
            //qaAIList.add(qaAI);
            qaAIList.add(qaAI1);
            insert qaAIList;
            
            in_gage__Quality_Assessment_Form__c qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id 
                                                                ,Type__c = 'Case'
                                                                ,Case__c = c.id
                                                                ,AI_Created__c = true); 
            insert qaf;
            
            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
        
            ApexPages.StandardController stdCont = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont);
        
                controller.editQAResult();
                controller.submitQAResult();
            Test.stopTest();
        
            
        }
        
        
     }
    
    /*private static testMethod void test3(){
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
            FeatureConsoleAPI.enableQAwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());

            Email_For_Processing_Data__c emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test', Language__c ='English');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            Test.startTest();
            case c = new case(contactId = con.id, Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
            
            QA_AI__c qaAI = new QA_AI__c(AI_Score__c = 0.87,
                                              Anger_Score__c = 0.15, Concern_Score__c = 0.25,
                                              Enthusiasm_Score__c = 0.35, Happiness_Score__c = 0.45,
                                              Neutral_Score__c = 0.55, Relief_Score__c = 0.16, Surprise_Score__c = 0.17, 
                                              FirstTopCategoryLabel__c = 'TestCategory1', FirstTopCategoryScore__c = 0.5, FirstTopSubCategoryLabel__c = 'TestSubCategory1',
                                              Parent_Case__c = c.id, Source__c = 'Customer', Categorisation_Only__c = true, Prediction_Completed__c = true);
            
            /*QA_AI__c qaAI1 = new QA_AI__c(AI_Score__c = 0.87,
                                              Anger_Score__c = 0.25, Concern_Score__c = 0.27,
                                              Enthusiasm_Score__c = 0.35, Happiness_Score__c = 0.42,
                                              Neutral_Score__c = 0.26, Relief_Score__c = 0.39, Surprise_Score__c = 0.66,
                                              Parent_Case__c = c.id, Source__c = 'Agent', Employee__c = emp.id, Prediction_Completed__c = true);
            
            List<QA_AI__c> qaAIList = new List<QA_AI__c>();
            qaAIList.add(qaAI);
            //qaAIList.add(qaAI1);
            insert qaAIList;
            
            in_gage__Quality_Assessment_Form__c qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id 
                                                                ,Type__c = 'Case'
                                                                ,Case__c = c.id
                                                                ,AI_Created__c = true); 
            insert qaf;
            
            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
        
            ApexPages.StandardController stdCont = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont);
        
                controller.editQAResult();
                controller.submitQAResult();
            Test.stopTest();
        
            
        }
        
        
     }*/
     private static testMethod void test12(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            UserRole ur = [SELECT Id FROM UserRole limit 1];
            User u = new User(
                Alias = 'alias',
                Email = 'email@email.email',
                Emailencodingkey = 'UTF-8',
                FirstName = 'FirstName',
                LastName = 'LastName',
                LanguageLocaleKey='en_US',
                LocalesIdKey='en_US',
                ProfileId = p.Id,
                UserRoleid = ur.Id,
                TimezonesIdKey='America/Los_Angeles',
                Username = String.valueOf(system.today())+'@email.email'
            );
        System.runAs (u) {
            
            FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
            FeatureConsoleAPI.enableQAwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
             
            Agent_Profiles__c aProfileSetting = new Agent_Profiles__c();
            aProfileSetting.Name = p.Id;
            insert aProfileSetting;
             mgr = new Employee__c();
             mgr.First_Name__c = 'Manager';
             mgr.User_Record__c=u.id;
             insert mgr;

            Employee__c emp = new employee__c(User_Record__c = u.Id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true, Manager_User__c = u.Id);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test', Language__c ='English');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            case c = new case(contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', Description='My account is disabled. Please disable my account with test@gmail.com created by testworld.com your team', type = 'Other', status = 'Closed');
            insert c;
            Test.startTest();

            
            /*QA_AI__c qaAI = new QA_AI__c(AI_Score__c = 0.87,
                                              Anger_Score__c = 0.15, Concern_Score__c = 0.25,
                                              Enthusiasm_Score__c = 0.35, Happiness_Score__c = 0.45,
                                              Neutral_Score__c = 0.55, Relief_Score__c = 0.16, Surprise_Score__c = 0.17, 
                                              FirstTopCategoryLabel__c = 'TestCategory1', FirstTopCategoryScore__c = 0.5, FirstTopSubCategoryLabel__c = 'TestSubCategory1',
                                              Parent_Case__c = c.id, Source__c = 'Customer', Categorisation_Only__c = true);
            
            
            insert qaAI;*/
            
            
            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
        
            ApexPages.StandardController stdCont = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont);
        
                controller.editQAResult();
                controller.submitQAResult();
            Test.stopTest();
        
            
        }
        
        
     }
    static testMethod void test21() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            UserRole ur = [SELECT Id FROM UserRole limit 1];
            User u = new User(
                Alias = 'alias',
                Email = 'email@email.email',
                Emailencodingkey = 'UTF-8',
                FirstName = 'FirstName',
                LastName = 'LastName',
                LanguageLocaleKey='en_US',
                LocalesIdKey='en_US',
                ProfileId = p.Id,
                UserRoleid = ur.Id,
                TimezonesIdKey='America/Los_Angeles',
                Username = String.valueOf(system.today())+'@email.email'
            );
        System.runAs (u) {
            
            FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', true);
            FeatureConsoleAPI.enableNPSwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            Agent_Profiles__c aProfileSetting = new Agent_Profiles__c();
            aProfileSetting.Name = p.Id;
            insert aProfileSetting;
            
            npssetting = new NPS_Feedback_Types__c(Name='English Fees',Type__c ='Fees', Values__c ='fee,rates', Language__c='English', AI_Training_Language__c = 'English');        
            insert npssetting;
            
            setting = new ingage_Application_Settings__c(Update_Reason_For_New_Model__c = true, Reduce_NPS_AI_Processing__c = false);
            insert setting;
        
            emp = new employee__c(User_Record__c = u.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
            insert c;
            
            n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English', Reason__c ='Fees', Completed_Date__c = Date.Today());
            
            insert n;
            
        }
    }
    static testMethod void test103() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', true);
            FeatureConsoleAPI.enableNPSwithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            npssetting = new NPS_Feedback_Types__c(Name='English Fees',Type__c ='Fees', Values__c ='fee,rates', Language__c='English', AI_Training_Language__c = 'English');        
            insert npssetting;
            
            setting = new ingage_Application_Settings__c(Update_Reason_For_New_Model__c = true, Reduce_NPS_AI_Processing__c = false);
            insert setting;
            date d1 = Date.Today();
            date d2 = d1.addMonths(-1);
            emp = new employee__c(User_Record__c = thisUser.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true, DB_End_Date__c = d1, DB_Start_Date__c =d2);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
            insert c;
            
            n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English', Reason__c ='Fees', Completed_Date__c = Date.Today());
            
            insert n;
            
        }
        
    }

    public static testMethod void test1()
    {

        Profile p = [select id FROM Profile WHERE Name ='System Administrator'];
        Profile agentProfile = [SELECT Id FROM Profile WHERE Name='Agent Profile'];
        system.debug('agentProfile: '+ agentProfile);
        Profile managerProfile = [SELECT Id FROM Profile WHERE Name='Manager Profile'];
        Agent_Profiles__c aSetting = new Agent_Profiles__c();
        aSetting.Name = p.Id;
        insert aSetting;

        Employee__c emp;
        Employee__c emp2;
        Employee__c emp3;

        System.runAs ([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]) {

            Test.startTest();

            User manageruser = new User();
            manageruser.isActive = true;
            manageruser.Username = 'ManagerOne' + '@testme.org';
            manageruser.Email = 'ManagerOne' + '@testme.org';
            manageruser.LastName = 'MLastName1';
            manageruser.FirstName = 'MFirstName1';
            manageruser.Alias = 'Manag';
            manageruser.CommunityNickname ='man1';
            manageruser.ProfileId =p.id;
            manageruser.EmailEncodingKey='UTF-8';
            manageruser.LanguageLocaleKey='en_US';
            manageruser.LocaleSidKey='en_US';
            manageruser.TimeZoneSidKey='America/Los_Angeles';
            manageruser.Country = 'US';
            manageruser.Title = 'Territory Manager';
            manageruser.managerid = (id)UserInfo.getUserId() ;
            insert manageruser;

            User user1 = new User();
            user1.isActive = true;
            user1.Username = 'UserOne' + '@testme.org';
            user1.Email = 'UserOne' + '@testme.org';
            user1.LastName = 'ULastName1';
            user1.FirstName = 'UFirstName1';
            user1.Alias = 'User';
            user1.CommunityNickname ='use1';
            user1.ProfileId =agentProfile.id;
            user1.EmailEncodingKey='UTF-8';
            user1.LanguageLocaleKey='en_US';
            user1.LocaleSidKey='en_US';
            user1.TimeZoneSidKey='America/Los_Angeles';
            user1.Country = 'US';
            user1.Title = 'Territory Manager';
            user1.managerid = manageruser.id ;
            insert user1;

        

                emp2 = new Employee__c();
                emp2.Department__c= 'xx';
                emp2.First_Name__c = 'First';
                emp2.User_Record__c = UserInfo.getUserId();
                insert emp2;

                emp3 = new Employee__c();
                emp3.Department__c= 'xx';
                emp3.First_Name__c = 'First';
                emp3.User_Record__c = manageruser.id;
                insert emp3;

                emp = new Employee__c();
                emp.Department__c= 'xx';
                emp.Manager_User__c = UserInfo.getUserId() ;
                emp.First_Name__c = 'First';
                emp.User_Record__c = user1.id;
              
                insert emp;

                Employee_Survey_Anonymous__c sfb = new Employee_Survey_Anonymous__c();
                sfb.Related_Manager__c = emp.id;
                sfb.Question_1_1__c  = 3;
                sfb.Question_2__c  = 5;
                sfb.Question_3__c  = 3;
            	sfb.Question_4__c   = 1;
                sfb.Question_6__c  = 4;
                sfb.Started_Date__c   = Date.Today();
                insert sfb;

            
            PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
            pageRef.getParameters().put('id', emp.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdCont1 = new ApexPages.StandardController(emp);
            AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont1);
            
           controller.editResult();
           controller.submitResult();

            Test.stopTest();
        }
    }
    
    static testMethod void test104() {
        
        date d1 = Date.Today();
        date d2 = d1.addMonths(-1);
        emp = new employee__c(First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true, DB_End_Date__c = d1, DB_Start_Date__c =d2);
        insert emp;
        
        a = new account(name = 'test');
        insert a;
        con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
        insert con;
        
        c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
        insert c;
        
        n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English', Reason__c ='Fees', Completed_Date__c = Date.Today());
        insert n;
        in_gage__Quality_Assessment_Form__c  qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id, Submitted_Form_Date__c = system.today(), Case__c = c.id);
        insert qaf;
        
        PageReference pageRef = new PageReference('/apex/AIEmployeeDetailsController');
        pageRef.getParameters().put('id', emp.id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController(emp);
        AIEmployeeDetailsController controller = new AIEmployeeDetailsController(stdCont);
        
        Test.startTest();
        controller.editResult();
        controller.submitResult();
        Test.stopTest();
        
    }

}