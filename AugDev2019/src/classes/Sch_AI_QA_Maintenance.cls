global class Sch_AI_QA_Maintenance implements Schedulable {

		
    global void execute(SchedulableContext sc) {
		
        ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    	decimal aiQAFrequency = setting.QA_AI_Batch_Frequency__c;
    	boolean batchQASAI = setting.Reduce_QA_AI_Procesing__c;
        boolean qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        boolean qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        boolean clAutoDetectEnabled = FeatureConsoleAPI.caseLangAutoDetectEnabled();

        if(batchQASAI && aiQAFrequency!=null){
        	/*DateTime startDT = DateTime.now();
			DateTime closeDT = startDT.addHours(-(aiQAFrequency.intValue()+1));
        	string startDateTime = startDT.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            string closeDateTime = closeDT.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');*/
            
            Integer batchFrequency = aiQAFrequency.intValue() + 1;
            Datetime startDT = Datetime.now();    
            DateTime closeDT = startDT.addHours(-batchFrequency);
            system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);

            String startDateTime = startDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            String closeDateTime = closeDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            system.debug('startDateTime: '+startDateTime+ ' closeDateTime: '+closeDateTime); 
            
            //Run QA Language Prediction Batch
            //================================
            if(clAutoDetectEnabled){
            	string cQuery = 'select id, Description, AI_Language__c from Case where CreatedDate >=' + closeDateTime + ' AND CreatedDate <=' + startDateTime;
            	system.debug('cQuery: '+ cQuery);
                database.executeBatch(new AI_Batch_QALanguageDetection(cQuery),1); 
            }
            // Run QA Prediction for Case Batch
            //=================================
            if(qaAIEnabled){
            	string cQuery = 'select id, Origin, Type, Subject, Description, AI_Language__c, Employee__c from Case where id NOT IN (SELECT Parent_Case__c FROM QA_AI__c) and CreatedDate >=' + closeDateTime + ' AND CreatedDate <=' + startDateTime;
            	system.debug('cQuery: '+ cQuery);
                database.executeBatch(new AI_Batch_CreateQAForCase(cQuery),1); 
            }
            // Run QA Prediction for Email Batch
            //================================== 
            if(qaSEAIEnabled) database.executeBatch(new AI_Batch_CreateQAForEmail(''),100);
            
            //run QA Training batch
            string fQuery = 'select id, Text_for_Training__c,Case_Subject__c, Category__c, Sub_Category__c, Sentiment__c, Emotion__c, Language__c, Category_Sent_for_Training__c, Emotion_Sent_for_Training__c, Sentiment_Sent_For_Training__c from QA_AI_Feedback__c where Category_Sent_for_Training__c!=true and CreatedDate >=' + closeDateTime + 'AND CreatedDate <' + startDateTime; 
			system.debug('fQuery: '+ fQuery);
            database.executeBatch(new AI_Batch_QATraining(fQuery),100); 
            
            rescheduleJob(aiQAFrequency.intValue()); 
        }

	}
    
    @testVisible
	@future
	private static void rescheduleJob(integer frequency){
	
		CronJobDetail cronJob = [SELECT Id,JobType,Name FROM CronJobDetail WHERE Name = 'In-gage AI QA Schedule'];
		if(cronJob != null){
			CronTrigger cron = [SELECT Id FROM CronTrigger WHERE CronJobDetailId = :cronJob.id];
			if(cron != null) System.abortJob(cron.id);
		}
            
		Sch_AI_QA_Maintenance aiQA = new Sch_AI_QA_Maintenance();       
        
        Datetime startDT = Datetime.now();
        DateTime dt = startDT.addHours(frequency);
        system.debug('startDT: '+ startDT + ' dt: '+dt);
        
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
		system.schedule('In-gage AI QA Schedule', schedule , aiQA);
	
	}
	
}